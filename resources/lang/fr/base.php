<?php
return [
    "cart" => "Panier",
    "dashboard" => "Tableau de bord",
    "footer" => [
        "contact" => "Contact",
        "my_account" => "Mon compte",
        "title_links" => "Liens d'intérêt"
    ],
    "language" => "Langue",
    "our_products_in_store" => "Nos produits dans les magasins"
];
