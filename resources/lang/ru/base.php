<?php
return [
    "cart" => "Корзина",
    "dashboard" => "Приборная доска",
    "footer" => [
        "contact" => "Контакт",
        "my_account" => "Мой аккаунт",
        "title_links" => "Ссылки по интересам"
    ],
    "language" => "Язык",
    "our_products_in_store" => "Наши продукты в магазинах"
];
