<?php
return [
    "cart" => "Carrello della spesa",
    "dashboard" => "Pannello di controllo",
    "footer" => [
        "contact" => "Contatto",
        "my_account" => "Il mio account",
        "title_links" => "Link di interesse"
    ],
    "language" => "linguaggio",
    "our_products_in_store" => "I nostri prodotti nei negozis"
];
