<?php
return [
    "cart" => "Einkaufswagen",
    "dashboard" => "Instrumententafel",
    "footer" => [
        "contact" => "Kontakt",
        "my_account" => "Mein Konto",
        "title_links" => "Links von Interesse"
    ],
    "language" => "Sprache",
    "our_products_in_store" => "Unsere Produkte in den Geschäften"
];
