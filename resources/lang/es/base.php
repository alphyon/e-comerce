<?php
return [
    "cart" => "Carrito",
    "dashboard" => "Panel de Gestión",
    "footer" => [
        "contact" => "Contacto",
        "my_account" => "Mi Cuenta",
        "title_links" => "Enlaces de Interés"
    ],
    "language" => "Lenguaje",
    "our_products_in_store" => "Nuestros productos en tiendas"
];
