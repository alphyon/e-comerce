<?php
return [
    "cart" => "Shopping cart",
    "dashboard" => "Dashboard",
    "footer" => [
        "contact" => "Contact",
        "my_account" => "My Account",
        "title_links" => "Links of interest"
    ],
    "language" => "Language",
    "our_products_in_store" => "Our products in shops"
];
