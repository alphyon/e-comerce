<?php
return [
    "cart" => "Carrinho de compras",
    "dashboard" => "Painel",
    "footer" => [
        "contact" => "Contato",
        "my_account" => "Minha conta",
        "title_links" => "Links de interesse"
    ],
    "language" => "Língua",
    "our_products_in_store" => "Nossos produtos em lojas"
];
