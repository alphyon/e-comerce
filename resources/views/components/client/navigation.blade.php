<nav class="" x-data="{open: false}">
    <div class="px-2 py-2 flex justify-between border-b border-gray-300">
        <div class="flex md:space-x-3 items-start md:items-center space-y-2 md:space-y-0 flex-col md:flex-row">
            <div class="text-sm">
                <i class="fas fa-phone-alt"></i>
                <span>
                    +34 933 806 526
                </span>
            </div>
            @auth
                <span class="hidden md:block">|</span>
                <div class="ml-2">{{\Illuminate\Support\Facades\Auth::user()->email ??""}}</div>
            @endauth

            <span class="hidden md:block">|</span>
            <div class="relative" x-data="{showb: false}">
                <button @click="showb = !showb" class="focus:outline-none">{{ __('base.language') }} <i
                        class="fas fa-chevron-down text-xs"></i></button>
                <div class="origin-top absolute left-0 mt-1 p-2 bg-gray-50 text-gray-800 shadow border-t-2 border-title"
                     x-show="showb">
                    <ul>
                        @foreach(\App\Models\Language::all() as $lang)
                            <li class="inline"><a href="{{route(Route::currentRouteName(),[
    'lang'=>$lang->locale,
    'family'=>Route::current()->parameters()['family'] ?? null,
    'id'=>Route::current()->parameters()['id'] ?? null
])}}">
                                    <div class="flex flex-col-2 gap-2 justify-items-start">
                                        <div class="flex flex-row align-baseline items-baseline gap-0.5">
                                            <img class="w-1/6 h-1/2"
                                                 src="{{ asset("/img/flags/".$lang->locale.".png") }}" alt=""><span
                                                class="ml-2 mr-3">{{__($lang->name)}}</span></div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="flex md:space-x-2 space-y-2 md:space-y-0 flex-col md:flex-row text-sm">

            @if(isset(\Illuminate\Support\Facades\Auth::user()->role_id) )
                @if(\Illuminate\Support\Facades\Auth::user()->role_id === 1)
                    <a href="{{ route('admin.index') }}" class="hover:text-title hover:underline">{{__('Admin')}}</a>
                    <span class="hidden md:block">|</span>
                @endif
            @endif
            @guest
                <span class="hidden md:block">|</span>
                <a href="{{ route('login') }}" class="hover:text-title hover:underline">{{ __('Gestionar') }}</a>
                <span class="hidden md:block">|</span>
                <a href="{{ route('login') }}"
                   class="hover:text-title hover:underline">{{ __('base.cart') }}</a>
                <span class="hidden md:block">|</span>
            @endguest

            @auth
                <span class="hidden md:block">|</span>
                <a href="{{route('client.dashboard',app()->getLocale())}}"
                   class="hover:text-title hover:underline">{{ __('Gestionar') }}</a>
                <span class="hidden md:block">|</span>
                <a href="{{ route('client.cart',app()->getLocale()) }}"
                   class="hover:text-title hover:underline">{{ __('base.cart') }}</a>
                <span class="hidden md:block">|</span>
            @endauth
            @auth
                <livewire:auth.logout/>
            @endauth
            @guest
                <a href="{{route('login')}}" class="hover:text-title hover:underline">{{ __('Login') }}</a>
            @endguest
        </div>
    </div>

    <!--  -->
    <div class="flex justify-between md:justify-start items-center px-2">
        <a href="">
            <div class="px-2 py-4">
                <img class="h-8 w-40" src="{{ asset('img/logo-header.png') }}" alt="Logo">
            </div>
        </a>

        <div class="block md:hidden mr-2">
            <button @click="open=!open"
                    class="inline-flex p-2 rounded-full focus:bg-table focus:outline-none focus:text-white"><i
                    class="fas fa-bars"></i></button>
        </div>
        <div class="hidden md:ml-10 md:flex space-x-2 items-center md:flex-wrap">
            <a href="{{route('home',['lang'=>app()->getLocale()])}}"
               class="uppercase py-2 px-2 inline-block text-xl text-gray-500 font-base hover:text-title hover:bg-gray-100">Home</a>
            @auth
                @foreach(\App\Models\RolFamily::with('family')->where('user_id',auth()->user()->id)->where('permission',true)->get() as $family)
                    <a href="{{route('client.family',['family'=>$family->family->slug,'lang'=> app()->getLocale()]) }}"
                       class="uppercase py-2 px-2 inline-block text-xl text-gray-500 font-base hover:text-title hover:bg-gray-100">{{$family->family->name}}</a>
                @endforeach
            @endauth
            <a href=""
               class="uppercase py-2 px-2 inline-block text-xl text-gray-500 font-base hover:text-title hover:bg-gray-100">Pedidos</a>

            <!-- Descargas Dropdown -->
            <div class="relative" x-data="{showd: false}">
                <button @click="showd = !showd"
                        class="uppercase py-2 px-2 inline-block text-xl text-gray-500 font-base hover:text-title hover:bg-gray-100 focus:text-title focus:bg-gray-100 focus:outline-none">
                    Descargas
                </button>
                <div class="origin-top absolute left-0 mt-1 p-2 bg-gray-50 text-gray-800 shadow border-t-2 border-title"
                     x-show="showd">
                    @auth
                        @foreach(\App\Models\RolFamily::with('family')->where('user_id',auth()->user()->id)->where('permission',true)->get() as $family)
                            <a href="{{route('client.download',['family'=>$family->family->slug,'lang'=> app()->getLocale()]) }}"
                               class="pl-1 pr-3 block hover:text-title hover:underline">{{$family->family->name}}</a>
                        @endforeach
                    @endauth
                </div>
            </div>

        </div>
    </div>
    <!-- -->

    <div :class="{'block': open, 'hidden': ! open}" class="md:hidden block">
        <div class="pt-2 pb-3 space-y-1" x-data="{ showDescargas : false }">
            @foreach(\App\Models\RolFamily::with('family')->where('user_id',auth()->user()->id)->where('permission',true)->get() as $family)
                <a class="block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
                   href="{{route('client.family',['family'=>$family->family->slug,'lang'=> app()->getLocale()]) }}">
                    {{$family->family->name}}
                </a>
            @endforeach
            <a class="block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
               href="http://template.test/dashboard/inmuebles">
                Pedidos
            </a>
            <button @click="showDescargas = !showDescargas" class="block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
              >
                Descargas
            </button>
            <div class="relaltive" :class="{'block': showDescargas, 'hidden': ! showDescargas}">
                @foreach(\App\Models\RolFamily::with('family')->where('user_id',auth()->user()->id)->where('permission',true)->get() as $family)
                <a class="block pl-8 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
                    href="{{route('client.family',['family'=>$family->family->slug,'lang'=> app()->getLocale()]) }}">
                    {{ __($family->family->name) }}
                </a>
                @endforeach
            </div>

        </div>
    </div>
    <!-- -->
</nav>
