@props(['version'])

{{-- <div class="border-t-4 border-green-600 shadow-xl">
    <div class="flex justify-between items-center p-2">
        <span class="h-3 w-3" style="background-color: {{'#'.$version->color}}"></span>
        <a href="{{ route('client.product', ['id'=>$version->version->id, 'lang' => app()->getLocale()]) }}">{{ $version->product['name'] }}</a>
    </div>
    <div class="relative h-0 pb-52 md:pb-32">
        <img class="absolute object-fill w-full h-full" src="{{ Storage::url($version->version['photo']) }}" alt="">
    </div>
    <div class="grid grid-cols-2 text-white">

        <button class="appearance-none flex justify-center items-center bg-gray-500">{{ $version->version['stock'] }}</button>
        <button class="appearance-none flex justify-center items-center bg-pink-600" wire:click="addToCart({{$version->version->id}});">1 c.u</button>
        <button class="appearance-none flex justify-center items-center bg-pink-300">$ {{ $version->price/100  }} </button>
        <button class="appearance-none flex justify-center items-center bg-purple-400">{{ $version->version['reference']}}</button>
    </div>
</div> --}}

<div class="shadow-md rounded-xl p-8 md:p-0">
    <div>
        <a href="{{ route('client.product', ['id'=>$version->version->id, 'lang' => app()->getLocale()]) }}">
            <img class="w-full h-52 md:h-44 md:rounded-none rounded-full mx-auto" src="{{ url(Storage::url($version->version->photo)) }}" alt="" width="384" height="512">
        </a>
        <div class="px-4 py-2 ">
            <a class="text-title hover:underline" href="{{ route('client.product', ['id'=>$version->version->id, 'lang' => app()->getLocale()]) }}">
                <div class="flex justify-between items-center">
                    <h2 class="text-lg font-medium uppercase">{{ $version->product['name'] }}</h2>
                    <div class="flex items-center"> 
                        Color: 
                        <span class="h-3 w-3 ml-2" style="background-color: {{'#'.$version->color}}"></span>
                    </div>
                </div>
            </a>
            <div class="text-sm flex justify-between items-center">
                @if($version->version->stock > 0)
                <div>
                    <div class="text-white text-sm bg-green-600 p-1 flex items-center">
                        <i class="fas fa-truck"></i>
                        <span>{{ $version->version->stock}}</span>
                    </div>
                </div>
                @else
                <div>
                    <div class="text-white text-sm bg-red-600 p-1 flex items-center">
                        <i class="fas fa-times"></i>
                        <span>Stock</span>
                    </div>
                </div>
                @endif

                {{-- Material: {{ $version->version->material }} <br>
                Dim: Long {{ $version->product['width'] }} | Diam {{ $version->product['large'] }} --}}
                <div class="w-full text-title font-semibold my-2 text-right text-lg">{{ $version->price  }} €</div>
            </div>
            <div class="mt-2 flex space-x-2">
                <button wire:click="addBoxToCart({{$version->version->id}});" class="block w-full uppercase bg-menuitem text-white px-4 py-1">
                    <i class="fas fa-cube"></i> 
                    <span>1 </span>
                </button>
                <button wire:click="addToCart({{$version->version->id}});" class="block w-full uppercase bg-menuitem text-white px-4 py-1">
                    <i class="fas fa-shopping-cart"></i> 
                    <span>1 U.</span>
                </button>
                
                {{-- <div class="mt-3" x-data="{ verMas: false }">
                    <button class="block w-full text-sm focus:outline-none hover:underline appearance-none text-center" @click="verMas = !verMas">
                        <span x-show="verMas">Ocultar</span>
                        <span x-show="!verMas">Ver mas opciones</span>
                    </button>
                    <div x-show="verMas">
                        <h3 class="upperCase font-medium">MegaBox</h3>
                        <div class="flex justify-between">
                            <span>Contiene: 15 productos</span>
                            <span class="text-title font-semibold">213123€</span>
                        </div>
                        <button wire:click="addToCart({{$version->version->id}});" class="block w-full uppercase bg-menuitem text-white px-4 py-1">
                            Añadir MegaBox al carrito
                        </button>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>
