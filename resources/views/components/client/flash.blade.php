@if ($message = Session::get('success'))
    <div
        class="flex justify-between text-green-200 shadow-inner rounded p-3 bg-green-600"
    >
        <p class="self-center">
            <strong>Success </strong>{{$message}}.
        </p>
        <strong class="text-xl align-center cursor-pointer alert-del"
        >&times;</strong
        >
    </div>
@endif


@if ($message = Session::get('error'))
    <div
        class="flex justify-between text-red-200 shadow-inner rounded p-3 bg-red-600"
    >
        <p class="self-center">
            <strong>Error </strong>{{$message}}.
        </p>
        <strong class="text-xl align-center cursor-pointer alert-del"
        >&times;</strong
        >
    </div>
@endif



<script>
var alert_del = document.querySelectorAll('.alert-del');
alert_del.forEach((x) =>
x.addEventListener('click', function () {
x.parentElement.classList.add('hidden');
})
);
</script>
