@props(['icon', 'title'])

<div>
    <!-- header -->
    <div class="bg-table text-white px-4 py-2">
        <i class="fas fa-{{$icon}}"></i>
        <span class="ml-2 text-lg font-semibold">{{ $title }}</span>
    </div>
    <!-- body -->
    <div class="border border-title p-3">
        {{ $slot }}  
    </div>
</div>