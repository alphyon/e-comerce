@props(['label'])

<div class="border-l-4 border-title px-4 py-4 text-sm bg-alert">
    {{ $label }}
</div>