@props(['page'])

<div class="bg-gray-100 px-2 py-2 text-sm text-gray-600">
    <div class="flex space-x-3 items-center">
        <a href="#" class="flex items-center">
            <i class="fas fa-home"></i>
            <span class="ml-1">Home</span>
        </a>
        <div>
            <i class="fas fa-chevron-right"></i>
        </div>
        <div>
            <a href="#">
                {{ $page }}
            </a>
        </div>
    </div>
</div>