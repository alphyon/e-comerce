@props(['active', 'icon', 'label', 'routename'])

@php
$classes = ($active ?? false) 
    ? 'text-sm py-3 bg-menuitem'
    : 'text-sm py-3 bg-transparent text-white hover:bg-black border-t border-gray-600 hover:bg-opacity-75';

@endphp
<a href="{{ route($routename) }}">
    <div {{ $attributes->merge(['class' => $classes]) }}>
        <div class="relative flex items-center justify-between">
            <div class="pl-4">
                <i class="fas fa-{{$icon}}"></i>
                <span class="ml-2 font-semibold">{{ $slot }}</span>
            </div>
            @if($active)
            <div class="text-4xl absolute right-0">
                <i class="fas fa-caret-left"></i>
            </div>
            @else
            <div class="pr-4">
                <i class="fas fa-chevron-left"></i>
            </div>
            @endif
        </div>
    </div>
</a>