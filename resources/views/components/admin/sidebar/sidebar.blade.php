<div >
    <nav class="mt-5" ref="navigation">
        <x-admin.sidebar.menu-item routename="admin.index" icon="home" :active="request()->routeIs('admin.index')">
            Panel de inicio
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.language" icon="language" :active="request()->routeIs('admin.language')">
            Idiomas
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.family" icon="sitemap" :active="request()->routeIs('admin.family')">
            Familias
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.colors" icon="tint" :active="request()->routeIs('admin.colors')">
            Colores
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.sizes" icon="text-height" :active="request()->routeIs('admin.sizes')">
            Tallas
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.products.index" icon="gift" :active="request()->routeIs('admin.products.*')">
            Productos
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.orders" icon="gift" :active="request()->routeIs('admin.orders')">
            Ordenar
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.users" icon="users" :active="request()->routeIs('admin.users')">
            Usuarios
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.requests" icon="level-up-alt" :active="request()->routeIs('admin.requests')">
            Pedidos
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.downloads" icon="file-pdf" :active="request()->routeIs('admin.downloads')">
            Descargas
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.showcases" icon="store" :active="request()->routeIs('admin.showcases')">
            Exhibidores
        </x-admin.sidebar.menu-item>
        <x-admin.sidebar.menu-item routename="admin.settings" icon="cog" :active="request()->routeIs('admin.settings')">
            Configuración
        </x-admin.sidebar.menu-item>
    </nav>
</div>
