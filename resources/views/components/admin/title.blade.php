@props(['title', 'subtitle'])
<div class="flex items-baseline">
    <h3 class="text-3xl font-medium text-title">{{ $title }}</h3>
    <span class="ml-2 text-sm text-gray-500 ">{{ $subtitle }}</span>
</div>