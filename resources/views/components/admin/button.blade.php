@props(['label' => false, 'icon', 'reverse' => false])

@php
$classes = "px-4 py-2 text-white font-medium focus:outline-none focus:ring-2 flex items-center";

$classes = ($reverse) 
    ? $classes . ' flex-row-reverse space-x-reverse'
    : $classes;
    
$classes = ($label)
    ? $classes . ' space-x-2'
    : $classes;

@endphp

<button {{ $attributes->merge(['class' => $classes]) }}>
    <span>{{ $label }}</span>
    <i class="fas fa-{{$icon}} font-bold"></i>
</button>