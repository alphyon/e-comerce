@props(['name', 'path'])

<div x-data="{photoName: null, photoPreview: null }" {{ $attributes->merge(['class'=> ''])}}>
    <!-- Profile Photo File Input -->
    <input type="file" class="hidden" x-ref="photo" name="{{ $name }}" {{ $attributes->wire('model') }} x-on:change="
                        photoName = $refs.photo.files[0].name;
                        const reader = new FileReader();
                        reader.onload = (e) => {
                            photoPreview = e.target.result;
                        };
                        reader.readAsDataURL($refs.photo.files[0]);
                ">

    <label class="block font-medium text-sm text-gray-700" for="photo">
        Image
    </label>


    <!-- Current Profile Photo -->
    <div class="mt-2" x-show="! photoPreview">
        @if($path !== 'null')
        <img src="{{ $path }}" alt="Example Picture" class="h-20 w-20 object-cover">
        @else
        <img src="https://ui-avatars.com/api/?name=Selecciona+Foto&amp;color=7F9CF5&amp;background=EBF4FF" alt="Selecciona foto" class="rounded-full h-20 w-20 object-cover">
        @endif
    </div>

    <!-- New Profile Photo Preview -->
    <div class="mt-2" x-show="photoPreview" style="display: none;">
        <span class="block w-20 h-20" x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'" style="background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url('null');">
        </span>
    </div>

    <div class="text-red-700">
        @error($name) {{ $message }} @enderror
    </div>

    <button type="button" class="inline-flex items-center px-4 py-2 bg-white border border-gray-300 rounded-md font-semibold text-xs text-gray-700 uppercase tracking-widest shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50 transition ease-in-out duration-150 mt-2 mr-2" x-on:click.prevent="$refs.photo.click()">
        Selecciona una foto
    </button>
</div>
