@props(['method' => null])

<div
    x-data="{ show : @entangle($attributes->wire('model')) }"
    class="fixed z-10 inset-0 overflow-y-auto"
    x-on:close.stop="$wire.cancel()"
    x-on:keydown.escape.window="$wire.cancel()"
    x-show="show"
    style="display: none;">

    <div class="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:p-0">

        <div x-show="show" class="fixed inset-0 transition-opacity" aria-hidden="true"
            x-on:click="$wire.cancel()"
            x-transition:enter="ease-out duration-300"
            x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100"
            x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>

        <div x-show="show"
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-headline"
            x-transition:enter="ease-out duration-300"
            x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
            <header class="flex flex-col justify-center items-center p-3 text-blue-600">
                <div class="flex justify-center w-28 h-28 border-4 border-blue-600 rounded-full mb-4">
                    <svg class="fill-current w-20" viewBox="0 0 20 20">
                        <path
                            d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"
                        ></path>
                    </svg>
                </div>
                <h2 class="font-semibold text-2xl">Success</h2>
            </header>
            <main class="p-3 text-center">
                <p>
                    {{ $message }}
                </p>
            </main>
            <footer class="flex justify-center bg-transparent">
                <button
                    class="bg-blue-600 font-semibold text-white py-3 w-full rounded-b-md hover:bg-blue-700 focus:outline-none focus:ring shadow-lg hover:shadow-none transition-all duration-300"
                    wire:click="{{$method}}"
                >
                    {{__("Cerrar")}}
                </button>
            </footer>
        </div>

        </div>
    </div>
</div>
