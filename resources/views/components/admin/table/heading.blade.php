@props(['sortable'=>false, 'direction'=>null])

<th {{ $attributes->merge(['scope'=>'col', 'class'=>'px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider']) }}>
    @if($sortable)
    <div class="appearance-none cursor-pointer">
        {{ $slot }}
        @if($direction)
            <i class="fas fa-{{$direction === 'asc' ? 'sort-up' : 'sort-down' }} font-bold"></i>
        @endif
    </div>
    @else
    {{ $slot }}
    @endif
</th>