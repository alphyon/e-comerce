@props(['showPerPage' => true,'showFinder'=>true])
<div>
    <div class="flex md:justify-between items-center flex-col space-y-2 md:flex-row">
        @if($showPerPage)
        <label>
            Resultados por pagina:
            <select class="py-1" wire:model.debounce.500ms="perPage">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="100">100</option>
            </select>
        </label>
        @else
            <label></label>
        @endif
        @if($showFinder)
        <label>
            Buscar:
            <input
                type="text"
                wire:model="search"
                class="ml-2 px-2 py-1 border border-gray-400"/>
        </label>
            @endif
    </div>

    <div class="flex flex-col mt-4">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>

                                {{ $head }}

                            </tr>
                        </thead>

                        <tbody class="bg-white divide-y divide-gray-200">

                            {{ $body }}

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
