<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title ?? config('app.name') }} - CNEX</title>
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">

    @livewireStyles

    @stack('css')
</head>
<body class="text-gray-600">
    <div class="w-full  mx-auto">
        <x-client.navigation />
    </div>
    <div >
        <div class="bg-white w-full px-6 py-4">
            {{ $slot }}
        </div>
    </div>

    <!-- Footer -->
    <div class="w-full  mx-auto text-gray-300">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 p-4" style="background-color: #313030;">
            <div>
                <h4 class="text-2xl font-semibold mb-2">{{__('base.footer.title_links')}}</h4>

                <!-- Families Options -->
                @foreach(\App\Models\RolFamily::with('family')->where('user_id',auth()->user()->id)->where('permission',true)->get() as $family)
                <div class="flex space-x-2 items-center mt-2">
                    <i class="fas fa-chevron-right text-xs"></i>
                    <a class="text-blue-500 hover:underline hover:text-title"
                       href="{{route('client.family',['family'=>$family->family->slug,'lang'=> app()->getLocale()]) }}">
                        {{$family->family->name}}
                    </a>
                </div>
                @endforeach
                <div class="flex space-x-2 items-center mt-2">
                <i class="fas fa-chevron-right text-xs"></i>
                <a class="text-blue-500 hover:underline hover:text-title"
                   href="{{route('client.showcases',['lang'=>app()->getLocale()])}}">
                    {{__('base.our_products_in_store')}}
                </a>
                </div>
                <!-- End Families -->
            </div>
            <div>
                <h4 class="text-2xl font-semibold">{{__('base.footer.my_account')}}</h4>

                <div class="flex space-x-2 items-center mt-4">
                    <i class="fas fa-chevron-right text-xs"></i>
                    <a class="text-blue-500 hover:underline hover:text-title" href="{{route('client.dashboard',app()->getLocale())}}">Panel de gestion</a>
                </div>

                <div class="flex space-x-2 items-center mt-2">
                    <i class="fas fa-chevron-right text-xs"></i>
                    <a class="text-blue-500 hover:underline hover:text-title" href="">{{__('base.cart')}}}</a>
                </div>
            </div>
            <address class="lg:col-span-2">
                <h4 class="text-2xl font-semibold">{{__('base.footer.contact')}}</h4>
                <p class="mt-4">CNEX.AIE,S.L.</p>
                <p>Camí de Ca la Madrona 5-7</p>
                <p>Pol. Ind. Mata-Rocafonda</p>
                <p>08304 – Mataró (Barcelona)</p>
                <p>Tel.: +34 93.380.65.26</p>
                <p>Fax: 93.380.65.28</p>
            </address>
        </div>
        <div class="p-4" style="background-color: #272626;">
            <p>2014 © CNEX. Todos Los Derechos Reservados.</p>
            <p>Powered by</p>
        </div>
    </div>
    <!-- End Footer -->


    @livewireScripts
    <script src="{{ url(mix('js/app.js')) }}" defer></script>

    @stack('scripts')

</body>
</html>
