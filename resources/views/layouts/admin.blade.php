<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title ?? config('app.name') }} - CNEX</title>
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">

    @livewireStyles
</head>
<body>
    <div class="w-full bg-gray-900 text-white px-4 py-2">
        <div class="flex justify-between items-center">
            <div>
                <h1 class="text-2xl font-semibold">CNEX</h1>
            </div>
            <div>
              <span class="fa fa-arrow-down"></span>
            </div>
        </div>
    </div>
    <div class="flex">
        <div class="bg-black bg-opacity-75 text-white w-64 min-h-screen">
            <x-admin.sidebar.sidebar />
        </div>
        <div  class="bg-white w-full px-6 py-4">
            {{ $slot }}
        </div>
    </div>


    @livewireScripts
    <script src="{{ url(mix('js/app.js')) }}" defer></script>

</body>
</html>
