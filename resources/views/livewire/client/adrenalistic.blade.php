<div class="max-w-screen-2xl w-full mx-auto">
    <div class="flex space-x-4">
        <div class="grid grid-cols-5 gap-6 w-full">

            @foreach ($versions as $version)
                <!-- card -->
                <div class="border-t-4 border-green-600 ">
                    <div class="flex justify-between items-center p-2">
                        <span class="h-3 w-3" style="background-color: {{'#'.$version->color->code}}"></span>
                        <span>{{ $version->product->name }}</span>
                    </div>
                    <div class="relative h-0 pb-32">
                        <img class="absolute object-fill w-full h-full" src="{{ Storage::url($version->photo) }}" alt="">
                    </div>
                    <div class="grid grid-cols-2 text-white">
                        <button class="appearance-none flex justify-center items-center bg-gray-500">{{ $version->stock }}</button>
                        <button class="appearance-none flex justify-center items-center bg-pink-600">1 c.u</button>
                        <button class="appearance-none flex justify-center items-center bg-pink-300">{{$version}}</button>
                        <button class="appearance-none flex justify-center items-center bg-purple-400">{{ $version->reference}}</button>
                    </div>
                </div>
            @endforeach


        </div>

        <div class="w-1/4 bg-gray-500"></div>
    </div>
</div>
