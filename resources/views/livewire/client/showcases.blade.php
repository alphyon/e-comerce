<div>
    <div class="mt-4">
        @include('components.client.flash')
        <div class="container border-sm shadow m-2 p-4 mx-auto">
            <div class="grid grid-flow-col auto-cols-max gap-12">
{{----}}


{{--                --}}
                @forelse($stores as $store)
                    <a href="{{route('client.showcases.items',['lang'=>app()->getLocale(),'showcase'=>$store->id])}}"><div class="bg-gray-100 m-auto w-96 h-64 mt-5 container" style="background-image:url({{Storage::url($store->picture)}});
                        background-position: center;
                        background-repeat: no-repeat; background-size: cover;">
                        <div class="flex flex-row items-end h-full w-full">
                            <div class="flex flex-col w-full pb-3 pt-10 px-3 bg-gradient-to-t from-black text-white">
                                <h3 class="text-base font-bold leading-5 uppercase">{{$store->name}}</h3>
                                <div class="inline-flex items-center">
                                    <svg class="stroke-current stroke-1 text-blue-600 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                                <div class="flex flex-row justify-between">
                                    <div class="flex flex-row">

                                        <div class="w-max inline-flex ml-4 items-center">
                                            <span class="fa fa-map"></span>
                                            <span class="text-xs ml-1 antialiased">{{$store->address}}</span>
                                        </div>
                                    </div>
                                    <div class="flex flex-row">


                                        <div class="w-max inline-flex ml-4 items-center">
                                                    <span class="fa fa-phone"></span>
                                            <span class="text-xs ml-1 antialiased">{{$store->contact}}</span>
                                        </div>
                                    </div>
                                    <div class="w-max">
                                        <svg class="w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>

                @empty
                    <h1 class="text-2xl">{{__('NO HAY RESULTADOS ....')}}</h1>
                @endforelse
            </div>
        </div>
    </div>
</div>
