<div class="w-full mx-auto">
    <div class="w-full h-40 bg-cover bg-no-repeat bg-center" style='background-image: url({{asset("img/".$family->slug.".jpg")}});'>
        <div class="flex items-center h-full">
            <img src="{{ asset('img/logo-'.$family->slug.'.png') }}" alt="">
        </div>
    </div>



    <div class="flex flex-col space-y-4 lg:space-y-0 lg:flex-row lg:space-x-4 mt-10">
        <div class="w-full">

            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
                <div class="">
                    <div>Disponibilidad: </div>
                    <div class="flex space-x-2">
                        <div class="text-white text-sm bg-red-600 p-1">
                            <i class="fas fa-times"></i>
                            <span>Stock</span>
                        </div>
                        <div class="text-white text-sm bg-yellow-600 p-1">
                            <i class="fas fa-exclamation-triangle"></i>
                            <span>Stock</span>
                        </div>
                        <div class="text-white text-sm bg-green-600 p-1">
                            <i class="fas fa-truck"></i>
                            <span>Stock</span>
                        </div>
                    </div>
                </div>

                <div>
                    <label class="flex flex-col">Buscador
                        <input type="text" class="py-1" wire:model="search">
                    </label>
                </div>

                <div>
                    <label class="flex flex-col">Categorias
                        <select name="" id="" class="py-1" wire:model="category">
                            <option value="">categoria</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <div>
                    <label class="flex flex-col">Ordenar por
                        <select name="" id="" class="py-1">
                            <option value="">Ordenar</option>
                        </select>
                    </label>
                </div>
            </div>


            {{-- <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-6 mt-6"> --}}
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4  gap-6 mt-6">

                @foreach ($versions as $version)

                    @isset($version->product)
                    <!-- card -->
                    <x-client.family.version :version="$version" />
                    @endisset
                @endforeach
            </div>

            {{ $versions->links() }}
        </div>
        <div class="w-full lg:w-2/5 ">
            <div class="shadow-lg">
                <div class="py-2 px-4 border-b-2 border-gray-200 flex items-center">
                    <div class="p-2 bg-title rounded-full text-white text-xs">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                    <h4 class="ml-3 text-xl font-semibold uppercase">{{ __('Carrito' ) }}</h4>
                </div>
                <div class="my-4 px-4">

                    @foreach($items as $key => $item)
                        <livewire:client.shopping-cart.item :item="$item" :key="$key" />
                    @endforeach
                </div>
                <div class="py-3 px-4">
                    <a class="px-4 py-2 bg-menuitem text-white font-semibold uppercase hover:bg-table" href="{{ route('client.cart',app()->getLocale()) }}">Ver carrito</a>
                </div>
            </div>
        </div>
    </div>
</div>
