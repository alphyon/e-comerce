<div>
    <div class="mt-4">
        @include('components.client.flash')
        <div class="container border-sm shadow m-2 p-4 mx-auto">
            <div class="grid grid-flow-col auto-cols-max gap-12">

            @forelse($packages as $pack)
                    <div
                        class="flex flex-col justify-between w-64 sm:w-64 h-64 bg-white bg-center text-gray-800 shadow-md overflow-hidden"
                        style="background-image:url({{Storage::url($pack->image)}})">
                        <div class="flex justify-between items-center ml-4 pr-8">
                            <button
                                type="button"
                                wire:click="download({{$pack->id}})"
                                class="bg-pink-600 text-white bg-opacity-95 mt-2 shadow px-2 py-1 flex items-center font-bold text-xl rounded border-opacity-75 border-white border">
                                <span class="fa fa-download m-2" ></span>Download
                            </button>
                        </div>
                        <div class="bg-white bg-opacity-95 shadow-md rounded-r-xl p-4 flex flex-col mr-4 mb-8">
                            <h3 class="text-xl font-bold pb-2">{{$pack->name}}</h3>
                            <div class="flex justify-between items-center">

                            </div>
                        </div>
                    </div>

                @empty
                    <h1 class="text-2xl">{{__('NO HAY RESULTADOS ....')}}</h1>
                @endforelse
            </div>
        </div>
    </div>
</div>
