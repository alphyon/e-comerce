<div class="mx-auto">
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <style>
        .swiper-container {
            width: 100%;
            height: 300px;
            margin-left: auto;
            margin-right: auto;
        }

        .swiper-slide {
            background-size: cover;
            background-position: center;
        }

        .gallery-top {
            height: 80%;
            width: 100%;
        }

        .gallery-thumbs {
            height: 40%;
            box-sizing: border-box;
            padding: 10px 0;
        }

        .gallery-thumbs .swiper-slide {
            height: 100%;
            opacity: 0.4;
        }

        .gallery-thumbs .swiper-slide-thumb-active {
            opacity: 1;
        }
    </style>
    @endpush

    @push('scripts')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        var galleryThumbs = new Swiper('.gallery-thumbs', {
          spaceBetween: 10,
          slidesPerView: 4,
          loop: true,
          freeMode: true,
          loopedSlides: 5, //looped slides should be the same
          watchSlidesVisibility: true,
          watchSlidesProgress: true,
        });
        var galleryTop = new Swiper('.gallery-top', {
          spaceBetween: 10,
          loop: true,
          loopedSlides: 5, //looped slides should be the same
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          thumbs: {
            swiper: galleryThumbs,
          },
        });
      </script>
    @endpush

    <div class="flex flex-col space-y-4 lg:space-y-0 lg:flex-row lg:space-x-4 mt-10">
        <div class="w-full">
            <div class="shadow-xl py-3 px-4 grid grid-cols-1 lg:grid-cols-3  gap-8">
                <div class="relative h-64" wire:ignore>
                    <div class="swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            @foreach($gallery as $photos)
                            <div class="swiper-slide" style="background-image:url({{ Storage::url($photos->url) }})"></div>
                            @endforeach
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            @foreach($gallery as $photos)
                            <div class="swiper-slide" style="background-image:url({{ Storage::url($photos->url) }})"></div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-span-2">
                    <h1 class="text-xl font-semibold uppercase">{{ $version->product->name }}</h1>
                    <div class="mt-6 bg-gray-100 p-2">
                        {{ $version->product->productLanguages->description }}
                    </div>
                    <div class="flex space-x-2 text-white mt-3">
                        <span class="bg-purple-600 py-0 px-3">{{ $version->price/100 }} €</span>
                        <div class="py-0 px-3 {{ $version->stock === 0 ? 'bg-red-400' : 'bg-green-500' }}">
                            <i class="fas fa-truck"></i>
                            Stock
                        </div>
                    </div>
                    <div class="mt-3">
                        <!-- This example requires Tailwind CSS v2.0+ -->
                        <div class="flex flex-col">
                            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                  <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                        <table class="min-w-full divide-y divide-gray-200">
                                            <thead class="bg-gray-50">
                                                <tr>
                                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        Ref
                                                    </th>
                                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        Material
                                                    </th>
                                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        Color
                                                    </th>
                                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        Batería
                                                    </th>
                                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        Dimensiones
                                                    </th>
                                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        Stock
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="bg-white divide-y divide-gray-200">
                                                <tr>
                                                    <td class="px-6 py-4 whitespace-nowrap">
                                                        {{ $version->reference }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap">
                                                        {{ $version->material }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap">
                                                        {{ $version->color->name }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap">
                                                        {{ $version->battery}}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap">
                                                        Long {{ $version->product->width }} | Diam {{ $version->product->large }}
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap">
                                                        <span class="px-2 py-1 inline-flex items-center space-x-2 leading-5 font-semibold  text-white {{ $version->stock === 0 ? 'bg-red-400' : 'bg-green-500' }}">
                                                            <i class="fas fa-truck text-xs mr-2"></i>
                                                            Stock
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
  
                    </div>

                    <div class="mt-6">
                        @if($version->stock === 0)
                            <input type="email" class="w-64 py-1 px-0 text-center" wire:model="email">
                            <button class="bg-menuitem text-lg text-white px-4 py-1 uppercase -ml-1" wire:click="addToCart">Avisame cuando este disponible</button>
                        @else
                            <input type="number" class="w-12 py-1 px-0 text-center" wire:model="prodQuantity">
                            <button class="bg-menuitem text-lg text-white px-4 py-1 uppercase -ml-1" wire:click="addToCart">añadir al carrito</button>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="w-full lg:w-2/5">
            <div class="shadow-lg">
                <div class="py-2 px-4 border-b-2 border-gray-200 flex items-center">
                    <div class="p-2 bg-title rounded-full text-white text-xs">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                    <h4 class="ml-3 text-xl font-semibold uppercase">{{ __('Carrito' ) }}</h4>
                </div>
                <div class="my-4 px-4">

                    @foreach($items as $key => $item)
                        <livewire:client.shopping-cart.item :item="$item" :key="$key" />
                    @endforeach
                </div>
                <div class="py-3 px-4">
                    <button class="px-4 py-2 bg-menuitem text-white font-semibold uppercase hover:bg-table">Ver carrito</button>
                </div>
            </div>
        </div>
    </div>
</div>
