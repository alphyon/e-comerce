<div class="max-w-screen-2xl w-full mx-auto">
    <style>
        label {
            top: 0%;
            transform: translateY(-50%);
            font-size: 1em;
            color: rgba(37, 99, 235, 1);
        }

        .empty m-3 input:not(:focus) + label {
            top: 50%;
            transform: translateY(-50%);
            font-size: 14px;
        }

        input:not(:focus) + label {
            color: rgba(70, 70, 70, 1);
        }

        input {
            border-width: 1px;
        }

        input:focus {
            outline: none;
            border-color: rgba(37, 99, 235, 1);
        }
    </style>

    <div class="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4 mt-10">
        <div class="w-full">

            <div class="my-4">
                <x-admin.breadcrumb page="dashboard"/>
            </div>

            @include('components.client.flash')

            <form wire:submit.prevent="changePassword" class="container">
                <div class='flex justify-left bg-white shadow-md rounded-lg overflow-hidden  flex flex-col p-5 '>
                    <div class="grid grid-cols-2 gap-2">
                        <div class="flex flex-col">
                            <h3 class="text-2xl font-bold mb-4">{{__('Cambio de password')}}</h3>
                            <!-- This is the input component -->
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="password"
                                    type="password"
                                    name="password"
                                    wire:model="password"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                @error('password')
                                <div class="text-red-700 text-right">{{ $message }}</div> @enderror
                                <label for="password" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('password')}} <span class="text-red-800">*</span>
                                </label>
                            </div>
                            <!-- This is the input component -->
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="password_confirmation"
                                    type="password"
                                    name="password_confirmation"
                                    wire:model="password_confirmation"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                @error('password_confirmation')
                                <div class="text-red-700 text-right">{{ $message }}</div> @enderror
                                <label for="email" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('password confirmación')}} <span class="text-red-800">*</span>
                                </label>
                            </div>
                    <div class="mb-4"></div>
                        <div class="flex flex-col col-span-2 justify-start items-start">
                            <div class="grid grid-cols-2">
                                <x-admin.button type="button" label="{{__('cancelar')}}"
                                                class="ml-4 rounded bg-gray-500 hover:bg-gray-600 w-36 justify-center"
                                                icon="times-circle" wire:click="close"/>
                                <x-admin.button type="submit" label="{{__('guardar')}}"
                                                class="ml-4 rounded bg-blue-500 hover:bg-blue-600 w-36 justify-center"
                                                icon="save"/></div>
                        </div>
                    </div>
                </div>
                </div>
            </form>

        </div>
    </div>
    <x-admin.modal_confirmation wire:model="showModal" method="cancel">
        <x-slot name="message">
            {{__("se ha cambiado su contraseña de manera correcta por seguridad vamos a cerrar su sesion")}}
        </x-slot>
    </x-admin.modal_confirmation>

</div>



