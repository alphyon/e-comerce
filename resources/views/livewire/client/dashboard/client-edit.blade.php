<div class="max-w-screen-2xl w-full mx-auto">
    <style>
        label {
            /*top: 0%;*/
            transform: translateY(-50%);
            font-size: 1em;
            color: rgba(37, 99, 235, 1);
        }

        .empty m-3 input:not(:focus) + label {
            top: 50%;
            transform: translateY(-50%);
            font-size: 14px;
        }

        input:not(:focus) + label {
            color: rgba(70, 70, 70, 1);
        }

        input {
            border-width: 1px;
        }

        input:focus {
            outline: none;
            border-color: rgba(37, 99, 235, 1);
        }
    </style>

    <div class="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4 mt-10">
        <div class="w-full">

            <div class="my-4">
                <x-admin.breadcrumb page="dashboard"/>
            </div>



            <form wire:submit.prevent="editUser" class="container">
                <div class='flex justify-left bg-white shadow-md rounded-lg overflow-hidden  flex flex-col p-5 '>
                    <div class="grid grid-cols-2 gap-2">
                        <div class="flex flex-col">
                            <h3 class="text-2xl font-bold mb-4">{{__('Datos de la cuenta')}}</h3>
                            <!-- This is the input component -->
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="name"
                                    type="text"
                                    name="name"
                                    wire:model="name"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                @error('name')
                                <div class="text-red-700 text-right">{{ $message }}</div> @enderror
                                <label for="name" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('nombre')}} <span class="text-red-800">*</span>
                                </label>
                            </div>
                            <!-- This is the input component -->
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="name_complement"
                                    type="text"
                                    name="name_complement"
                                    wire:model="name_complement"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                @error('name_complement')
                                <div class="text-red-700 text-right">{{ $message }}</div> @enderror
                                <label for="email" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('complemento nombre')}} <span class="text-red-800">*</span>
                                </label>
                            </div>
                        <!-- This is the input component -->
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="email"
                                    type="text"
                                    name="email"
                                    wire:model="email"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                @error('email')
                                <div class="text-red-700 text-right">{{ $message }}</div> @enderror
                                <label for="email" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('email')}} <span class="text-red-800">*</span>
                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="nif"
                                    type="text"
                                    name="nif"
                                    wire:model="nif"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                @error('nif')
                                <div class="text-red-700 text-right">{{ $message }}</div> @enderror
                                <label for="nif" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('nif/vat')}} <span class="text-red-800">*</span>
                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="business_name"
                                    type="text"
                                    name="business_name"
                                    wire:model="business_name"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="business_name" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('razon social')}}
                                </label>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <h3 class="text-2xl font-bold mb-4">{{__('Datos de ubicación')}}</h3>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="address"
                                    type="text"
                                    name="address"
                                    wire:model="address"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="address" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Dirección')}}
                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="postcode"
                                    type="text"
                                    name="postcode"
                                    wire:model="postcode"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="postcode" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Código Postal')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="state"
                                    type="text"
                                    name="state"
                                    wire:model="state"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="state" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Provincia')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="city"
                                    type="text"
                                    name="city"
                                    wire:model="city"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="city" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Poblacion')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="country"
                                    type="text"
                                    name="country"
                                    wire:model="country"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="country" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Pais')}}

                                </label>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <h3 class="text-2xl font-bold mb-4">{{__('Contactos')}}</h3>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="phone1"
                                    type="text"
                                    name="phone1"
                                    wire:model="phone1"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="phone1" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Telefono 1')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="phone2"
                                    type="text"
                                    name="phone2"
                                    wire:model="phone2"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="phone2" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Telefono2')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="fax"
                                    type="text"
                                    name="fax"
                                    wire:model="fax"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="fax" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Fax')}}

                                </label>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <h3 class="text-2xl font-bold mb-4">{{__('Otros')}}</h3>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="web"
                                    type="text"
                                    name="web"
                                    wire:model="web"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />

                                <label for="web" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Web')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty m-3">
                                <select name="currency" id="currency" wire:model="currency_id">
                                    <option value="">elige una moneda</option>
                                    @foreach(\App\Models\Currency::all() as $currency)
                                        <option value="{{$currency->id}}">{{$currency->name}}</option>
                                    @endforeach
                                </select>
                                <label for="address" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Moneda')}}
                                </label>
                            </div>
                        </div>
                        <div class="flex flex-col col-span-2 justify-end items-end">
                            <div class="grid grid-cols-2">
                            <x-admin.button type="button" label="{{__('cancelar')}}"
                                            class="ml-4 rounded bg-gray-500 hover:bg-gray-600 w-36 justify-center"
                                            icon="times-circle" wire:click="close"/>
                            <x-admin.button type="submit" label="{{__('editar')}}"
                                            class="ml-4 rounded bg-blue-500 hover:bg-blue-600 w-36 justify-center"
                                            icon="edit"/></div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>

</div>

