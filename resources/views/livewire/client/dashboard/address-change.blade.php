<div class="max-w-screen-2xl w-full mx-auto">
    <style>
        label {
            /*top: 0%;*/
            transform: translateY(-50%);
            font-size: 1em;
            color: rgba(37, 99, 235, 1);
        }

        .empty m-3 input:not(:focus) + label {
            top: 50%;
            transform: translateY(-50%);
            font-size: 14px;
        }

        input:not(:focus) + label {
            color: rgba(70, 70, 70, 1);
        }

        input {
            border-width: 1px;
        }

        input:focus {
            outline: none;
            border-color: rgba(37, 99, 235, 1);
        }
    </style>

    <div class="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4 mt-10">
        <div class="w-full">

            <div class="my-4">
                <x-admin.breadcrumb page="dashboard"/>
            </div>

            @include('components.client.flash')


            <form wire:submit.prevent="editDelivery" class="container">
                <div class='flex justify-left bg-white shadow-md rounded-lg overflow-hidden  flex flex-col p-5 '>
                    <div class="grid grid-cols-2 gap-2">

                        <div class="flex flex-col">
                            <h3 class="text-2xl font-bold mb-4">{{__('Datos de envio')}}</h3>
                            <label class="flex space-x-2 inline-block m-4">

                                    <input type="checkbox" wire:click="checkData">


                                <span>{{__("Usar misma direccion de perfil")}}</span>
                            </label>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="address"
                                    type="text"
                                    name="address"
                                    wire:model="address"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="address" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Dirección')}}
                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="postcode"
                                    type="text"
                                    name="postcode"
                                    wire:model="postcode"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="postcode" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Código Postal')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="state"
                                    type="text"
                                    name="state"
                                    wire:model="state"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="state" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Provincia')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="city"
                                    type="text"
                                    name="city"
                                    wire:model="city"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="city" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Poblacion')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="country"
                                    type="text"
                                    name="country"
                                    wire:model="country"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="country" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Pais')}}

                                </label>
                            </div>
                            <div class="relative h-10 input-component empty  m-3">
                                <input
                                    id="contact"
                                    type="text"
                                    name="contact"
                                    wire:model="contact_reference"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm"
                                />
                                <label for="contact" class="absolute left-2 transition-all bg-white px-1">
                                    {{__('Contacto')}}

                                </label>
                            </div>
                        </div>


                        <div class="flex flex-col col-span-2 justify-end items-end">
                            <div class="grid grid-cols-2">
                                <x-admin.button type="button" label="{{__('cancelar')}}"
                                                class="ml-4 rounded bg-gray-500 hover:bg-gray-600 w-36 justify-center"
                                                icon="times-circle" wire:click="close"/>
                                <x-admin.button type="submit" label="{{__('editar')}}"
                                                class="ml-4 rounded bg-blue-500 hover:bg-blue-600 w-36 justify-center"
                                                icon="edit"/></div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>

</div>

