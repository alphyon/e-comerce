<div>

    <div class="mt-4">

        <div class="container border-sm shadow m-2 p-4 mx-auto">
            <div class="flex flex-wrap">

                @forelse($images as $image)

                        <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 mb-4 bg-gray-500">
                             <img class="m-4" src="{{Storage::url($image->image)}}" alt="">

                        </div>

                @empty
                    <h1 class="text-2xl">{{__('NO HAY RESULTADOS ....')}}</h1>
                @endforelse

            </div>
        </div>
    </div>
</div>
