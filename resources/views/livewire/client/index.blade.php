<div>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <style>
        .swiper-container {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
    </style>
    @endpush

    @push('scripts')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            spaceBetween: 30,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>
    @endpush


    @if(isset($config->value))
        <img

            src="{{ Storage::url($config->value) }}"
            alt=""/>
    @else
        <img

            src="{{asset('img/home/home.png') }}"
            alt=""/>
    @endif

    @auth

        <!-- Swiper -->
    <div class="swiper-container mt-4">
        <div class="swiper-wrapper">
            @foreach(\App\Models\RolFamily::with('family')->where('user_id',auth()->user()->id)->where('permission',true)->get() as $family)
            <div class="swiper-slider">
                <div>
                    <a class="" href="{{route('client.family',['family'=>$family->family->slug,'lang'=> app()->getLocale()]) }}"
                        class="pl-1 pr-3 block hover:text-title hover:underline">
                        <img class="object-cover" src="{{\Illuminate\Support\Facades\Storage::url($family->family->picture)}}"
                            alt="{{$family->family->slug}}">
                    </a>
                </div>
            </div>
            @endforeach
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
        
    @endauth
</div>
