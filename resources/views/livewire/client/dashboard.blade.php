<div class="max-w-screen-2xl w-full mx-auto">


    <div class="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4 mt-10">
        <div class="w-full">

            <div class="my-4">
                <x-admin.breadcrumb page="dashboard"/>
            </div>
            <h1 class="text-4xl">{{__('DASHBOARD')}}</h1>
            <div class="mt-4">
                <div class="container border-sm shadow m-2 p-4">
                    @include('components.client.flash')
                    <h1 class="text-2xl">{{__('MI CUENTA')}}</h1>
                    <div class="grid grid-cols-4">
                        <div class="border-sm shadow m-2 p-4 bg-blue-300 hover:bg-blue-400  w-auto h-auto">
                            <a href="{{route('client.dashboard.edit',app()->getLocale())}}">
                            <div class="flex flex-col items-center">
                                <span class="fa fa-user-edit text-4xl text-white font-extrabold"></span>
                                <h1 class="text-xl">{{__('Editar mis datos')}}</h1>
                            </div>
                            </a>

                        </div>
                        <div class="border-sm shadow m-2 p-4 bg-blue-300 hover:bg-blue-400  w-auto h-auto">
                            <a href="{{route('client.dashboard.pass_change',app()->getLocale())}}">
                            <div class="flex flex-col items-center">
                                <span class="fa fa-passport text-4xl text-white font-extrabold"></span>
                                <h1 class="text-xl justify-center">{{__('Cambiar password')}}</h1>
                            </div>
                            </a>
                        </div>
                        <div class="border-sm shadow m-2 p-4 bg-blue-300 hover:bg-blue-400  w-auto h-auto">
                            <a href="{{route('client.dashboard.delivery_change',app()->getLocale())}}">
                            <div class="flex flex-col items-center">
                                <span class="fa fa-truck text-4xl text-white font-extrabold"></span>
                                <h1 class="text-xl">{{__('Cambiar dirección de envio ')}}</h1>
                            </div>
                            </a>
                        </div>
                        <div class="border-sm shadow m-2 p-4 bg-blue-300 hover:bg-blue-400  w-auto h-auto">
                            <div class="flex flex-col items-center">
                                <span class="fa fa-list-ol text-4xl text-white font-extrabold"></span>
                                <h1 class="text-xl">{{__('Ver mis pedidos')}}</h1>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
