<div class="grid grid-cols-5 items-center text-sm">
    <div class="flex flex-row">
        <img class="h-5 w-5" src="{{ Storage::url($item->version->photo) }}" alt="">
        <span class="ml-4 whitespace-nowrap w-14 overflow-hidden overflow-ellipsis">{{ $item->version->product->name  }}</span>
    </div>
    <span class="">{{ $item->version->reference }}</span>
    <div class="" x-data>
        <input class="p-1 text-xs w-16 text-center" type="number" x-on:input="$wire.quantityChange($event.target.value);" value="{{ $item->quantity }}">
    </div>
    <span class="">{{ $item->price * $item->quantity }}</span>
    <button wire:click="delete" class="inline-block rounded-full p-1 appearance-none">
        <i class="fas fa-trash"></i>
    </button>
</div>
