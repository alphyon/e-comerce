<div class="max-w-screen-2xl w-full mx-auto">

    <div class="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4 mt-10">
        <div class="w-full">

            <div class="my-4">
                <x-admin.breadcrumb page="dashboard"/>
            </div>

            <h2 class="my-4 text-xl font-semibold">CARRITO</h2>

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            IMG
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            REF
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            NOMBRE
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            CARACTERISTICAS
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            VOLUMEN TOTAL
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            PESO TOTAL
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            CANTIDAD
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            PRECIO UNITARIO
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            TIPO IVA
                                        </th>
                                        <th scope="col" class="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            TOTAL
                                        </th>
                                        <th scope="col" class="relative px-6 py-3">
                                            <span class="sr-only">Edit</span>
                                          </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                    <!-- Foreach -->
                                    @foreach($items as $item)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <img class="h-5 w-5" src="{{ Storage::url($item->version->photo) }}" alt="">
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            {{ $item->version->reference }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            {{ $item->version->product->name  }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            Color: {{ $item->version->color->name }}; Size: {{ $item->version->size->size }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            213123
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            {{ $item->version->weight }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap" x-data>
                                            <input type="number" class="w-14 p-0 text-center" x-on:input="$wire.quantityChange({{$item->id}}, $event.target.value);" value="{{ $item->quantity }}" >
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            {{-- {{ $item->version->rateVersion->price }} --}}
                                            {{ $item->price }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            {{ $item->version->product->iva }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 text-right">
                                            @php
                                                // $totalp = $item->version->rateVersion->price * $item->quantity;
                                                $totalp = $item->price * $item->quantity;
                                                $iva = $item->version->product->iva / 100;
                                                $total = $totalp * $iva;
                                                $total = $total + $totalp;
                                            @endphp
                                            {{ $total }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <!-- Endforeach --> 
                                    

                                    <tr class="bg-gray-500 text-white">
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            Total
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            0,000483 m3	
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            0.003 Kg	
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="pl-6 py-4 whitespace-nowrap text-right">
                                            € 20,61	
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap"">
                                            
                                        </td>
                                    </tr>

                                    <tr class="">
                                        <td colspan="9"></td>
                                        <td class="flex flex-col p-4" colspan="2">
                                            <div class="grid grid-cols-2 mt-4 text-xl">
                                                <div class="text-left">TOTAL (EX IVA)</div>
                                                <div class="text-right text-title font-semibold">€ 20,61</div>
                                            </div>
                                            <div class="grid grid-cols-2 mt-4 text-xl">
                                                <div class="text-left">IVA</div>
                                                <div class="text-right text-title font-semibold">€ 20,61</div>
                                            </div>
                                            <div class="grid grid-cols-2 mt-4 text-xl">
                                                <div class="text-left">TOTAL</div>
                                                <div class="text-right text-title font-semibold">€ 20,61</div>
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>