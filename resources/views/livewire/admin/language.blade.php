<div>
    <x-slot name="title">Idiomas</x-slot>

    <div>
        <x-admin.title title="Listar idiomas" subtitle="listado de idiomas" />

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de idiomas" />
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de idiomas." />
        </div>

        <div class="mt-4">
            <x-admin.card icon="language" title="Tabla de idiomas">
                <x-admin.button label="Añadir idioma" class="bg-menuitem hover:bg-pink-400" icon="plus"
                wire:click="open()"/>

                <div class="mt-5">
                    <x-admin.table.table>
                        <x-slot name="head">
                            {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                            <x-admin.table.heading >Icon</x-admin.table.heading>
                            <x-admin.table.heading sortable wire:click="sortBy('name')" :direction="$sortField === 'name' ? $sortDirection : null">Nombre</x-admin.table.heading>
                            <x-admin.table.heading sortable wire:click="sortBy('locale')" :direction="$sortField === 'locale' ? $sortDirection : null">identificador</x-admin.table.heading>
                            <x-admin.table.heading>Acciones</x-admin.table.heading>
                        </x-slot>

                        <x-slot name="body">
                            @forelse($languages as $key => $language)
                                <x-admin.table.row wire:loading.class="opacity-50" >
                                    {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="flex-shrink-0 h-10 w-10">
                                                    <img
                                                        class="h-6 w-8 lg:bg-cover"
                                                        src="{{ asset("/img/flags/".$language->locale.".png") }}"
                                                        alt="" />
                                                </div>
                                            </div>


                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $language->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $language->locale }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex space-x-3">
                                            <x-admin.button wire:click="open('edit', {{ $language->id }})" class="bg-menuitem hover:bg-pink-400" icon="edit" />
                                            <x-admin.button wire:click="open('delete', {{ $language->id }})" class="bg-red-500 hover:bg-pink-600" icon="trash" />
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @empty
                                <x-admin.table.row>
                                    <x-admin.table.cell colspan="2">
                                        <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay resultados...</div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @endforelse
                        </x-slot>
                    </x-admin.table.table>

                    <div class="my-3">
                        {{ $languages->links() }}
                    </div>
                </div>


            </x-admin.card>
        </div>
        <x-admin.modal wire:model="modal" method="save">
            <x-slot name="title">Datos del lenguaje</x-slot>




            <label class="flex flex-col mt-4">
                <span>Lenguaje</span>
                <input type="text" wire:model="language.name">
                @error('language.name') <div class="text-red-700">{{ $message }}</div> @enderror
            </label>

            <label class="flex flex-col mt-4">
                <span>Locale</span>
                <input type="text" wire:model="language.locale">
                @error('language.locale') <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600" icon="save" />
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600" icon="trash" wire:click="destroy" />
            </x-slot>
        </x-admin.modal>
        </div>
    </div>
</div>
