<div>
    <x-slot name="title">Versión de productos</x-slot>

    <div>
        <x-admin.title title="Gestionar versiones producto" subtitle="Gestionar versiones"/>

        <div class="mt-4">
            <x-admin.breadcrumb page="Gestionar versión"/>
        </div>

        <div class="mt-4">
            <x-admin.alert label="Gestion de versión producto."/>
        </div>

        <div class="mt-4">
            <div class="mt-4">
                <x-admin.card icon="gift" title="Idiomas del producto">
                    <div class="flex justify-between items-center text-xl font-bold text-gray-500 mb-4">
                        <h5>para agregar idioma al producto de click en el icono de la bandera del lenguaje que quieras
                            agregar y/o editar</h5>
                        <br>
                    </div>
                    <div class="flex justify-between items-center text-xl font-bold text-gray-500">
                        @forelse($languages as $key => $language)
                            <img
                                wire:click="openProductLanguage({{$language->id}})"
                                class="h-6 w-8 lg:bg-cover"
                                src="{{ asset("/img/flags/".$language->language->locale.".png") }}"
                                alt="" style="
                            @if(empty($language->description))
                                filter: grayscale(100%);
                            @else
                                filter: grayscale(0);
                            @endif
                                "/>
                        @empty
                            No hay resultados...
                        @endforelse
                    </div>

                </x-admin.card>
            </div>
        </div>

        <div class="mt-4">
            <x-admin.card icon="gift" title="Tabla de gestión de versiones de producto">
                <x-admin.button label="Añadir modelo" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="openVersion()"/>

                <x-admin.table.table>
                    <x-slot name="head">
                        {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                        <x-admin.table.heading>
                            Foto
                        </x-admin.table.heading>
                        <x-admin.table.heading>
                            Color
                        </x-admin.table.heading>
                        <x-admin.table.heading>
                            Talla
                        </x-admin.table.heading>
                        <x-admin.table.heading sortable wire:click="sortBy('material')"
                                               :direction="$sortField === 'material' ? $sortDirection : null">
                            Material
                        </x-admin.table.heading>
                        <x-admin.table.heading>
                            Ref
                        </x-admin.table.heading>
                        <x-admin.table.heading>
                            Bateria
                        </x-admin.table.heading>
                        <x-admin.table.heading>
                            Stock
                        </x-admin.table.heading>
                        <x-admin.table.heading>Acciones</x-admin.table.heading>
                    </x-slot>

                    <x-slot name="body">
                        @forelse($versions as $key => $version)
                            <x-admin.table.row wire:loading.class="opacity-50">
                                {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                <x-admin.table.cell>
                                    <div class=" h-10 w-10">
                                        <img
                                            class="h-10 w-10 rounded-full"
                                            src="{{ Storage::url($version->photo) }}"
                                            alt=""/>
                                    </div>
                                </x-admin.table.cell>

                                <x-admin.table.cell>{{ $version->color->name ?? ""}} </x-admin.table.cell>

                                <x-admin.table.cell>{{ $version->size->size ??"" }}</x-admin.table.cell>

                                <x-admin.table.cell>{{ $version->material }}</x-admin.table.cell>

                                <x-admin.table.cell>{{ $version->reference }}</x-admin.table.cell>

                                <x-admin.table.cell>{{ $version->battery }}</x-admin.table.cell>

                                <x-admin.table.cell>{{ $version->stock }}</x-admin.table.cell>

                                <x-admin.table.cell>
                                    <div class="flex space-x-3">
                                        <x-admin.button label="editar" wire:click="openVersion('edit', {{ $version->id }})"
                                                        class="bg-menuitem hover:bg-pink-400" icon="edit"/>
                                        <a href="{{ route('admin.versions.manage', $version->id) }}" class="text-white bg-indigo-500 hover:bg-indigo-600 px-4 py-2">
                                            <span class="fa fa-cog"></span> gestionar
                                        </a>

                                        @if($version->active)
                                        <x-admin.button label="desactivar" wire:click="openVersion('delete', {{ $version->id }})"
                                                        class="bg-red-500 hover:bg-pink-600" icon="times-circle"/>
                                        @else
                                            <x-admin.button label="activar" wire:click="openVersion('delete', {{ $version->id }})"
                                                            class="bg-yellow-700 hover:bg-yellow-800" icon="redo"/>
                                        @endif
                                    </div>
                                </x-admin.table.cell>
                            </x-admin.table.row>
                        @empty
                            <x-admin.table.row>
                                <x-admin.table.cell colspan="8">
                                    <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay
                                        resultados...
                                    </div>
                                </x-admin.table.cell>
                            </x-admin.table.row>
                        @endforelse
                    </x-slot>
                </x-admin.table.table>
                <div class="my-3">
                    {{ $versions->links() }}
                </div>
            </x-admin.card>
        </div>

        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">Ejecutar operación</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Ejecutar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="" wire:click="destroy"/>
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="modalVersion" method="saveVersion">
            <x-slot name="title">Datos de version</x-slot>
            <div class="m-2">
                <h3>Nombre del producto: <strong>{{$product->name}}</strong></h3>
            </div>
            <x-admin.form.image-field class="" name="picture" wire:model="picture"
                                      :path=" url(Storage::url($picture))"/>

            <div class="grid grid-cols-2 gap-2">

                <input type="hidden"  wire:model="version.product_id">

                <label class="flex flex-col mt-4">
                    <span>Talla</span>
                    <select wire:model="version.size_id">
                        <option value="">seleccione una talla</option>
                        @foreach($sizes as $key=>$size)
                            <option value="{{$size->id}}">{{$size->size}}</option>
                        @endforeach
                    </select>
                    @error('version.size_id')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Seleccione un color</span>
                    <select wire:model="version.color_id">
                        <option value="">seleccione un color</option>
                        @foreach($colors as $key=>$color)
                            <option value="{{$color->id}}"
                                    style="background-color: {{"#".$color->code}}">{{$color->name}}</option>
                        @endforeach
                    </select>
                    @error('version.color_id')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Stock</span>
                    <input type="text" wire:model="version.stock">
                    @error('version.stock')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Referencia</span>
                    <input type="text" wire:model="version.reference">
                    @error('version.reference')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
            </div>

            <div class="grid grid-cols-2 gap-2">
                <label class="flex flex-col mt-4">
                    <span>Peso</span>
                    <input type="text" wire:model="version.weight">
                    @error('version.weight')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Material</span>
                    <input type="text" wire:model="version.material">
                    @error('version.material')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Bateria</span>
                    <input type="text" wire:model="version.battery">
                    @error('version.battery')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>video</span>
                    <input type="text" wire:model="version.video">
                </label>
                <label class="flex flex-col mt-4">
                    <span>video</span>
                    <select wire:model="version.type">
                        <option value="">seleccione un tipo</option>
                            <option value="version" selected="selected">version</option>
                        <option value="pack" >pack</option>
                    </select>
                    @error('version.type')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>

            </div>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="modalLanguage" method="saveLanguage">
            <x-slot name="title">Agregar datos de lenguaje para el producto {{ $selectedItem }}</x-slot>
            <label class="flex flex-col mt-4">
                <span>Descripci&oacute;n</span>
                <input type="text" wire:model="pLanguage.description">
            </label>
            <label class="flex flex-col mt-4">
                <span>Metatitulo</span>
                <input type="text" wire:model="pLanguage.meta_title">

            </label>
            <label class="flex flex-col mt-4">
                <span>MetaKey</span>
                <input type="text" wire:model="pLanguage.meta_keys">

            </label>
            <label class="flex flex-col mt-4">
                <span>MetaDescription</span>
                <input type="text" wire:model="pLanguage.meta_description">

            </label>
            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>

            </x-slot>
        </x-admin.modal>

    </div>
</div>
