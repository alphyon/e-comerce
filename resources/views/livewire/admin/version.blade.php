<div>
    <x-slot name="title">Gestionar Version</x-slot>

    <div>
        <x-admin.title title="Listar Tarifas" subtitle="listado de Tarifas" />

        <div class="mt-4">
            <x-admin.breadcrumb page="version" />
        </div>

        <div class="mt-4">
            <x-admin.alert label="Gestion de tarifas." />
        </div>

        <div class="mt-4">
            <x-admin.card icon="money-bill" title="Tabla de tarifas">

                <div class="mt-5">
                    <div class="my-3">
                        {{ $rateVersions->links() }}
                    </div>
                    <x-admin.table.table :showPerPage="false">
                        <x-slot name="head">
                            {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                            <x-admin.table.heading sortable wire:click="sortBy('id')" :direction="$sortField === 'id' ? $sortDirection : null">#</x-admin.table.heading>
                            <x-admin.table.heading sortable wire:click="sortBy('name')" :direction="$sortField === 'name' ? $sortDirection : null">Nombre</x-admin.table.heading>
                            <x-admin.table.heading sortable wire:click="sortBy('price')" :direction="$sortField === 'price' ? $sortDirection : null">Tarifa</x-admin.table.heading>
                            <x-admin.table.heading>Acciones</x-admin.table.heading>
                        </x-slot>

                        <x-slot name="body">
                            @forelse($rateVersions as $key => $rateVersion)
                                <x-admin.table.row wire:loading.class="opacity-50" >
                                    {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $rateVersion->id }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $rateVersion->rate->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{ $rateVersion->price/100 }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex space-x-3">
                                            <x-admin.button wire:click="openRateVersion({{ $rateVersion->id }})" class="bg-menuitem hover:bg-pink-400" icon="edit" />
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @empty
                                <x-admin.table.row>
                                    <x-admin.table.cell colspan="2">
                                        <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay resultados...</div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @endforelse
                        </x-slot>
                    </x-admin.table.table>

                </div>

            </x-admin.card>
        </div>
        <div class="mt-4">
            <x-admin.card icon="picture-o" title="Galeria de imagenes">
                <x-admin.button label="Añadir Imagen" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="openGallery()"/>
                <div class="mt-5">


                        <div class="grid grid-cols-5 gap-6 w-full">

                        @forelse ($galleries as $gallery)
                            <!-- card -->
                                <div class="border-t-4 border-green-600 ">
                                    <x-admin.button wire:click="openGallery('delete', {{ $gallery->id }})"
                                                    class="bg-red-500 hover:bg-pink-600" icon="trash"/>
                                    <div class="relative h-0 pb-32">
                                        <img class="absolute object-fill w-full h-full" src="{{ Storage::url($gallery->url) }}" alt="">
                                    </div>

                                </div>
                            @empty
                            <h4>no hay resultados ....</h4>
                            @endforelse

                        </div>

                </div>

            </x-admin.card>
        </div>

        <x-admin.modal wire:model="modalRateVersion" method="saveRateVersion">
            <x-slot name="title">editar tarifa</x-slot>
            <label class="flex flex-col mt-4">
                <span>Tarifa de <strong> {{$selectedItem}}</strong></span>
                <input type="text" wire:model="rateVersion.price">
                @error('rateVersion.price') <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600" icon="save" />
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="modalGallery" method="saveGallery">
            <x-slot name="title">agregar imagen </x-slot>
            <x-admin.form.image-field class="" name="picture" wire:model="picture"
                                      :path="isset($gallery->url) ? (string) url(Storage::url($gallery->url)) : 'null'" />



            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600" icon="save" />
            </x-slot>
        </x-admin.modal>
        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600"
                                icon="trash" wire:click="destroy"/>
            </x-slot>
        </x-admin.modal>
    </div>
</div>
