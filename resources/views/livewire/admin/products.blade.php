<div>

    <x-slot name="title">Products</x-slot>
    <div>
        <x-admin.title title="Listar productos" subtitle="listado de productos"/>
        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de productos"/>
        </div>
        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de productos."/>

        </div>
        <div class="mt-4">
            <x-admin.card icon="gift" title="Tabla de productos">
                <x-admin.button label="Añadir producto" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="open()"/>
                <x-admin.table.table>
                    <x-slot name="head">
                        {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                        <x-admin.table.heading sortable wire:click="sortBy('name')"
                                               :direction="$sortField === 'name' ? $sortDirection : null">Nombre
                        </x-admin.table.heading>
                        <x-admin.table.heading sortable wire:click="sortBy('family_id')"
                                               :direction="$sortField === 'family_id' ? $sortDirection : null">Familia
                        </x-admin.table.heading>
                        <x-admin.table.heading>Acciones</x-admin.table.heading>
                    </x-slot>

                    <x-slot name="body">
                        @forelse($products as $key => $product)
                            <x-admin.table.row wire:loading.class="opacity-50">
                                {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                <x-admin.table.cell>
                                    <div class="flex items-center">
                                        <div class="ml-4">
                                            <div class="flex items-center">
                                                <div class="ml-4">
                                                    <div class="text-sm font-medium text-gray-900">
                                                        {{  $product->name }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </x-admin.table.cell>
                                <x-admin.table.cell>
                                    <div class="flex items-center">
                                        <div class="ml-4">
                                            <div class="text-sm font-medium text-gray-900">
                                                {{ $product->family->name ?? "" }}
                                            </div>
                                        </div>
                                    </div>
                                </x-admin.table.cell>
                                <x-admin.table.cell>
                                    <div class="flex space-x-3">
                                        <x-admin.button label="editar" wire:click="open('edit', {{ $product->id }})"
                                                        class="bg-menuitem hover:bg-pink-400" icon="edit"/>
                                        <a href="{{ route('admin.products.manage', $product->id) }}" class="text-white bg-indigo-500 hover:bg-indigo-600 px-4 py-2">
                                            <span class="fa fa-cog"></span> gestionar
                                        </a>
                                        @if($product->active)
                                        <x-admin.button label="desactivar" wire:click="open('delete', {{ $product->id }})"
                                                        class="bg-red-500 hover:bg-pink-600" icon="times-circle"/>
                                        @else
                                            <x-admin.button label="activar" wire:click="open('delete', {{ $product->id }})"
                                                            class="bg-yellow-700 hover:bg-yellow-800" icon="redo"/>
                                        @endif
                                    </div>
                                </x-admin.table.cell>
                            </x-admin.table.row>
                        @empty
                            <x-admin.table.row>
                                <x-admin.table.cell colspan="2">
                                    <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay
                                        resultados...
                                    </div>
                                </x-admin.table.cell>
                            </x-admin.table.row>
                        @endforelse
                    </x-slot>
                </x-admin.table.table>
                <div class="my-3">
                    {{ $products->links() }}
                </div>
            </x-admin.card>
        </div>
        <x-admin.modal wire:model="modal" method="save">
            <x-slot name="title">Datos del producto</x-slot>
            <div class="grid grid-cols-2 gap-2">
                <label class="flex flex-col mt-4">
                    <span>Nombre</span>
                    <input type="text" wire:model="product.name">
                    @error('product.name')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>familia</span>
                    <select wire:model="product.family_id">
                        <option value="">seleccione una familia</option>
                        @foreach($families as $key=>$family)
                            <option value="{{$family->id}}">{{$family->name}}</option>
                        @endforeach
                    </select>
                    @error('product.family_id')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Caja</span>
                    <input type="text" wire:model="product.box">
                    @error('product.box')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>IVA</span>
                    <input type="text" wire:model="product.iva">
                    @error('product.iva')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
            </div>

            <h2 class="mt-6 text-xl">Dimensiones</h2>
            <div class="grid grid-cols-2 gap-2">
                <label class="flex flex-col mt-4">
                    <span>Alto</span>
                    <input type="text" wire:model="product.height">
                    @error('product.height')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Ancho</span>
                    <input type="text" wire:model="product.width">
                    @error('product.width')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Largo</span>
                    <input type="text" wire:model="product.large">
                    @error('product.large')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>

                @if($editAction)
                    <label class="flex flex-col mt-4">
                        <span>Descuento</span>
                        <input type="text" wire:model="product.discount">
                        {{-- @error('product.discount')--}}
                        {{-- <div class="text-red-700">{{ $message }}</div> @enderror--}}
                    </label>
                    <label class="flex flex-col mt-4">
                        <span>categoria</span>
                        <select wire:model="product.category_id">
                            <option value="">seleccione una categoria</option>
                            @foreach(\App\Models\Category::all() as $key=>$category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>

                    </label>
                    <label class="flex flex-col mt-4">
                        <span>Umbral</span>
                        <input type="text" wire:model="product.umbral">
                        @error('product.umbral')
                        <div class="text-red-700">{{ $message }}</div> @enderror
                    </label>
                @else

                    <input type="hidden"  wire:model="product.discount">
                    {{-- @error('product.discount')--}}
                    {{-- <div class="text-red-700">{{ $message }}</div> @enderror--}}


                    <input type="hidden" wire:model="product.category_id">
                    {{-- @error('product.large')--}}
                    {{-- <div class="text-red-700">{{ $message }}</div> @enderror--}}

                    <input type="hidden" wire:model="product.umbral">
                    @error('product.umbral')
                    <div class="text-red-700">{{ $message }}</div> @enderror

                @endif
            </div>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">desea realizar la operacion sobre el elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Ejecutar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="" wire:click="destroy"/>
            </x-slot>
        </x-admin.modal>
    </div>
</div>
