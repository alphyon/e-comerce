<div>
    <x-slot name="title">lista items descarga</x-slot>

    <div>
        <x-admin.title title="Listar item descargas descargas" subtitle="listado items de descargas"/>

        <div class="mt-4">
            <x-admin.breadcrumb page="items descargas"/>
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, eliminación de descargas."/>
        </div>

        <div class="mt-4">
            <x-admin.card icon="file-pdf" title="Tabla de items de descargas">
                <x-admin.button label="Añadir descarga" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="openItem"/>

                <div class="mt-5">
                    <x-admin.table.table :showPerPage="false" :showFinder="false">
                        <x-slot name="head">
                            {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                            <x-admin.table.heading >Nombre
                            </x-admin.table.heading>

                            <x-admin.table.heading>Acciones</x-admin.table.heading>
                        </x-slot>

                        <x-slot name="body">
                            @forelse($items as $key => $item)
                                <x-admin.table.row wire:loading.class="opacity-50">
                                    {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="flex-shrink-0 h-10 w-10">
                                                <img
                                                    class="h-10 w-10 rounded-full"
                                                    src="{{ Storage::url($item->file) }}"
                                                    alt=""/>
                                            </div>
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $item->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>

                                    <x-admin.table.cell>
                                        <div class="flex space-x-3">
                                            <x-admin.button wire:click="openItem({{ $item->id }},'delete')"
                                                            class="bg-red-500 hover:bg-pink-600" icon="trash"/>
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @empty
                                <x-admin.table.row>
                                    <x-admin.table.cell colspan="2">
                                        <div
                                            class="flex justify-center items-center text-xl font-bold text-gray-500">
                                            No hay resultados...
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @endforelse
                        </x-slot>
                    </x-admin.table.table>

                    {{--                                        <div class="my-3">--}}
                    {{--                                            {{ $items->links() }}--}}
                    {{--                                        </div>--}}
                </div>
            </x-admin.card>
            {{--           --}}
            <div class="{{$listOpen ?"visible":"invisible"}}">
                <x-admin.card icon="file-pdf" title="Tabla de items de descarga" class="bg-red-300">
                    <x-admin.button label="Cerrar" class="bg-menuitem hover:bg-pink-400" icon="close"
                                    wire:click="closeTable"/>


                </x-admin.card>
            </div>

        </div>



        <x-admin.modal wire:model="modalItem" method="saveItem">
            <x-slot name="title">Datos de item</x-slot>
            <label class="flex flex-col mt-4">
                <span>Nombre</span>
                <input type="text" wire:model="itemDownload.name">
                @error('itemDownload.name')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <label class="flex flex-col mt-4">
                <span>Reference</span>
                <input type="text" wire:model="itemDownload.reference">
                @error('itemDownload.reference')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>

            <x-admin.form.image-field class="" name="picture2" wire:model="picture2"
                                      :path="url(Storage::url($picture2))"/>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="deleteModalItem">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600"
                                icon="trash" wire:click="destroyItem"/>
            </x-slot>
        </x-admin.modal>


    </div>
</div>
