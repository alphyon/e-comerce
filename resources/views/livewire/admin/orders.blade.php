<div>
    <x-slot name="title">Orders</x-slot>

    <div>
        <x-admin.title title="Ordenar productos" subtitle="listado para ordenamiento de productos" />

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de ordenes" />
        </div>


        <div class="mt-4">

             <x-admin.card icon="sitemap" title="">
                 <div>
                     <div class="mb-8">
                         <label class="inline-block w-32 font-bold">Familia</label>
                         <select name="country" wire:model="family" class="border shadow p-2 bg-white">
                             <option value=''>Selecciona una familia</option>
                             @foreach($families as $family)
                                 <option value={{ $family->id }}>{{ $family->name }}</option>
                             @endforeach
                         </select>
                     </div>
                     <x-admin.table.table>
                         <x-slot name="head">
                             {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                             <x-admin.table.heading >Nombre
                             </x-admin.table.heading>
                             <x-admin.table.heading colspan="2">Caracteristicas
                             </x-admin.table.heading>
                             <x-admin.table.heading>Orden</x-admin.table.heading>
                         </x-slot>
                         <x-slot name="body">
                             @forelse($versions as $key => $version)
                                 <x-admin.table.row wire:loading.class="opacity-50">
                                     {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                     <x-admin.table.cell>
                                         <div class="flex items-center">
                                             <div class="ml-4">
                                                 <div class="flex items-center">
                                                     <div class="ml-4">
                                                         <div class="text-sm font-medium text-gray-900">
                                                             {{  $version->product->name }}


                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </x-admin.table.cell>
                                     <x-admin.table.cell>
                                         <div class="flex items-center">
                                             <div class="ml-4">
                                                 <div class="text-sm font-medium text-gray-900">
                                                     {{$version->size->size}} Color: {{$version->color->name}}
                                                     <div class="w-5 h-5" style="background-color: {{'#'.$version->color->code}}"></div>
                                                 </div>
                                             </div>
                                         </div>
                                     </x-admin.table.cell>
                                     <x-admin.table.cell>
                                         <div class="flex items-center">
                                         <img
                                             class="h-10 w-10 rounded-full"
                                             src="{{ Storage::url($version->photo) }}"
                                             alt="" />
                                         </div>

                                     </x-admin.table.cell>
                                     <x-admin.table.cell >
                                         @if(!$statusChange)
                                         <div class="flex items-center"  wire:click="changeStatus({{$version->id}},{{$key}})">
                                            {{$version->sort}}
                                         </div>
                                         @else

                                             @if($key == $keyActive)
                                             <div class="flex space-x-2">
                                             <input type="number" class="w-20" min="0" wire:model="version.sort" wire:change.debounce.1s="update({{$version->id}})">


                                             <x-admin.button wire:click="initialState" class="bg-red-600 hover:bg-red-700" icon="times-circle" />
                                             </div>
                                             @else
                                                 <div class="flex items-center"  wire:click="changeStatus({{$version->id}},{{$key}})">
                                                     {{$version->sort}}
                                                 </div>
                                             @endif
                                         @endif

                                     </x-admin.table.cell>
                                 </x-admin.table.row>
                             @empty
                                 <x-admin.table.row>
                                     <x-admin.table.cell colspan="2">
                                         <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay
                                             resultados...
                                         </div>
                                     </x-admin.table.cell>
                                 </x-admin.table.row>
                             @endforelse
                         </x-slot>
                     </x-admin.table.table>
                     <div class="my-3">
                         {{ $versions == []?"":$versions->links() }}
                     </div>

            </x-admin.card>
        </div>
    </div>
</div>
