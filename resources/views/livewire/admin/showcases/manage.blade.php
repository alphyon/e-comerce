<div>
    <x-slot name="title">lista items exhibidore</x-slot>

    <div>
        <x-admin.title title="Listar item exhibidores " subtitle="listado items de exhibidores"/>

        <div class="mt-4">
            <x-admin.breadcrumb page="items exhibidores"/>
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, eliminación de exhibidores."/>
        </div>

        <div class="mt-4">
            <x-admin.card icon="file-pdf" title="Tabla de items de exhibidores">
                <x-admin.button label="Añadir item a exhibidor" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="openItem"/>

                <div class="mt-5">


                    <div class="grid grid-cols-5 gap-6 w-full">

                    @forelse ($items as $item)
                        <!-- card -->
                            <div class="border-t-4 border-green-600 ">
                                <x-admin.button wire:click="openItem({{ $item->id }},'delete')"
                                                class="bg-red-500 hover:bg-pink-600" icon="trash"/>
                                <div class="relative h-0 pb-32">
                                    <img class="absolute object-fill w-full h-full" src="{{ Storage::url($item->image) }}" alt="">
                                </div>

                            </div>
                        @empty
                            <h4>no hay resultados ....</h4>
                        @endforelse

                    </div>


                </div>
            </x-admin.card>
            {{--           --}}
            <div class="{{$listOpen ?"visible":"invisible"}}">
                <x-admin.card icon="file-pdf" title="Tabla de items de exhibidor" class="bg-red-300">
                    <x-admin.button label="Cerrar" class="bg-menuitem hover:bg-pink-400" icon="close"
                                    wire:click="closeTable"/>


                </x-admin.card>
            </div>

        </div>



        <x-admin.modal wire:model="modalItem" method="saveItem">
            <x-slot name="title">Datos de item</x-slot>


            <x-admin.form.image-field class="" name="picture2" wire:model="picture2"
                                      :path="url(Storage::url($picture2))"/>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="deleteModalItem">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600"
                                icon="trash" wire:click="destroyItem"/>
            </x-slot>
        </x-admin.modal>


    </div>
</div>


