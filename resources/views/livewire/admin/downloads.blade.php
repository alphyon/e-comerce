<div>
    <x-slot name="title">Downloads</x-slot>

    <div>
        <x-admin.title title="Listar descargas" subtitle="listado de descargas"/>

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de descargas"/>
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de descargas."/>
        </div>

        <div class="mt-4">
            <x-admin.card icon="file-pdf" title="Tabla de descargas">
                <x-admin.button label="Añadir descarga" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="open"/>

                <div class="mt-5">
                    <x-admin.table.table>
                        <x-slot name="head">
                            {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                            <x-admin.table.heading sortable wire:click="sortBy('name')"
                                                   :direction="$sortField === 'name' ? $sortDirection : null">Nombre
                            </x-admin.table.heading>
                            <x-admin.table.heading sortable wire:click="sortBy('family_id')"
                                                   :direction="$sortField === 'family_id' ? $sortDirection : null">
                                Familia
                            </x-admin.table.heading>
                            <x-admin.table.heading>Acciones</x-admin.table.heading>
                        </x-slot>

                        <x-slot name="body">
                            @forelse($downloads as $key => $download)
                                <x-admin.table.row wire:loading.class="opacity-50">
                                    {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="flex-shrink-0 h-10 w-10">
                                                <img
                                                    class="h-10 w-10 rounded-full"
                                                    src="{{ Storage::url($download->image) }}"
                                                    alt=""/>
                                            </div>
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $download->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $download->family->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex space-x-3">
                                            <x-admin.button wire:click="openItem( {{ $download->id }})"
                                                            class="bg-menuitem hover:bg-pink-400" icon="plus"/>
                                            <a href="{{ route("admin.downloads.items",$download->id) }}" class="text-white bg-indigo-500 hover:bg-indigo-600 px-4 py-2">
                                                <span class="fa fa-list"></span> Lista
                                            </a>
                                            <x-admin.button wire:click="open('delete', {{ $download->id }})"
                                                            class="bg-red-500 hover:bg-pink-600" icon="trash"/>
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @empty
                                <x-admin.table.row>
                                    <x-admin.table.cell colspan="2">
                                        <div class="flex justify-center items-center text-xl font-bold text-gray-500">No
                                            hay resultados...
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @endforelse
                        </x-slot>
                    </x-admin.table.table>

                    <div class="my-3">
                        {{ $downloads->links() }}
                    </div>
                </div>
            </x-admin.card>


        </div>
        <x-admin.modal wire:model="modal" method="save">
            <x-slot name="title">Datos de descarga</x-slot>

            <label class="flex flex-col mt-4">
                <span>Familia</span>
                <select wire:model="subFamily.family_id">
                    <option value="">seleccione una familia</option>
                    @foreach($families as $key=>$family)
                        <option value="{{$family->id}}">{{$family->name}}</option>
                    @endforeach
                </select>
                @error('product.family_id')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <label class="flex flex-col mt-4">
                <span>Nombre</span>
                <input type="text" wire:model="subFamily.name">
                @error('Subfamily.name')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>


            <x-admin.form.image-field class="" name="picture" wire:model="picture"
                                      :path="url(Storage::url($picture))"/>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>


        <x-admin.modal wire:model="modalItem" method="saveItem">
            <x-slot name="title">Datos de item</x-slot>
            <label class="flex flex-col mt-4">
                <span>Nombre</span>
                <input type="text" wire:model="itemDownload.name">
                @error('itemDownload.name')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <label class="flex flex-col mt-4">
                <span>Reference</span>
                <input type="text" wire:model="itemDownload.reference">
                @error('itemDownload.reference')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>

            <x-admin.form.image-field class="" name="picture2" wire:model="picture2"
                                      :path="url(Storage::url($picture2))"/>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>


        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <h1 class="text-red-800 text-4xl"> Al confirmar esta accion se eliminaran tambien todos los items
            de la desacarga que esten asociados
            </h1>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600"
                                icon="trash" wire:click="destroy"/>
            </x-slot>
        </x-admin.modal>




    </div>
</div>
