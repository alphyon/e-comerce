<div>
    <x-slot name="title">Colors</x-slot>

    <div>
        <x-admin.title title="Listar colores" subtitle="listado de colores"/>

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de colores"/>
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de colores."/>
        </div>

        <div class="mt-4">
            <x-admin.card icon="tint" title="Tabla de colores">
                <x-admin.button label="Añadir color" class="bg-menuitem hover:bg-pink-400" icon="plus"
                                wire:click="open()"/>

                <x-admin.table.table>
                    <x-slot name="head">
                        {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                        <x-admin.table.heading sortable wire:click="sortBy('name')"
                                               :direction="$sortField === 'name' ? $sortDirection : null">Color
                        </x-admin.table.heading>
                        <x-admin.table.heading sortable wire:click="sortBy('code')"
                                               :direction="$sortField === 'code' ? $sortDirection : null">C&oacute;digo
                        </x-admin.table.heading>
                        <x-admin.table.heading>Acciones</x-admin.table.heading>
                    </x-slot>

                    <x-slot name="body">
                        @forelse($colors as $key => $color)
                            <x-admin.table.row wire:loading.class="opacity-50">
                                {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                <x-admin.table.cell>
                                    <div class="flex items-center">

                                        <div class="ml-4">
                                            <div class="flex items-center">
                                                <div class="ml-2">
                                                    <div
                                                        style="background-color: {{'#'.$color->code}}; width: 10px; height: 10px">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="ml-4">
                                                    <div class="text-sm font-medium text-gray-900">

                                                        {{  $color->name }}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </x-admin.table.cell>
                                <x-admin.table.cell>
                                    <div class="flex items-center">


                                        <div class="ml-4">
                                            <div class="text-sm font-medium text-gray-900">

                                                {{  $color->code }}
                                            </div>
                                        </div>
                                    </div>
                                </x-admin.table.cell>
                                <x-admin.table.cell>
                                    <div class="flex space-x-3">
                                        <x-admin.button wire:click="open('edit', {{ $color->id }})"
                                                        class="bg-menuitem hover:bg-pink-400" icon="edit"/>
                                        <x-admin.button wire:click="open('delete', {{ $color->id }})"
                                                        class="bg-red-500 hover:bg-pink-600" icon="trash"/>
                                    </div>
                                </x-admin.table.cell>
                            </x-admin.table.row>
                        @empty
                            <x-admin.table.row>
                                <x-admin.table.cell colspan="2">
                                    <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay
                                        resultados...
                                    </div>
                                </x-admin.table.cell>
                            </x-admin.table.row>
                        @endforelse
                    </x-slot>
                </x-admin.table.table>
                <div class="my-3">
                    {{ $colors->links() }}
                </div>
            </x-admin.card>
        </div>
        <x-admin.modal wire:model="modal" method="save">
            <x-slot name="title">Datos de color</x-slot>

            <label class="flex flex-col mt-4">
                <span>Nombre</span>
                <input type="text" wire:model="color.name">
                @error('color.name')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <label class="flex flex-col mt-4">
                <span>C&oacute;digo</span>
                <input type="color"  wire:model="color.code">
                @error('color.code')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>
        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600"
                                icon="trash" wire:click="destroy"/>
            </x-slot>
        </x-admin.modal>

    </div>
</div>
