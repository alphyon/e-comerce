<div>
    <x-slot name="title">Configuraciones</x-slot>

    <div>
        <x-admin.title title="Listar configuraciones" subtitle="listado de configuraciones" />

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de configuraciones" />
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de configuraciones." />
        </div>

        <div class="mt-4">
            <x-admin.card icon="sitemap" title="Tabla de Configuraciones">
                <x-admin.button label="Añadir Configuración" class="bg-menuitem hover:bg-pink-400" icon="plus" wire:click="open()" />

                <div class="mt-5">
                    <x-admin.table.table>
                        <x-slot name="head">
                            {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                            <x-admin.table.heading sortable wire:click="sortBy('name')" :direction="$sortField === 'name' ? $sortDirection : null">Nombre</x-admin.table.heading>
                            <x-admin.table.heading>valor</x-admin.table.heading>
                            <x-admin.table.heading>complemento</x-admin.table.heading>
                            <x-admin.table.heading>Acciones</x-admin.table.heading>
                        </x-slot>

                        <x-slot name="body">
                            @forelse($configurations as $key => $config)
                                <x-admin.table.row wire:loading.class="opacity-50" >
                                    {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $config->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="mr-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $config->value }}
                                                </div>
                                            </div>

                                            @if(preg_match('/^.*\.(jpg|jpeg|png|gif)$/i',$config->value))

                                            <div class="flex-shrink-0 h-10 w-10">
                                                <img
                                                    class="h-10 w-10 rounded-full"
                                                    src="{{ Storage::url($config->value) }}"
                                                    alt="" />
                                            </div>
                                            @endif
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell >
                                        <div class="flex items-center">

                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $config->complement }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex space-x-3">
                                            <x-admin.button wire:click="open('edit', {{ $config->id }})" class="bg-menuitem hover:bg-pink-400" icon="edit" />
                                            <x-admin.button wire:click="open('delete', {{ $config->id }})" class="bg-red-500 hover:bg-pink-600" icon="trash" />
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @empty
                                <x-admin.table.row>
                                    <x-admin.table.cell colspan="2">
                                        <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay resultados...</div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @endforelse
                        </x-slot>
                    </x-admin.table.table>

                    <div class="my-3">
                        {{ $configurations->links() }}
                    </div>
                </div>

            </x-admin.card>
        </div>

        <x-admin.modal wire:model="modal" method="save">
            <x-slot name="title">Datos de la configuracion</x-slot>



            <label class="flex flex-col mt-4">
                <span>Nombre</span>
                <input type="text" wire:model="config.name">
                @error('config.name')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <label class="flex flex-col mt-4">
                <input type="checkbox" wire:click="checkData"> cargar imagen para el valor?
                <span>valor</span>
                @if($imgValue)
                <x-admin.form.image-field class="" name="picture" wire:model="picture"
                                          :path="url(Storage::url($picture))" />
                    <input type="hidden" wire:model="config.value">
                @else
                <input type="text" wire:model="config.value">
                @error('config.value')
                <div class="text-red-700">{{ $message }}</div> @enderror
                @endif
            </label>
            <label class="flex flex-col mt-4">
                <span>Complemento</span>
                <input type="text" wire:model="config.complement">
                @error('config.complement')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </label>
            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600" icon="save" />
            </x-slot>
        </x-admin.modal>

        <x-admin.modal wire:model="deleteModal">
            <x-slot name="title">Eliminar ese elemento {{ $selectedItem }}</x-slot>
            <x-slot name="action">
                <x-admin.button type="button" label="Eliminar" class="ml-4 rounded bg-red-500 hover:bg-pink-600" icon="trash" wire:click="destroy" />
            </x-slot>
        </x-admin.modal>
    </div>
</div>
