<div>
    <x-slot name="title">Request</x-slot>

    <div>
        <x-admin.title title="Listar pedidos" subtitle="listado de pedidos" />

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de pedidos" />
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de pedidos." />
        </div>

        <div class="mt-4">
            <x-admin.card icon="level-up-alt" title="Tabla de pedidos">
                <x-admin.button label="Añadir pedidos" class="bg-menuitem hover:bg-pink-400" icon="plus" />

                {{-- Table here --}}
            </x-admin.card>
        </div>
    </div>
</div>
