<div>
    <x-slot name="title">Users</x-slot>

    <div>
        <x-admin.title title="Listar usuarios" subtitle="listado de usuarios" />

        <div class="mt-4">
            <x-admin.breadcrumb page="Listar de usuarios" />
        </div>

        <div class="mt-4">
            <x-admin.alert label="Creación, edición y eliminación de usuarios." />
        </div>

        <div class="mt-4">
            <x-admin.card icon="users" title="Tabla de usuarios">

                <div class="mt-5">
                    <x-admin.table.table>
                        <x-slot name="head">
                            {{-- <x-admin.table.heading sortable>#</x-admin.table.heading> --}}
                            <x-admin.table.heading sortable wire:click="sortBy('name')" :direction="$sortField === 'name' ? $sortDirection : null">Nombre</x-admin.table.heading>
                            <x-admin.table.heading>Complemento</x-admin.table.heading>
                            <x-admin.table.heading>Email</x-admin.table.heading>
                            <x-admin.table.heading>Tarifa</x-admin.table.heading>
                            <x-admin.table.heading>Factura</x-admin.table.heading>
                            <x-admin.table.heading>IVA</x-admin.table.heading>
                            <x-admin.table.heading>Acciones</x-admin.table.heading>
                        </x-slot>

                        <x-slot name="body">
                            @forelse($profiles as $key => $profile)

                                <x-admin.table.row wire:loading.class="opacity-50" class="{{$profile->user->status? '': 'bg-red-300'}}">

                                    {{-- <x-admin.table.cell>{{ $key }}</x-admin.table.cell> --}}
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $profile->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $profile->name_complement }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $profile->user->email }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{  $profile->rate->name }}
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    @if($profile->bill)
                                                        <x-admin.button wire:click="changeStatus('bill', {{ $profile->id }})" label="si" class="bg-green-500 hover:bg-green-400" icon="check" />

                                                    @else
                                                        <x-admin.button wire:click="changeStatus('bill', {{ $profile->id }})" label="no" class="bg-red-500 hover:bg-red-600" icon="times-circle" />

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                     @if($profile->iva)
                                                        <x-admin.button wire:click="changeStatus('IVA', {{ $profile->id }})" label="si" class="bg-green-500 hover:bg-green-400" icon="check" />

                                                    @else
                                                        <x-admin.button wire:click="changeStatus('IVA', {{ $profile->id }})" label="no" class="bg-red-500 hover:bg-red-600" icon="times-circle" />

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </x-admin.table.cell>
                                    <x-admin.table.cell>
                                        <div class="flex space-x-3">
                                            <x-admin.button label="tarifa" wire:click="openRate({{ $profile->id }})" class="bg-menuitem hover:bg-pink-400 text-xs" icon="money-bill" />
                                            <x-admin.button label="familias" wire:click="openManageFamily({{ $profile->user_id }})" class="bg-yellow-700 hover:bg-yellow-800 text-xs" icon="users" />
                                            <x-admin.button label="editar" wire:click="open('edit', {{ $profile->id }})" class="bg-menuitem hover:bg-pink-400 text-xs" icon="edit" />

                                            @if($profile->user->status)
                                            <x-admin.button label="desactivar" wire:click="open('delete', {{ $profile->id }})" class="bg-red-500 hover:bg-red-600 text-xs m-0" icon="times-circle" />
                                            @else
                                                <x-admin.button label="activar" wire:click="open('delete', {{ $profile->id }})" class="bg-yellow-800 hover:bg-yellow-700 text-xs" icon="redo" />

                                            @endif
                                        </div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @empty
                                <x-admin.table.row>
                                    <x-admin.table.cell colspan="2">
                                        <div class="flex justify-center items-center text-xl font-bold text-gray-500">No hay resultados...</div>
                                    </x-admin.table.cell>
                                </x-admin.table.row>
                            @endforelse
                        </x-slot>
                    </x-admin.table.table>

                    <div class="my-3">
                        {{ $profiles->links() }}
                    </div>
                </div>


            </x-admin.card>
        </div>
        <x-admin.modal wire:model="deleteModal" >
            <x-slot name="title">Eliminar usuario {{ $selectedItem }}</x-slot>
            <x-slot name="action">

                <x-admin.button type="button" label="Ejecutar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600" icon="" wire:click="changeUserStatus" />

            </x-slot>
        </x-admin.modal>


        <x-admin.modal wire:model="modal" method="save">
            <x-slot name="title">Datos del producto</x-slot>
            <div class="grid grid-cols-2 gap-2">
                <label class="flex flex-col mt-4">
                    <span>Nombre</span>
                    <input type="text" wire:model="profile.name">
                    @error('profile.name')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>

                <label class="flex flex-col mt-4">
                    <span>Complemento</span>
                    <input type="text" wire:model="profile.name_complement">
                    @error('profile.name_complement')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Email</span>
                    <input type="text" disabled="disabled" wire:model="email" class="text-gray-500 bg-gray-300 hover:bg-gray-300 outline-none">

                </label>
            </div>


            <div class="grid grid-cols-2 gap-2">
                <label class="flex flex-col mt-4">
                    <span>Password</span>
                    <input type="password" wire:model="password">
                    @error('password')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
                <label class="flex flex-col mt-4">
                    <span>Confirmación Password </span>
                    <input type="password" wire:model="password_confirmation">
                    @error('password_confirmation')
                    <div class="text-red-700">{{ $message }}</div> @enderror
                </label>
            </div>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>
        <x-admin.modal wire:model="modalChangeRate" method="saveRate">
            <x-slot name="title">Cambio de Tarifa</x-slot>
            <div class="grid grid-cols-1">
                <span>Tarifa</span>
                <select wire:model="profile.rate_id">
                    <option value="">seleccione la tarifa</option>
                    @foreach($rates as $key=>$rate)
                        <option value="{{$rate->id}}">{{$rate->name}}</option>
                    @endforeach
                </select>
                @error('profile.rate_id')
                <div class="text-red-700">{{ $message }}</div> @enderror
            </div>

            <x-slot name="action">
                <x-admin.button type="submit" label="Guardar" class="ml-4 rounded bg-blue-500 hover:bg-blue-600"
                                icon="save"/>
            </x-slot>
        </x-admin.modal>
        <x-admin.modal wire:model="modalFamilyManage">
            <x-slot name="title">Gestionar Familias</x-slot>
            <div class="grid grid-cols-1">
                @foreach($families as $key=>$family)
                    <label class="flex space-x-2 inline-block">
                        @if($family->permission)
                            <input type="checkbox" checked="checked" wire:click="changePermission({{$family->id}})">
                        @else
                            <input type="checkbox" wire:click="changePermission({{$family->id}})">
                        @endif

                        <span>{{$family->family->name}}</span>
                    </label>
                @endforeach

            </div>
            <x-slot name="action">

            </x-slot>
        </x-admin.modal>

    </div>
</div>
