<?php

namespace Tests\Unit;

use App\Utils\ManagerFTPFiles;
use Tests\TestCase;

class FTPControllerTest extends TestCase
{

    public function test_dir(){
        $ftp = new ManagerFTPFiles();
        $result = $ftp->getFileFromFTP("ARTICULOS","ConnectPlain/devtest");
        $this->assertNotNull($result);
    }

    public function test_dir_null(){
        $ftp = new ManagerFTPFiles();
        $result = $ftp->getFileFromFTP("ARTICULOS","ConnectPlain/devtes");
        $this->assertNull($result);
    }
}
