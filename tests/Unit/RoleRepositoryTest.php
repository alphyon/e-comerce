<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Role;
use App\Repositories\RoleRepository;
use Tests\TestCase;

class RoleRepositoryTest extends TestCase
{
    public function testCreateRole()
    {
        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->slug(1)
        ];
        $repo = new RoleRepository(new Role());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['slug'], $dataRepo->slug);
    }

    public function testCreateRoleException()
    {
        $this->expectException(CreateException::class);
        $repo = new RoleRepository(new Role());
        $repo->create([]);
    }

    public function testGetRole()
    {
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $data = $repo->find($role->id);
        $this->assertEquals($role->name, $data->name);
        $this->assertEquals($role->slug, $data->slug);
    }
    public function testGetRoleException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RoleRepository(new Role());
        $repo->find(999);
    }

    public function testGetRoleByParam()
    {
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $data = $repo->findByParam('name',$role->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($role->name, $data[0]->name);
        $this->assertEquals($role->slug, $data[0]->slug);
    }

    public function testGetRoleByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RoleRepository(new Role());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateRole(){
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($role->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateRoleException1(){
        $this->expectException(UpdateException::class);
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateRoleException2(){
        $this->expectException(UpdateException::class);
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $data = [
            'name'=>null
        ];
        $repo->update($role->id,$data);
    }

    public function testDeleteRole()
    {
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $delete = $repo->delete($role->id);
        $this->assertTrue($delete);
    }
    public function testDeleteRoleException()
    {
        $this->expectException(DeleteException::class);
        $repo = new RoleRepository(new Role());
        $repo->delete(999);
    }

    public function testGetRoleMultiParam(){
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $data = $repo->findByMultiParams([
            'name'=>$role->name,
            'slug'=>$role->slug
        ]);
        $this->assertEquals($data[0]->name,$role->name);
        $this->assertEquals($data[0]->slug,$role->slug);
    }

    public function testGetRoleMultiParamException(){
        $this->expectException(NotFoundException::class);
        $role = Role::factory()->create();
        $repo = new RoleRepository(new Role());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'slug'=>$role->slug
        ]);
    }

    public function testGetAllRoles(){
        $roles = Role::factory()->times(10)->create();
        $repo = new RoleRepository(new Role());
        $data = $repo->all();
        $this->assertEquals($roles->count(),$data->count());
    }
    public function testGetAllRolesException(){
        $this->expectException(NotFoundException::class);
        $repo = new RoleRepository(new Role());
        $repo->all();
    }
    public function testGetAllRolesPaginate(){
        Role::factory()->times(10)->create();
        $repo = new RoleRepository(new Role());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllRolesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new RoleRepository(new Role());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
