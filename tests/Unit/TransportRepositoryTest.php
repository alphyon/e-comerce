<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Transport;
use App\Repositories\TransportRepository;
use Tests\TestCase;

class TransportRepositoryTest extends TestCase
{
    public function testCreateTransport()
    {
        $data = [
            'name' => $this->faker->name,
            'photo' => $this->faker->imageUrl()
        ];
        $repo = new TransportRepository(new Transport());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['photo'], $dataRepo->photo);
    }

    public function testCreateTransportException()
    {
        $this->expectException(CreateException::class);
        $repo = new TransportRepository(new Transport());
        $repo->create([]);
    }

    public function testGetTransport()
    {
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $data = $repo->find($transport->id);
        $this->assertEquals($transport->name, $data->name);
        $this->assertEquals($transport->photo, $data->photo);
    }
    public function testGetTransportException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new TransportRepository(new Transport());
        $repo->find(999);
    }

    public function testGetTransportByParam()
    {
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $data = $repo->findByParam('name',$transport->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($transport->name, $data[0]->name);
        $this->assertEquals($transport->photo, $data[0]->photo);
    }

    public function testGetTransportByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new TransportRepository(new Transport());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateTransport(){
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($transport->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateTransportException1(){
        $this->expectException(UpdateException::class);
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateTransportException2(){
        $this->expectException(UpdateException::class);
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $data = [
            'name'=>null
        ];
        $repo->update($transport->id,$data);
    }

    public function testDeleteTransport()
    {
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $delete = $repo->delete($transport->id);
        $this->assertTrue($delete);
    }
    public function testDeleteTransportException()
    {
        $this->expectException(DeleteException::class);
        $repo = new TransportRepository(new Transport());
        $repo->delete(999);
    }

    public function testGetTransportMultiParam(){
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $data = $repo->findByMultiParams([
            'name'=>$transport->name,
            'photo'=>$transport->photo
        ]);
        $this->assertEquals($data[0]->name,$transport->name);
        $this->assertEquals($data[0]->photo,$transport->photo);
    }

    public function testGetTransportMultiParamException(){
        $this->expectException(NotFoundException::class);
        $transport = Transport::factory()->create();
        $repo = new TransportRepository(new Transport());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'photo'=>$transport->photo
        ]);
    }

    public function testGetAllTransports(){
        $transports = Transport::factory()->times(10)->create();
        $repo = new TransportRepository(new Transport());
        $data = $repo->all();
        $this->assertEquals($transports->count(),$data->count());
    }
    public function testGetAllTransportsException(){
        $this->expectException(NotFoundException::class);
        $repo = new TransportRepository(new Transport());
        $repo->all();
    }
    public function testGetAllTransportsPaginate(){
        Transport::factory()->times(10)->create();
        $repo = new TransportRepository(new Transport());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllTransportsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new TransportRepository(new Transport());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
