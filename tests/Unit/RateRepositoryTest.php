<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Rate;
use App\Repositories\RateRepository;
use Tests\TestCase;

class RateRepositoryTest extends TestCase
{
    public function testCreateRate()
    {
        $data = [
            'name' => $this->faker->name,
            'connector_id' => $this->faker->randomDigit
        ];
        $repo = new RateRepository(new Rate());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['connector_id'], $dataRepo->connector_id);
    }

    public function testCreateRateException()
    {
        $this->expectException(CreateException::class);
        $repo = new RateRepository(new Rate());
        $repo->create([]);
    }

    public function testGetRate()
    {
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $data = $repo->find($rate->id);
        $this->assertEquals($rate->name, $data->name);
        $this->assertEquals($rate->connector_id, $data->connector_id);
    }
    public function testGetRateException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RateRepository(new Rate());
        $repo->find(999);
    }

    public function testGetRateByParam()
    {
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $data = $repo->findByParam('name',$rate->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($rate->name, $data[0]->name);
        $this->assertEquals($rate->connector_id, $data[0]->connector_id);
    }

    public function testGetRateByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RateRepository(new Rate());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateRate(){
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($rate->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateRateException1(){
        $this->expectException(UpdateException::class);
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateRateException2(){
        $this->expectException(UpdateException::class);
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $data = [
            'name'=>null
        ];
        $repo->update($rate->id,$data);
    }

    public function testDeleteRate()
    {
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $delete = $repo->delete($rate->id);
        $this->assertTrue($delete);
    }
    public function testDeleteRateException()
    {
        $this->expectException(DeleteException::class);
        $repo = new RateRepository(new Rate());
        $repo->delete(999);
    }

    public function testGetRateMultiParam(){
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $data = $repo->findByMultiParams([
            'name'=>$rate->name,
            'connector_id'=>$rate->connector_id
        ]);
        $this->assertEquals($data[0]->name,$rate->name);
        $this->assertEquals($data[0]->connector_id,$rate->connector_id);
    }

    public function testGetRateMultiParamException(){
        $this->expectException(NotFoundException::class);
        $rate = Rate::factory()->create();
        $repo = new RateRepository(new Rate());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'connector_id'=>$rate->connector_id
        ]);
    }

    public function testGetAllRates(){
        $rates = Rate::factory()->times(10)->create();
        $repo = new RateRepository(new Rate());
        $data = $repo->all();
        $this->assertEquals($rates->count(),$data->count());
    }
    public function testGetAllRatesException(){
        $this->expectException(NotFoundException::class);
        $repo = new RateRepository(new Rate());
        $repo->all();
    }
    public function testGetAllRatesPaginate(){
        Rate::factory()->times(10)->create();
        $repo = new RateRepository(new Rate());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllRatesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new RateRepository(new Rate());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
