<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Role;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    public function testCreateUser()
    {
        $data = [
            'role_id' => Role::factory()->create()->id,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
        $repo = new UserRepository(new User());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['email'], $dataRepo->email);
        $this->assertEquals($data['role_id'], $dataRepo->role_id);
    }

    public function testCreateUserException()
    {
        $this->expectException(CreateException::class);
        $repo = new UserRepository(new User());
        $repo->create([]);
    }

    public function testGetUser()
    {
        $User = User::factory()->create();
        $repo = new UserRepository(new User());
        $data = $repo->find($User->id);
        $this->assertEquals($User->name, $data->name);
        $this->assertEquals($User->slug, $data->slug);
    }

    public function testGetUserException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new UserRepository(new User());
        $repo->find(999);
    }

    public function testGetUserByParam()
    {
        $user = User::factory()->create();
        $repo = new UserRepository(new User());
        $data = $repo->findByParam('email', $user->email);
        $this->assertGreaterThan(0, $data->count());
        $this->assertEquals($user->role_id, $data[0]->role_id);
        $this->assertEquals($user->email, $data[0]->email);
    }

    public function testGetUserByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new UserRepository(new User());
        $repo->findByParam('email', $this->faker->word);
    }

    public function testUpdateUser()
    {
        $user = User::factory()->create();
        $repo = new UserRepository(new User());
        $data = [
            'email' => $this->faker->safeEmail
        ];
        $update = $repo->update($user->id, $data);
        $this->assertEquals($update->email, $data['email']);
    }

    public function testUpdateUserException1()
    {
        $this->expectException(UpdateException::class);
        $User = User::factory()->create();
        $repo = new UserRepository(new User());
        $data = [
            'email' => $this->faker->safeEmail
        ];
        $update = $repo->update(999, $data);
    }

    public function testUpdateUserException2()
    {
        $this->expectException(UpdateException::class);
        $user = User::factory()->create();
        $repo = new UserRepository(new User());
        $data = [
            'email' => null
        ];
        $repo->update($user->id, $data);
    }

    public function testDeleteUser()
    {
        $user = User::factory()->create();
        $repo = new UserRepository(new User());
        $delete = $repo->delete($user->id);
        $this->assertTrue($delete);
    }

    public function testDeleteUserException()
    {
        $this->expectException(DeleteException::class);
        $repo = new UserRepository(new User());
        $repo->delete(999);
    }

    public function testGetUserMultiParam()
    {
        $user = User::factory()->create();
        $repo = new UserRepository(new User());
        $data = $repo->findByMultiParams([
            'email' => $user->email,
            'role_id' => $user->role_id
        ]);
        $this->assertEquals($data[0]->email, $user->email);
        $this->assertEquals($data[0]->role_id, $user->role_id);
    }

    public function testGetUserMultiParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new UserRepository(new User());
        $repo->findByMultiParams([
            'email' => $this->faker->email,
            'role_id' => $this->faker->randomDigit
        ]);
    }

    public function testGetAllUsers()
    {
        $users = User::factory()->times(10)->create();
        $repo = new UserRepository(new User());
        $data = $repo->all();
        $this->assertEquals($users->count(), $data->count());
    }

    public function testGetAllUsersException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new UserRepository(new User());
        $repo->all();
    }

    public function testGetAllUsersPaginate()
    {
        User::factory()->times(10)->create();
        $repo = new UserRepository(new User());
        $testNumber = 5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber, $data->count());
    }

    public function testGetAllUsersPaginateException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new UserRepository(new User());
        $testNumber = 5;
        $repo->allPaginate($testNumber);
    }
}
