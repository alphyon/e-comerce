<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\Models\Version;
use App\Repositories\VersionRepository;
use Tests\TestCase;

class VersionRepositoryTest extends TestCase
{
    public function testCreateVersion()
    {
        $data = [
            'product_id'=>Product::factory()->create()->id,
            'size_id'=>Size::factory()->create()->id,
            'color_id'=>Color::factory()->create()->id,
            'stock'=>$this->faker->numberBetween(10,100),
            'weight'=>$this->faker->numberBetween(1000,3000),
            'photo'=>$this->faker->imageUrl(),
            'active'=>$this->faker->boolean,
            'reference'=>$this->faker->numerify('#####'),
            'type'=>$this->faker->randomElement(['pack','version']),
            'battery'=>$this->faker->slug,
            'material'=>$this->faker->slug,
            'video'=>$this->faker->imageUrl(),
            'sort'=>$this->faker->randomDigit,
        ];
        $repo = new VersionRepository(new Version());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['type'], $dataRepo->type);
        $this->assertEquals($data['material'], $dataRepo->material);
    }

    public function testCreateVersionException()
    {
        $this->expectException(CreateException::class);
        $repo = new VersionRepository(new Version());
        $repo->create([]);
    }

    public function testGetVersion()
    {
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $data = $repo->find($version->id);
        $this->assertEquals($version->type, $data->type);
        $this->assertEquals($version->battery, $data->battery);
    }
    public function testGetVersionException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new VersionRepository(new Version());
        $repo->find(999);
    }

    public function testGetVersionByParam()
    {
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $data = $repo->findByParam('type',$version->type);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($version->battery, $data[0]->battery);
        $this->assertEquals($version->material, $data[0]->material);
    }

    public function testGetVersionByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new VersionRepository(new Version());
        $repo->findByParam('type',$this->faker->word);
    }

    public function testUpdateVersion(){
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $data = [
            'active'=>$this->faker->boolean,
            'reference'=>$this->faker->randomNumber([1000,3000]),
            'type'=>$this->faker->randomElement(['pack','version']),
            'battery'=>$this->faker->words(),
        ];
        $update = $repo->update($version->id,$data);
        $this->assertEquals($update->battery, $data['battery']);
    }

    public function testUpdateVersionException1(){
        $this->expectException(UpdateException::class);
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $data = [
            'active'=>$this->faker->boolean,
            'reference'=>$this->faker->randomNumber([1000,3000]),
            'type'=>$this->faker->randomElement(['pack','version']),
            'battery'=>$this->faker->words(),
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateVersionException2(){
        $this->expectException(UpdateException::class);
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $data = [
            'reference'=>null,
        ];
        $repo->update($version->id,$data);
    }

    public function testDeleteVersion()
    {
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $delete = $repo->delete($version->id);
        $this->assertTrue($delete);
    }
    public function testDeleteVersionException()
    {
        $this->expectException(DeleteException::class);
        $repo = new VersionRepository(new Version());
        $repo->delete(999);
    }

    public function testGetVersionMultiParam(){
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $data = $repo->findByMultiParams([
            'type'=>$version->type,
            'battery'=>$version->battery
        ]);
        $this->assertEquals($data[0]->type,$version->type);
        $this->assertEquals($data[0]->battery,$version->battery);
    }

    public function testGetVersionMultiParamException(){
        $this->expectException(NotFoundException::class);
        $version = Version::factory()->create();
        $repo = new VersionRepository(new Version());
        $repo->findByMultiParams([
            'type'=>$this->faker->word,
            'battery'=>$this->faker->word
        ]);
    }

    public function testGetAllVersions(){
        $versions = Version::factory()->times(10)->create();
        $repo = new VersionRepository(new Version());
        $data = $repo->all();
        $this->assertEquals($versions->count(),$data->count());
    }
    public function testGetAllVersionsException(){
        $this->expectException(NotFoundException::class);
        $repo = new VersionRepository(new Version());
        $repo->all();
    }
    public function testGetAllVersionsPaginate(){
        Version::factory()->times(10)->create();
        $repo = new VersionRepository(new Version());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllVersionsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new VersionRepository(new Version());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
