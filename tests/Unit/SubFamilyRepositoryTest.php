<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Family;
use App\Models\SubFamily;
use App\Repositories\SubFamilyRepository;
use Tests\TestCase;

class SubFamilyRepositoryTest extends TestCase
{
    public function testCreateSubFamily()
    {
        $data = [
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl(),
            'family_id'=>Family::factory()->create()->id
        ];
        $repo = new SubFamilyRepository(new SubFamily());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['image'], $dataRepo->image);
    }

    public function testCreateSubFamilyException()
    {
        $this->expectException(CreateException::class);
        $repo = new SubFamilyRepository(new SubFamily());
        $repo->create([]);
    }

    public function testGetSubFamily()
    {
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = $repo->find($subFamily->id);
        $this->assertEquals($subFamily->name, $data->name);
        $this->assertEquals($subFamily->image, $data->image);
    }
    public function testGetSubFamilyException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new SubFamilyRepository(new SubFamily());
        $repo->find(999);
    }

    public function testGetSubFamilyByParam()
    {
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = $repo->findByParam('name',$subFamily->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($subFamily->name, $data[0]->name);
        $this->assertEquals($subFamily->image, $data[0]->image);
    }

    public function testGetSubFamilyByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new SubFamilyRepository(new SubFamily());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateSubFamily(){
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($subFamily->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateSubFamilyException1(){
        $this->expectException(UpdateException::class);
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateSubFamilyException2(){
        $this->expectException(UpdateException::class);
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = [
            'name'=>null
        ];
        $repo->update($subFamily->id,$data);
    }

    public function testDeleteSubFamily()
    {
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $delete = $repo->delete($subFamily->id);
        $this->assertTrue($delete);
    }
    public function testDeleteSubFamilyException()
    {
        $this->expectException(DeleteException::class);
        $repo = new SubFamilyRepository(new SubFamily());
        $repo->delete(999);
    }

    public function testGetSubFamilyMultiParam(){
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = $repo->findByMultiParams([
            'name'=>$subFamily->name,
            'image'=>$subFamily->image
        ]);
        $this->assertEquals($data[0]->name,$subFamily->name);
        $this->assertEquals($data[0]->image,$subFamily->image);
    }

    public function testGetSubFamilyMultiParamException(){
        $this->expectException(NotFoundException::class);
        $subFamily = SubFamily::factory()->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'image'=>$subFamily->image
        ]);
    }

    public function testGetAllSubFamilys(){
        $subFamilys = SubFamily::factory()->times(10)->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $data = $repo->all();
        $this->assertEquals($subFamilys->count(),$data->count());
    }
    public function testGetAllSubFamilysException(){
        $this->expectException(NotFoundException::class);
        $repo = new SubFamilyRepository(new SubFamily());
        $repo->all();
    }
    public function testGetAllSubFamilysPaginate(){
        SubFamily::factory()->times(10)->create();
        $repo = new SubFamilyRepository(new SubFamily());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllSubFamilysPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new SubFamilyRepository(new SubFamily());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
