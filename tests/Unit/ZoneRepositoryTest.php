<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Zone;
use App\Repositories\ZoneRepository;
use Tests\TestCase;

class ZoneRepositoryTest extends TestCase
{
    public function testCreateZone()
    {
        $data = [
            'name' => $this->faker->name,
        ];
        $repo = new ZoneRepository(new Zone());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
    }

    public function testCreateZoneException()
    {
        $this->expectException(CreateException::class);
        $repo = new ZoneRepository(new Zone());
        $repo->create([]);
    }

    public function testGetZone()
    {
        $zone = Zone::factory()->create();
        $repo = new ZoneRepository(new Zone());
        $data = $repo->find($zone->id);
        $this->assertEquals($zone->name, $data->name);
    }
    public function testGetZoneException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ZoneRepository(new Zone());
        $repo->find(999);
    }

    public function testGetZoneByParam()
    {
        $zone = Zone::factory()->create();
        $repo = new ZoneRepository(new Zone());
        $data = $repo->findByParam('name',$zone->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($zone->name, $data[0]->name);
    }

    public function testGetZoneByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ZoneRepository(new Zone());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateZone(){
        $zone = Zone::factory()->create();
        $repo = new ZoneRepository(new Zone());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($zone->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateZoneException1(){
        $this->expectException(UpdateException::class);
        $zone = Zone::factory()->create();
        $repo = new ZoneRepository(new Zone());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateZoneException2(){
        $this->expectException(UpdateException::class);
        $zone = Zone::factory()->create();
        $repo = new ZoneRepository(new Zone());
        $data = [
            'name'=>null
        ];
        $repo->update($zone->id,$data);
    }

    public function testDeleteZone()
    {
        $zone = Zone::factory()->create();
        $repo = new ZoneRepository(new Zone());
        $delete = $repo->delete($zone->id);
        $this->assertTrue($delete);
    }
    public function testDeleteZoneException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ZoneRepository(new Zone());
        $repo->delete(999);
    }

    public function testGetAllZones(){
        $zones = Zone::factory()->times(10)->create();
        $repo = new ZoneRepository(new Zone());
        $data = $repo->all();
        $this->assertEquals($zones->count(),$data->count());
    }
    public function testGetAllZonesException(){
        $this->expectException(NotFoundException::class);
        $repo = new ZoneRepository(new Zone());
        $repo->all();
    }
    public function testGetAllZonesPaginate(){
        Zone::factory()->times(10)->create();
        $repo = new ZoneRepository(new Zone());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllZonesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ZoneRepository(new Zone());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
