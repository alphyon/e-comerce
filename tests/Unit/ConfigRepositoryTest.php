<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Config;
use App\Repositories\ConfigRepository;
use Tests\TestCase;

class ConfigRepositoryTest extends TestCase
{
    public function testCreateConfig()
    {
        $data = [
            'value'=>$this->faker->word,
            'name'=>$this->faker->word,
            'complement'=>$this->faker->text(140),
        ];
        $repo = new ConfigRepository(new Config());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['complement'], $dataRepo->complement);
    }

    public function testCreateConfigException()
    {
        $this->expectException(CreateException::class);
        $repo = new ConfigRepository(new Config());
        $repo->create([]);
    }

    public function testGetConfig()
    {
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $data = $repo->find($role->id);
        $this->assertEquals($role->name, $data->name);
        $this->assertEquals($role->complement, $data->complement);
    }
    public function testGetConfigException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ConfigRepository(new Config());
        $repo->find(999);
    }

    public function testGetConfigByParam()
    {
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $data = $repo->findByParam('name',$role->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($role->name, $data[0]->name);
        $this->assertEquals($role->complement, $data[0]->complement);
    }

    public function testGetConfigByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ConfigRepository(new Config());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateConfig(){
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($role->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateConfigException1(){
        $this->expectException(UpdateException::class);
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateConfigException2(){
        $this->expectException(UpdateException::class);
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $data = [
            'name'=>null
        ];
        $repo->update($role->id,$data);
    }

    public function testDeleteConfig()
    {
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $delete = $repo->delete($role->id);
        $this->assertTrue($delete);
    }
    public function testDeleteConfigException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ConfigRepository(new Config());
        $repo->delete(999);
    }

    public function testGetConfigMultiParam(){
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $data = $repo->findByMultiParams([
            'name'=>$role->name,
            'complement'=>$role->complement
        ]);
        $this->assertEquals($data[0]->name,$role->name);
        $this->assertEquals($data[0]->complement,$role->complement);
    }

    public function testGetConfigMultiParamException(){
        $this->expectException(NotFoundException::class);
        $role = Config::factory()->create();
        $repo = new ConfigRepository(new Config());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'complement'=>$role->complement
        ]);
    }

    public function testGetAllConfigs(){
        $roles = Config::factory()->times(10)->create();
        $repo = new ConfigRepository(new Config());
        $data = $repo->all();
        $this->assertEquals($roles->count(),$data->count());
    }
    public function testGetAllConfigsException(){
        $this->expectException(NotFoundException::class);
        $repo = new ConfigRepository(new Config());
        $repo->all();
    }
    public function testGetAllConfigsPaginate(){
        Config::factory()->times(10)->create();
        $repo = new ConfigRepository(new Config());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllConfigsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ConfigRepository(new Config());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
