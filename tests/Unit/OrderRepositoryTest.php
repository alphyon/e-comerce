<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Order;
use App\Models\Status;
use App\Models\Transport;
use App\Models\User;
use App\Repositories\OrderRepository;
use Tests\TestCase;

class OrderRepositoryTest extends TestCase
{
    public function testCreateOrder()
    {
        $data = [
            'user_id'=>User::factory()->create()->id,
            'status_id'=>Status::factory()->create()->id,
            'transport_id'=>Transport::factory()->create()->id,
            'date'=>$this->faker->date(),
            'alias'=>$this->faker->word,
            'identify_number'=>$this->faker->randomNumber([1000,3000]),
            'total'=>$this->faker->randomNumber([1000,3000]),
            'idpedv'=>$this->faker->randomNumber([100,300]),
            'iva'=>$this->faker->randomNumber([1000,3000]),
            'pay_method'=>$this->faker->word,
            'transport_price'=>$this->faker->randomDigit,
            'comment'=>$this->faker->text,
            'bill'=>$this->faker->slug(3).$this->faker->fileExtension,
            'process'=>$this->faker->boolean,
        ];
        $repo = new OrderRepository(new Order());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['identify_number'], $dataRepo->identify_number);
        $this->assertEquals($data['pay_method'], $dataRepo->pay_method);
    }

    public function testCreateOrderException()
    {
        $this->expectException(CreateException::class);
        $repo = new OrderRepository(new Order());
        $repo->create([]);
    }

    public function testGetOrder()
    {
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $data = $repo->find($order->id);
        $this->assertEquals($order->name, $data->name);
        $this->assertEquals($order->slug, $data->slug);
    }
    public function testGetOrderException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new OrderRepository(new Order());
        $repo->find(999);
    }

    public function testGetOrderByParam()
    {
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $data = $repo->findByParam('pay_method',$order->pay_method);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($order->identify_number, $data[0]->identify_number);
        $this->assertEquals($order->total, $data[0]->total);
    }

    public function testGetOrderByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new OrderRepository(new Order());
        $repo->findByParam('total',$this->faker->word);
    }

    public function testUpdateOrder(){
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $data = [
            'date'=>$this->faker->date(),
            'alias'=>$this->faker->word,
        ];
        $update = $repo->update($order->id,$data);
        $this->assertEquals($update->date, $data['date']);
        $this->assertEquals($update->alias, $data['alias']);
    }

    public function testUpdateOrderException1(){
        $this->expectException(UpdateException::class);
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $data = [
            'date'=>$this->faker->date(),
            'alias'=>$this->faker->word,
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateOrderException2(){
        $this->expectException(UpdateException::class);
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $data = [
            'date'=>null,
        ];
        $repo->update($order->id,$data);
    }

    public function testDeleteOrder()
    {
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $delete = $repo->delete($order->id);
        $this->assertTrue($delete);
    }
    public function testDeleteOrderException()
    {
        $this->expectException(DeleteException::class);
        $repo = new OrderRepository(new Order());
        $repo->delete(999);
    }

    public function testGetOrderMultiParam(){
        $order = Order::factory()->create();
        $repo = new OrderRepository(new Order());
        $data = $repo->findByMultiParams([
            'date'=>$order->date,
            'alias'=>$order->alias,
        ]);
        $this->assertEquals($data[0]->comment,$order->comment);
        $this->assertEquals($data[0]->alias,$order->alias);
    }

    public function testGetOrderMultiParamException(){
        $this->expectException(NotFoundException::class);
        $repo = new OrderRepository(new Order());
        $repo->findByMultiParams([
            'date'=>$this->faker->date(),
            'alias'=>$this->faker->word,
        ]);
    }

    public function testGetAllOrders(){
        $orders = Order::factory()->times(10)->create();
        $repo = new OrderRepository(new Order());
        $data = $repo->all();
        $this->assertEquals($orders->count(),$data->count());
    }
    public function testGetAllOrdersException(){
        $this->expectException(NotFoundException::class);
        $repo = new OrderRepository(new Order());
        $repo->all();
    }
    public function testGetAllOrdersPaginate(){
        Order::factory()->times(10)->create();
        $repo = new OrderRepository(new Order());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllOrdersPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new OrderRepository(new Order());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
