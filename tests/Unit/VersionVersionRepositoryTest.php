<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Version;
use App\Models\VersionVersion;
use App\Repositories\VersionVersionRepository;
use Tests\TestCase;

class VersionVersionRepositoryTest extends TestCase
{
    public function testCreateVersionVersion()
    {
        $data = [
            'parent_id'=>Version::factory()->create()->id,
            'child_id'=>Version::factory()->create()->id,
            'quantity'=>$this->faker->numerify('##')
        ];
        $repo = new VersionVersionRepository(new VersionVersion());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['parent_id'], $dataRepo->parent_id);
        $this->assertEquals($data['child_id'], $dataRepo->child_id);
    }

    public function testCreateVersionVersionException()
    {
        $this->expectException(CreateException::class);
        $repo = new VersionVersionRepository(new VersionVersion());
        $repo->create([]);
    }

    public function testGetVersionVersion()
    {
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = $repo->find($versionVersion->id);
        $this->assertEquals($versionVersion->parent_id, $data->parent_id);
        $this->assertEquals($versionVersion->child_id, $data->child_id);
    }
    public function testGetVersionVersionException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new VersionVersionRepository(new VersionVersion());
        $repo->find(999);
    }

    public function testGetVersionVersionByParam()
    {
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = $repo->findByParam('parent_id',$versionVersion->parent_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($versionVersion->parent_id, $data[0]->parent_id);
        $this->assertEquals($versionVersion->child_id, $data[0]->child_id);
    }

    public function testGetVersionVersionByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new VersionVersionRepository(new VersionVersion());
        $repo->findByParam('parent_id',$this->faker->word);
    }

    public function testUpdateVersionVersion(){
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = [
            'parent_id'=>Version::factory()->create()->id
        ];
        $update = $repo->update($versionVersion->id,$data);
        $this->assertEquals($update->parent_id, $data['parent_id']);
    }

    public function testUpdateVersionVersionException1(){
        $this->expectException(UpdateException::class);
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = [
            'parent_id'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateVersionVersionException2(){
        $this->expectException(UpdateException::class);
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = [
            'parent_id'=>null
        ];
        $repo->update($versionVersion->id,$data);
    }

    public function testDeleteVersionVersion()
    {
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $delete = $repo->delete($versionVersion->id);
        $this->assertTrue($delete);
    }
    public function testDeleteVersionVersionException()
    {
        $this->expectException(DeleteException::class);
        $repo = new VersionVersionRepository(new VersionVersion());
        $repo->delete(999);
    }

    public function testGetVersionVersionMultiParam(){
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = $repo->findByMultiParams([
            'parent_id'=>$versionVersion->parent_id,
            'child_id'=>$versionVersion->child_id
        ]);
        $this->assertEquals($data[0]->parent_id,$versionVersion->parent_id);
        $this->assertEquals($data[0]->child_id,$versionVersion->child_id);
    }

    public function testGetVersionVersionMultiParamException(){
        $this->expectException(NotFoundException::class);
        $versionVersion = VersionVersion::factory()->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $repo->findByMultiParams([
            'parent_id'=>$this->faker->word,
            'child_id'=>$versionVersion->child_id
        ]);
    }

    public function testGetAllVersionVersions(){
        $versionVersions = VersionVersion::factory()->times(10)->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $data = $repo->all();
        $this->assertEquals($versionVersions->count(),$data->count());
    }
    public function testGetAllVersionVersionsException(){
        $this->expectException(NotFoundException::class);
        $repo = new VersionVersionRepository(new VersionVersion());
        $repo->all();
    }
    public function testGetAllVersionVersionsPaginate(){
        VersionVersion::factory()->times(10)->create();
        $repo = new VersionVersionRepository(new VersionVersion());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllVersionVersionsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new VersionVersionRepository(new VersionVersion());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
