<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Cart;
use App\Models\User;
use App\Repositories\CartRepository;
use Tests\TestCase;

class CartRepositoryTest extends TestCase
{
    public function testCreateCart()
    {
        $data = [
            'user_id' => User::factory()->create()->id,
        ];
        $repo = new CartRepository(new Cart());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['user_id'], $dataRepo->user_id);
    }

    public function testCreateCartException()
    {
        $this->expectException(CreateException::class);
        $repo = new CartRepository(new Cart());
        $repo->create([]);
    }

    public function testGetCart()
    {
        $cart = Cart::factory()->create();
        $repo = new CartRepository(new Cart());
        $data = $repo->find($cart->id);
        $this->assertEquals($cart->user_id, $data->user_id);
    }
    public function testGetCartException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CartRepository(new Cart());
        $repo->find(999);
    }

    public function testGetCartByParam()
    {
        $cart = Cart::factory()->create();
        $repo = new CartRepository(new Cart());
        $data = $repo->findByParam('user_id',$cart->user_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($cart->user_id, $data[0]->user_id);
    }

    public function testGetCartByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CartRepository(new Cart());
        $repo->findByParam('user_id',$this->faker->word);
    }

    public function testDeleteCart()
    {
        $cart = Cart::factory()->create();
        $repo = new CartRepository(new Cart());
        $delete = $repo->delete($cart->id);
        $this->assertTrue($delete);
    }
    public function testDeleteCartException()
    {
        $this->expectException(DeleteException::class);
        $repo = new CartRepository(new Cart());
        $repo->delete(999);
    }

    public function testGetAllCarts(){
        $carts = Cart::factory()->times(10)->create();
        $repo = new CartRepository(new Cart());
        $data = $repo->all();
        $this->assertEquals($carts->count(),$data->count());
    }
    public function testGetAllCartsException(){
        $this->expectException(NotFoundException::class);
        $repo = new CartRepository(new Cart());
        $repo->all();
    }
    public function testGetAllCartsPaginate(){
        Cart::factory()->times(10)->create();
        $repo = new CartRepository(new Cart());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllCartsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new CartRepository(new Cart());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
