<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Size;
use App\Repositories\SizeRepository;
use Tests\TestCase;

class SizeRepositoryTest extends TestCase
{
    public function testCreateSize()
    {
        $data = [
            'size' => $this->faker->name,
        ];
        $repo = new SizeRepository(new Size());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['size'], $dataRepo->size);
    }

    public function testCreateSizeException()
    {
        $this->expectException(CreateException::class);
        $repo = new SizeRepository(new Size());
        $repo->create([]);
    }

    public function testGetSize()
    {
        $size = Size::factory()->create();
        $repo = new SizeRepository(new Size());
        $data = $repo->find($size->id);
        $this->assertEquals($size->size, $data->size);
    }
    public function testGetSizeException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new SizeRepository(new Size());
        $repo->find(999);
    }

    public function testGetSizeByParam()
    {
        $size = Size::factory()->create();
        $repo = new SizeRepository(new Size());
        $data = $repo->findByParam('size',$size->size);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($size->size, $data[0]->size);
    }

    public function testGetSizeByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new SizeRepository(new Size());
        $repo->findByParam('size',$this->faker->word);
    }

    public function testUpdateSize(){
        $size = Size::factory()->create();
        $repo = new SizeRepository(new Size());
        $data = [
            'size'=>$this->faker->word
        ];
        $update = $repo->update($size->id,$data);
        $this->assertEquals($update->size, $data['size']);
    }

    public function testUpdateSizeException1(){
        $this->expectException(UpdateException::class);
        $size = Size::factory()->create();
        $repo = new SizeRepository(new Size());
        $data = [
            'size'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateSizeException2(){
        $this->expectException(UpdateException::class);
        $size = Size::factory()->create();
        $repo = new SizeRepository(new Size());
        $data = [
            'size'=>null
        ];
        $repo->update($size->id,$data);
    }

    public function testDeleteSize()
    {
        $size = Size::factory()->create();
        $repo = new SizeRepository(new Size());
        $delete = $repo->delete($size->id);
        $this->assertTrue($delete);
    }
    public function testDeleteSizeException()
    {
        $this->expectException(DeleteException::class);
        $repo = new SizeRepository(new Size());
        $repo->delete(999);
    }

    public function testGetAllSizes(){
        $sizes = Size::factory()->times(10)->create();
        $repo = new SizeRepository(new Size());
        $data = $repo->all();
        $this->assertEquals($sizes->count(),$data->count());
    }
    public function testGetAllSizesException(){
        $this->expectException(NotFoundException::class);
        $repo = new SizeRepository(new Size());
        $repo->all();
    }
    public function testGetAllSizesPaginate(){
        Size::factory()->times(10)->create();
        $repo = new SizeRepository(new Size());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllSizesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new SizeRepository(new Size());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
