<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Notification;
use App\Models\Version;
use App\Repositories\NotificationRepository;
use Tests\TestCase;

class NotificationRepositoryTest extends TestCase
{
    public function testCreateNotification()
    {
        $data = [
            'version_id'=>Version::factory()->create()->id,
            'email'=>$this->faker->safeEmail,
            'locale'=>$this->faker->locale
        ];
        $repo = new NotificationRepository(new Notification());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['locale'], $dataRepo->locale);
        $this->assertEquals($data['email'], $dataRepo->email);
    }

    public function testCreateNotificationException()
    {
        $this->expectException(CreateException::class);
        $repo = new NotificationRepository(new Notification());
        $repo->create([]);
    }

    public function testGetNotification()
    {
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $data = $repo->find($notification->id);
        $this->assertEquals($notification->locale, $data->locale);
        $this->assertEquals($notification->email, $data->email);
    }
    public function testGetNotificationException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new NotificationRepository(new Notification());
        $repo->find(999);
    }

    public function testGetNotificationByParam()
    {
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $data = $repo->findByParam('locale',$notification->locale);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($notification->locale, $data[0]->locale);
        $this->assertEquals($notification->email, $data[0]->email);
    }

    public function testGetNotificationByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new NotificationRepository(new Notification());
        $repo->findByParam('locale',$this->faker->word);
    }

    public function testUpdateNotification(){
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $data = [
            'locale'=>$this->faker->word
        ];
        $update = $repo->update($notification->id,$data);
        $this->assertEquals($update->locale, $data['locale']);
    }

    public function testUpdateNotificationException1(){
        $this->expectException(UpdateException::class);
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $data = [
            'locale'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateNotificationException2(){
        $this->expectException(UpdateException::class);
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $data = [
            'locale'=>null
        ];
        $repo->update($notification->id,$data);
    }

    public function testDeleteNotification()
    {
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $delete = $repo->delete($notification->id);
        $this->assertTrue($delete);
    }
    public function testDeleteNotificationException()
    {
        $this->expectException(DeleteException::class);
        $repo = new NotificationRepository(new Notification());
        $repo->delete(999);
    }

    public function testGetNotificationMultiParam(){
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $data = $repo->findByMultiParams([
            'locale'=>$notification->locale,
            'email'=>$notification->email
        ]);
        $this->assertEquals($data[0]->locale,$notification->locale);
        $this->assertEquals($data[0]->email,$notification->email);
    }

    public function testGetNotificationMultiParamException(){
        $this->expectException(NotFoundException::class);
        $notification = Notification::factory()->create();
        $repo = new NotificationRepository(new Notification());
        $repo->findByMultiParams([
            'locale'=>$this->faker->word,
            'email'=>$notification->email
        ]);
    }

    public function testGetAllNotifications(){
        $notifications = Notification::factory()->times(10)->create();
        $repo = new NotificationRepository(new Notification());
        $data = $repo->all();
        $this->assertEquals($notifications->count(),$data->count());
    }
    public function testGetAllNotificationsException(){
        $this->expectException(NotFoundException::class);
        $repo = new NotificationRepository(new Notification());
        $repo->all();
    }
    public function testGetAllNotificationsPaginate(){
        Notification::factory()->times(10)->create();
        $repo = new NotificationRepository(new Notification());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllNotificationsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new NotificationRepository(new Notification());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
