<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Gallery;
use App\Models\Version;
use App\Repositories\GalleryRepository;
use Tests\TestCase;

class GalleryRepositoryTest extends TestCase
{
    public function testCreateGallery()
    {
        $data = [
            'url' => $this->faker->imageUrl(),
            'version_id'=>Version::factory()->create()->id
        ];
        $repo = new GalleryRepository(new Gallery());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['url'], $dataRepo->url);
    }

    public function testCreateGalleryException()
    {
        $this->expectException(CreateException::class);
        $repo = new GalleryRepository(new Gallery());
        $repo->create([]);
    }

    public function testGetGallery()
    {
        $zone = Gallery::factory()->create();
        $repo = new GalleryRepository(new Gallery());
        $data = $repo->find($zone->id);
        $this->assertEquals($zone->url, $data->url);
    }
    public function testGetGalleryException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new GalleryRepository(new Gallery());
        $repo->find(999);
    }

    public function testGetGalleryByParam()
    {
        $zone = Gallery::factory()->create();
        $repo = new GalleryRepository(new Gallery());
        $data = $repo->findByParam('url',$zone->url);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($zone->url, $data[0]->url);
    }

    public function testGetGalleryByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new GalleryRepository(new Gallery());
        $repo->findByParam('url',$this->faker->word);
    }

    public function testUpdateGallery(){
        $zone = Gallery::factory()->create();
        $repo = new GalleryRepository(new Gallery());
        $data = [
            'url'=>$this->faker->word
        ];
        $update = $repo->update($zone->id,$data);
        $this->assertEquals($update->url, $data['url']);
    }

    public function testUpdateGalleryException1(){
        $this->expectException(UpdateException::class);
        $zone = Gallery::factory()->create();
        $repo = new GalleryRepository(new Gallery());
        $data = [
            'url'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateGalleryException2(){
        $this->expectException(UpdateException::class);
        $zone = Gallery::factory()->create();
        $repo = new GalleryRepository(new Gallery());
        $data = [
            'url'=>null
        ];
        $repo->update($zone->id,$data);
    }

    public function testDeleteGallery()
    {
        $zone = Gallery::factory()->create();
        $repo = new GalleryRepository(new Gallery());
        $delete = $repo->delete($zone->id);
        $this->assertTrue($delete);
    }
    public function testDeleteGalleryException()
    {
        $this->expectException(DeleteException::class);
        $repo = new GalleryRepository(new Gallery());
        $repo->delete(999);
    }

    public function testGetAllGallerys(){
        $zones = Gallery::factory()->times(10)->create();
        $repo = new GalleryRepository(new Gallery());
        $data = $repo->all();
        $this->assertEquals($zones->count(),$data->count());
    }
    public function testGetAllGalleriesException(){
        $this->expectException(NotFoundException::class);
        $repo = new GalleryRepository(new Gallery());
        $repo->all();
    }
    public function testGetAllGallerysPaginate(){
        Gallery::factory()->times(10)->create();
        $repo = new GalleryRepository(new Gallery());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllGalleriesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new GalleryRepository(new Gallery());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
