<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Transport;
use App\Models\TransportZone;
use App\Models\Zone;
use App\Repositories\TransportZoneRepository;
use Tests\TestCase;

class TransportZoneRepositoryTest extends TestCase
{
    public function testCreateTransportZone()
    {
        $data = [
            'transport_id'=>Transport::factory()->create()->id,
            'zone_id'=>Zone::factory()->create()->id,
            'min_weight'=>$this->faker->numberBetween(1000,3000),
            'max_weight'=>$this->faker->numberBetween(3000,5000),
            'price'=>$this->faker->numberBetween(1000,3000),
        ];
        $repo = new TransportZoneRepository(new TransportZone());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['price'], $dataRepo->price);
        $this->assertEquals($data['zone_id'], $dataRepo->zone_id);
    }

    public function testCreateTransportZoneException()
    {
        $this->expectException(CreateException::class);
        $repo = new TransportZoneRepository(new TransportZone());
        $repo->create([]);
    }

    public function testGetTransportZone()
    {
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = $repo->find($transportZone->id);
        $this->assertEquals($transportZone->price, $data->price);
        $this->assertEquals($transportZone->min_weight, $data->min_weight);
    }
    public function testGetTransportZoneException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new TransportZoneRepository(new TransportZone());
        $repo->find(999);
    }

    public function testGetTransportZoneByParam()
    {
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = $repo->findByParam('max_weight',$transportZone->max_weight);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($transportZone->price, $data[0]->price);
        $this->assertEquals($transportZone->min_weight, $data[0]->min_weight);
    }

    public function testGetTransportZoneByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new TransportZoneRepository(new TransportZone());
        $repo->findByParam('price',$this->faker->randomDigit);
    }

    public function testUpdateTransportZone(){
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = [
            'price'=>$this->faker->randomDigit
        ];
        $update = $repo->update($transportZone->id,$data);
        $this->assertEquals($update->price, $data['price']);
    }

    public function testUpdateTransportZoneException1(){
        $this->expectException(UpdateException::class);
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = [
            'price'=>$this->faker->randomDigit
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateTransportZoneException2(){
        $this->expectException(UpdateException::class);
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = [
            'price'=>null
        ];
        $repo->update($transportZone->id,$data);
    }

    public function testDeleteTransportZone()
    {
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $delete = $repo->delete($transportZone->id);
        $this->assertTrue($delete);
    }
    public function testDeleteTransportZoneException()
    {
        $this->expectException(DeleteException::class);
        $repo = new TransportZoneRepository(new TransportZone());
        $repo->delete(999);
    }

    public function testGetTransportZoneMultiParam(){
        $transportZone = TransportZone::factory()->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = $repo->findByMultiParams([
            'max_weight'=>$transportZone->max_weight,
            'min_weight'=>$transportZone->min_weight
        ]);
        $this->assertEquals($data[0]->price,$transportZone->price);
        $this->assertEquals($data[0]->zone_id,$transportZone->zone_id);
    }

    public function testGetTransportZoneMultiParamException(){
        $this->expectException(NotFoundException::class);

        $repo = new TransportZoneRepository(new TransportZone());
        $repo->findByMultiParams([
            'max_weight'=>$this->faker->randomDigit,
            'min_weight'=>$this->faker->randomDigit
        ]);
    }

    public function testGetAllTransportZones(){
        $transportZones = TransportZone::factory()->times(10)->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $data = $repo->all();
        $this->assertEquals($transportZones->count(),$data->count());
    }
    public function testGetAllTransportZonesException(){
        $this->expectException(NotFoundException::class);
        $repo = new TransportZoneRepository(new TransportZone());
        $repo->all();
    }
    public function testGetAllTransportZonesPaginate(){
        TransportZone::factory()->times(10)->create();
        $repo = new TransportZoneRepository(new TransportZone());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllTransportZonesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new TransportZoneRepository(new TransportZone());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
