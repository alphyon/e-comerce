<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Currency;
use App\Models\Profile;
use App\Models\Rate;
use App\Models\User;
use App\Models\Zone;
use App\Repositories\ProfileRepository;
use Tests\TestCase;

class ProfileRepositoryTest extends TestCase
{
    public function testCreateProfile()
    {
        $data = [
            'user_id'=>User::factory()->create()->id,
            'name'=>$this->faker->name,
            'name_complement'=>$this->faker->sentence(5),
            'address'=>$this->faker->address,
            'city'=>$this->faker->city,
            'state'=>$this->faker->state,
            'country'=>$this->faker->country,
            'postcode'=>$this->faker->postcode,
            'contact_name'=>$this->faker->name,
            'connector_id'=>$this->faker->numerify('####'),
            'iva'=>$this->faker->boolean,
            'nif'=>$this->faker->numerify('#######'),
            'business_name'=>$this->faker->company,
            'web'=>$this->faker->url,
            'currency_id'=>Currency::factory()->create()->id,
            'bill'=>$this->faker->boolean,
            'rate_id'=>Rate::factory()->create()->id,
            'zone_id'=>Zone::factory()->create()->id,
        ];
        $repo = new ProfileRepository(new Profile());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['postcode'], $dataRepo->postcode);
    }

    public function testCreateProfileException()
    {
        $this->expectException(CreateException::class);
        $repo = new ProfileRepository(new Profile());
        $repo->create([]);
    }

    public function testGetProfile()
    {
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $data = $repo->find($profile->id);
        $this->assertEquals($profile->name, $data->name);
        $this->assertEquals($profile->postcode, $data->postcode);
    }
    public function testGetProfileException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ProfileRepository(new Profile());
        $repo->find(999);
    }

    public function testGetProfileByParam()
    {
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $data = $repo->findByParam('name',$profile->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($profile->name, $data[0]->name);
        $this->assertEquals($profile->postcode, $data[0]->postcode);
    }

    public function testGetProfileByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ProfileRepository(new Profile());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateProfile(){
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($profile->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateProfileException1(){
        $this->expectException(UpdateException::class);
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateProfileException2(){
        $this->expectException(UpdateException::class);
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $data = [
            'name'=>null
        ];
        $repo->update($profile->id,$data);
    }

    public function testDeleteProfile()
    {
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $delete = $repo->delete($profile->id);
        $this->assertTrue($delete);
    }
    public function testDeleteProfileException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ProfileRepository(new Profile());
        $repo->delete(999);
    }

    public function testGetProfileMultiParam(){
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $data = $repo->findByMultiParams([
            'name'=>$profile->name,
            'postcode'=>$profile->postcode
        ]);
        $this->assertEquals($data[0]->name,$profile->name);
        $this->assertEquals($data[0]->postcode,$profile->postcode);
    }

    public function testGetProfileMultiParamException(){
        $this->expectException(NotFoundException::class);
        $profile = Profile::factory()->create();
        $repo = new ProfileRepository(new Profile());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'postcode'=>$profile->postcode
        ]);
    }

    public function testGetAllProfiles(){
        $profiles = Profile::factory()->times(10)->create();
        $repo = new ProfileRepository(new Profile());
        $data = $repo->all();
        $this->assertEquals($profiles->count(),$data->count());
    }
    public function testGetAllProfilesException(){
        $this->expectException(NotFoundException::class);
        $repo = new ProfileRepository(new Profile());
        $repo->all();
    }
    public function testGetAllProfilesPaginate(){
        Profile::factory()->times(10)->create();
        $repo = new ProfileRepository(new Profile());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllProfilesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ProfileRepository(new Profile());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
