<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Tests\TestCase;

class ProductRepositoryTest extends TestCase
{
    public function testCreateProduct()
    {
        $data = [
            'name'=>$this->faker->word,
            'box'=>$this->faker->randomDigit,
            'active'=>$this->faker->boolean,
            'category_id'=>Category::factory()->create()->id,
            'width'=>$this->faker->randomNumber(),
            'large'=>$this->faker->randomNumber(),
            'height'=>$this->faker->randomNumber(),
            'iva'=>$this->faker->randomNumber(),
            'discount'=>$this->faker->randomNumber(),
            'umbral'=>$this->faker->randomDigit
        ];
        $repo = new ProductRepository(new Product());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['large'], $dataRepo->large);
        $this->assertEquals($data['iva'], $dataRepo->iva);
        $this->assertEquals($data['umbral'], $dataRepo->umbral);

    }

    public function testCreateProductException()
    {
        $this->expectException(CreateException::class);
        $repo = new ProductRepository(new Product());
        $repo->create([]);
    }

    public function testGetProduct()
    {
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $data = $repo->find($product->id);
        $this->assertEquals($product->name, $data->name);
        $this->assertEquals($product->iva, $data->iva);
    }
    public function testGetProductException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ProductRepository(new Product());
        $repo->find(999);
    }

    public function testGetProductByParam()
    {
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $data = $repo->findByParam('name',$product->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($product->name, $data[0]->name);
        $this->assertEquals($product->iva, $data[0]->iva);
    }

    public function testGetProductByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ProductRepository(new Product());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateProduct(){
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $data = [
            'name'=>$this->faker->word,
            'width'=>$this->faker->randomNumber(),
            'large'=>$this->faker->randomNumber(),
            'height'=>$this->faker->randomNumber(),
            'iva'=>$this->faker->randomNumber(),
        ];
        $update = $repo->update($product->id,$data);
        $this->assertEquals($update->name, $data['name']);
        $this->assertEquals($update->iva, $data['iva']);
        $this->assertEquals($update->height, $data['height']);
    }

    public function testUpdateProductException1(){
        $this->expectException(UpdateException::class);
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateProductException2(){
        $this->expectException(UpdateException::class);
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $data = [
            'name'=>null
        ];
        $repo->update($product->id,$data);
    }

    public function testDeleteProduct()
    {
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $delete = $repo->delete($product->id);
        $this->assertTrue($delete);
    }
    public function testDeleteProductException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ProductRepository(new Product());
        $repo->delete(999);
    }

    public function testGetProductMultiParam(){
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $data = $repo->findByMultiParams([
            'name'=>$product->name,
            'iva'=>$product->iva
        ]);
        $this->assertEquals($data[0]->name,$product->name);
        $this->assertEquals($data[0]->iva,$product->iva);
    }

    public function testGetProductMultiParamException(){
        $this->expectException(NotFoundException::class);
        $product = Product::factory()->create();
        $repo = new ProductRepository(new Product());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'iva'=>$product->iva
        ]);
    }

    public function testGetAllProducts(){
        $products = Product::factory()->times(10)->create();
        $repo = new ProductRepository(new Product());
        $data = $repo->all();
        $this->assertEquals($products->count(),$data->count());
    }
    public function testGetAllProductsException(){
        $this->expectException(NotFoundException::class);
        $repo = new ProductRepository(new Product());
        $repo->all();
    }
    public function testGetAllProductsPaginate(){
        Product::factory()->times(10)->create();
        $repo = new ProductRepository(new Product());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllProductsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ProductRepository(new Product());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
