<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Download;
use App\Models\SubFamily;
use App\Repositories\DownloadRepository;
use Tests\TestCase;

class DownloadRepositoryTest extends TestCase
{
    public function testCreateDownload()
    {
        $data = [
            'subfamily_id'=>SubFamily::factory()->create()->id,
            'reference'=>$this->faker->slug(3),
            'file'=>$this->faker->slug(3).".".$this->faker->fileExtension
        ];
        $repo = new DownloadRepository(new Download());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['file'], $dataRepo->file);
        $this->assertEquals($data['reference'], $dataRepo->reference);
    }

    public function testCreateDownloadException()
    {
        $this->expectException(CreateException::class);
        $repo = new DownloadRepository(new Download());
        $repo->create([]);
    }

    public function testGetDownload()
    {
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $data = $repo->find($download->id);
        $this->assertEquals($download->file, $data->file);
        $this->assertEquals($download->reference, $data->reference);
    }
    public function testGetDownloadException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new DownloadRepository(new Download());
        $repo->find(999);
    }

    public function testGetDownloadByParam()
    {
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $data = $repo->findByParam('file',$download->file);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($download->file, $data[0]->file);
        $this->assertEquals($download->reference, $data[0]->reference);
    }

    public function testGetDownloadByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new DownloadRepository(new Download());
        $repo->findByParam('file',$this->faker->word);
    }

    public function testUpdateDownload(){
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $data = [
            'reference'=>$this->faker->slug(3),

        ];
        $update = $repo->update($download->id,$data);
        $this->assertEquals($update->reference, $data['reference']);
    }

    public function testUpdateDownloadException1(){
        $this->expectException(UpdateException::class);
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $data = [
            'reference'=>$this->faker->slug(3),

        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateDownloadException2(){
        $this->expectException(UpdateException::class);
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $data = [
            'reference'=>null,

        ];
        $repo->update($download->id,$data);
    }

    public function testDeleteDownload()
    {
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $delete = $repo->delete($download->id);
        $this->assertTrue($delete);
    }
    public function testDeleteDownloadException()
    {
        $this->expectException(DeleteException::class);
        $repo = new DownloadRepository(new Download());
        $repo->delete(999);
    }

    public function testGetDownloadMultiParam(){
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $data = $repo->findByMultiParams([
            'reference'=>$download->reference,
            'file'=>$download->file
        ]);
        $this->assertEquals($data[0]->file,$download->file);
        $this->assertEquals($data[0]->reference,$download->reference);
    }

    public function testGetDownloadMultiParamException(){
        $this->expectException(NotFoundException::class);
        $download = Download::factory()->create();
        $repo = new DownloadRepository(new Download());
        $repo->findByMultiParams([
            'reference'=>$this->faker->slug(3),
            'file'=>$this->faker->slug(3).".".$this->faker->fileExtension
        ]);
    }

    public function testGetAllDownloads(){
        $downloads = Download::factory()->times(10)->create();
        $repo = new DownloadRepository(new Download());
        $data = $repo->all();
        $this->assertEquals($downloads->count(),$data->count());
    }
    public function testGetAllDownloadsException(){
        $this->expectException(NotFoundException::class);
        $repo = new DownloadRepository(new Download());
        $repo->all();
    }
    public function testGetAllDownloadsPaginate(){
        Download::factory()->times(10)->create();
        $repo = new DownloadRepository(new Download());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllDownloadsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new DownloadRepository(new Download());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
