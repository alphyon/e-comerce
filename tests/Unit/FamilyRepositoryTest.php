<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Family;
use App\Repositories\FamilyRepository;
use Tests\TestCase;

class FamilyRepositoryTest extends TestCase
{
    public function testCreateFamily()
    {
        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->slug(1),
            'picture'=>$this->faker->imageUrl(),
            'sort'=>$this->faker->randomDigit
        ];
        $repo = new FamilyRepository(new Family());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['slug'], $dataRepo->slug);
    }

    public function testCreateFamilyException()
    {
        $this->expectException(CreateException::class);
        $repo = new FamilyRepository(new Family());
        $repo->create([]);
    }

    public function testGetFamily()
    {
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $data = $repo->find($family->id);
        $this->assertEquals($family->name, $data->name);
        $this->assertEquals($family->slug, $data->slug);
    }
    public function testGetFamilyException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new FamilyRepository(new Family());
        $repo->find(999);
    }

    public function testGetFamilyByParam()
    {
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $data = $repo->findByParam('name',$family->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($family->name, $data[0]->name);
        $this->assertEquals($family->slug, $data[0]->slug);
    }

    public function testGetFamilyByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new FamilyRepository(new Family());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateFamily(){
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($family->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateFamilyException1(){
        $this->expectException(UpdateException::class);
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateFamilyException2(){
        $this->expectException(UpdateException::class);
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $data = [
            'name'=>null
        ];
        $repo->update($family->id,$data);
    }

    public function testDeleteFamily()
    {
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $delete = $repo->delete($family->id);
        $this->assertTrue($delete);
    }
    public function testDeleteFamilyException()
    {
        $this->expectException(DeleteException::class);
        $repo = new FamilyRepository(new Family());
        $repo->delete(999);
    }

    public function testGetFamilyMultiParam(){
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $data = $repo->findByMultiParams([
            'name'=>$family->name,
            'slug'=>$family->slug
        ]);
        $this->assertEquals($data[0]->name,$family->name);
        $this->assertEquals($data[0]->slug,$family->slug);
    }

    public function testGetFamilyMultiParamException(){
        $this->expectException(NotFoundException::class);
        $family = Family::factory()->create();
        $repo = new FamilyRepository(new Family());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'slug'=>$family->slug
        ]);
    }

    public function testGetAllFamilys(){
        $familys = Family::factory()->times(10)->create();
        $repo = new FamilyRepository(new Family());
        $data = $repo->all();
        $this->assertEquals($familys->count(),$data->count());
    }
    public function testGetAllFamilysException(){
        $this->expectException(NotFoundException::class);
        $repo = new FamilyRepository(new Family());
        $repo->all();
    }
    public function testGetAllFamilysPaginate(){
        Family::factory()->times(10)->create();
        $repo = new FamilyRepository(new Family());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllFamilysPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new FamilyRepository(new Family());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
