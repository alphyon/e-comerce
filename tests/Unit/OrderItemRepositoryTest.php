<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Version;
use App\Repositories\OrderItemRepository;
use Tests\TestCase;

class OrderItemRepositoryTest extends TestCase
{
    public function testCreateOrderItem()
    {
        $data = [
            'version_id'=>Version::factory()->create()->id,
            'order_id'=>Order::factory()->create()->id,
            'price'=>$this->faker->numberBetween(1000,9000),
            'quantity'=>$this->faker->numerify('##'),
        ];
        $repo = new OrderItemRepository(new OrderItem());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['version_id'], $dataRepo->version_id);
        $this->assertEquals($data['price'], $dataRepo->price);
    }

    public function testCreateOrderItemException()
    {
        $this->expectException(CreateException::class);
        $repo = new OrderItemRepository(new OrderItem());
        $repo->create([]);
    }

    public function testGetOrderItem()
    {
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = $repo->find($orderItem->id);
        $this->assertEquals($orderItem->version_id, $data->version_id);
        $this->assertEquals($orderItem->price, $data->price);
    }
    public function testGetOrderItemException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new OrderItemRepository(new OrderItem());
        $repo->find(999);
    }

    public function testGetOrderItemByParam()
    {
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = $repo->findByParam('version_id',$orderItem->version_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($orderItem->version_id, $data[0]->version_id);
        $this->assertEquals($orderItem->price, $data[0]->price);
    }

    public function testGetOrderItemByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new OrderItemRepository(new OrderItem());
        $repo->findByParam('version_id',$this->faker->word);
    }

    public function testUpdateOrderItem(){
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = [
            'quantity'=> $this->faker->numerify('##')
        ];
        $update = $repo->update($orderItem->id,$data);
        $this->assertEquals($update->quantity, $data['quantity']);
    }

    public function testUpdateOrderItemException1(){
        $this->expectException(UpdateException::class);
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = [
            'quantity'=> $this->faker->numerify('##')
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateOrderItemException2(){
        $this->expectException(UpdateException::class);
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = [
            'quantity'=> null
        ];
        $repo->update($orderItem->id,$data);
    }

    public function testDeleteOrderItem()
    {
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $delete = $repo->delete($orderItem->id);
        $this->assertTrue($delete);
    }
    public function testDeleteOrderItemException()
    {
        $this->expectException(DeleteException::class);
        $repo = new OrderItemRepository(new OrderItem());
        $repo->delete(999);
    }

    public function testGetOrderItemMultiParam(){
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = $repo->findByMultiParams([
            'quantity'=>$orderItem->quantity,
            'version_id'=>$orderItem->version_id
        ]);
        $this->assertEquals($data[0]->order_id,$orderItem->order_id);
        $this->assertEquals($data[0]->price,$orderItem->price);
    }

    public function testGetOrderItemMultiParamException(){
        $this->expectException(NotFoundException::class);
        $orderItem = OrderItem::factory()->create();
        $repo = new OrderItemRepository(new OrderItem());
        $repo->findByMultiParams([
            'quantity'=>999,
            'version_id'=>999
        ]);
    }

    public function testGetAllOrderItems(){
        $orderItems = OrderItem::factory()->times(10)->create();
        $repo = new OrderItemRepository(new OrderItem());
        $data = $repo->all();
        $this->assertEquals($orderItems->count(),$data->count());
    }
    public function testGetAllOrderItemsException(){
        $this->expectException(NotFoundException::class);
        $repo = new OrderItemRepository(new OrderItem());
        $repo->all();
    }
    public function testGetAllOrderItemsPaginate(){
        OrderItem::factory()->times(10)->create();
        $repo = new OrderItemRepository(new OrderItem());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllOrderItemsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new OrderItemRepository(new OrderItem());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
