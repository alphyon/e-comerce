<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Tests\TestCase;

class ImageRepositoryTest extends TestCase
{
    public function testCreateImage()
    {
        $data = [
            'url' => $this->faker->imageUrl(),
        ];
        $repo = new ImageRepository(new Image());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['url'], $dataRepo->url);
    }

    public function testCreateImageException()
    {
        $this->expectException(CreateException::class);
        $repo = new ImageRepository(new Image());
        $repo->create([]);
    }

    public function testGetImage()
    {
        $zone = Image::factory()->create();
        $repo = new ImageRepository(new Image());
        $data = $repo->find($zone->id);
        $this->assertEquals($zone->url, $data->url);
    }
    public function testGetImageException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ImageRepository(new Image());
        $repo->find(999);
    }

    public function testGetImageByParam()
    {
        $zone = Image::factory()->create();
        $repo = new ImageRepository(new Image());
        $data = $repo->findByParam('url',$zone->url);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($zone->url, $data[0]->url);
    }

    public function testGetImageByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ImageRepository(new Image());
        $repo->findByParam('url',$this->faker->word);
    }

    public function testUpdateImage(){
        $zone = Image::factory()->create();
        $repo = new ImageRepository(new Image());
        $data = [
            'url'=>$this->faker->word
        ];
        $update = $repo->update($zone->id,$data);
        $this->assertEquals($update->url, $data['url']);
    }

    public function testUpdateImageException1(){
        $this->expectException(UpdateException::class);
        $zone = Image::factory()->create();
        $repo = new ImageRepository(new Image());
        $data = [
            'url'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateImageException2(){
        $this->expectException(UpdateException::class);
        $zone = Image::factory()->create();
        $repo = new ImageRepository(new Image());
        $data = [
            'url'=>null
        ];
        $repo->update($zone->id,$data);
    }

    public function testDeleteImage()
    {
        $zone = Image::factory()->create();
        $repo = new ImageRepository(new Image());
        $delete = $repo->delete($zone->id);
        $this->assertTrue($delete);
    }
    public function testDeleteImageException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ImageRepository(new Image());
        $repo->delete(999);
    }

    public function testGetAllImages(){
        $zones = Image::factory()->times(10)->create();
        $repo = new ImageRepository(new Image());
        $data = $repo->all();
        $this->assertEquals($zones->count(),$data->count());
    }
    public function testGetAllImagesException(){
        $this->expectException(NotFoundException::class);
        $repo = new ImageRepository(new Image());
        $repo->all();
    }
    public function testGetAllImagesPaginate(){
        Image::factory()->times(10)->create();
        $repo = new ImageRepository(new Image());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllImagesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ImageRepository(new Image());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
