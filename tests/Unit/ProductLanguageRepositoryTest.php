<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Language;
use App\Models\Product;
use App\Models\ProductLanguage;
use App\Repositories\ProductLanguageRepository;
use Tests\TestCase;

class ProductLanguageRepositoryTest extends TestCase
{
    public function testCreateProductLanguage()
    {
        $data = [
            'product_id'=>Product::factory()->create()->id,
            'language_id'=>Language::factory()->create()->id,
            'description'=>$this->faker->text,
            'meta_title'=>$this->faker->locale,
            'meta_keys'=>$this->faker->word,
            'meta_description'=>$this->faker->word,
        ];
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['meta_title'], $dataRepo->meta_title);
        $this->assertEquals($data['description'], $dataRepo->description);
    }

    public function testCreateProductLanguageException()
    {
        $this->expectException(CreateException::class);
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $repo->create([]);
    }

    public function testGetProductLanguage()
    {
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = $repo->find($productLanguage->id);
        $this->assertEquals($productLanguage->description, $data->description);
        $this->assertEquals($productLanguage->meta_title, $data->meta_title);
    }
    public function testGetProductLanguageException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $repo->find(999);
    }

    public function testGetProductLanguageByParam()
    {
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = $repo->findByParam('meta_title',$productLanguage->meta_title);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($productLanguage->meta_title, $data[0]->meta_title);
        $this->assertEquals($productLanguage->description, $data[0]->description);
    }

    public function testGetProductLanguageByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $repo->findByParam('meta_title',$this->faker->word);
    }

    public function testUpdateProductLanguage(){
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = [
            'meta_title'=>$this->faker->word
        ];
        $update = $repo->update($productLanguage->id,$data);
        $this->assertEquals($update->meta_title, $data['meta_title']);
    }

    public function testUpdateProductLanguageException1(){
        $this->expectException(UpdateException::class);
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = [
            'meta_title'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateProductLanguageException2(){
        $this->expectException(UpdateException::class);
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = [
            'meta_title'=>null
        ];
        $repo->update($productLanguage->id,$data);
    }

    public function testDeleteProductLanguage()
    {
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $delete = $repo->delete($productLanguage->id);
        $this->assertTrue($delete);
    }
    public function testDeleteProductLanguageException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $repo->delete(999);
    }

    public function testGetProductLanguageMultiParam(){
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = $repo->findByMultiParams([
            'meta_title'=>$productLanguage->meta_title,
            'product_id'=>$productLanguage->product_id
        ]);
        $this->assertEquals($data[0]->meta_title,$productLanguage->meta_title);
        $this->assertEquals($data[0]->description,$productLanguage->description);
    }

    public function testGetProductLanguageMultiParamException(){
        $this->expectException(NotFoundException::class);
        $productLanguage = ProductLanguage::factory()->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $repo->findByMultiParams([
            'meta_title'=>$this->faker->word,
            'product_id'=>$productLanguage->product_id
        ]);
    }

    public function testGetAllProductLanguages(){
        $productLanguages = ProductLanguage::factory()->times(10)->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $data = $repo->all();
        $this->assertEquals($productLanguages->count(),$data->count());
    }
    public function testGetAllProductLanguagesException(){
        $this->expectException(NotFoundException::class);
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $repo->all();
    }
    public function testGetAllProductLanguagesPaginate(){
        ProductLanguage::factory()->times(10)->create();
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllProductLanguagesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ProductLanguageRepository(new ProductLanguage());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
