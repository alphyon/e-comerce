<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Image;
use App\Models\ImageShowCase;
use App\Models\Product;
use App\Repositories\ImageProductRepository;
use Tests\TestCase;

class ImageProductRepositoryTest extends TestCase
{
    public function testCreateImageProduct()
    {
        $data = [
            'product_id'=>Product::factory()->create()->id,
            'image_id'=>Image::factory()->create()->id,
        ];
        $repo = new ImageProductRepository(new ImageShowCase());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['product_id'], $dataRepo->product_id);
        $this->assertEquals($data['image_id'], $dataRepo->image_id);
    }

    public function testCreateImageProductException()
    {
        $this->expectException(CreateException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $repo->create([]);
    }

    public function testGetImageProduct()
    {
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = $repo->find($imageProduct->id);
        $this->assertEquals($imageProduct->product_id, $data->product_id);
    }
    public function testGetImageProductException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $repo->find(999);
    }

    public function testGetImageProductByParam()
    {
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = $repo->findByParam('image_id',$imageProduct->image_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($imageProduct->product_id, $data[0]->product_id);
    }

    public function testGetImageProductByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $repo->findByParam('product_id',$this->faker->word);
    }

    public function testUpdateImageProduct(){
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = [
            'product_id'=>Product::factory()->create()->id,
        ];
        $update = $repo->update($imageProduct->id,$data);
        $this->assertEquals($update->product_id, $data['product_id']);
    }

    public function testUpdateImageProductException1(){
        $this->expectException(UpdateException::class);
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = [
            'product_id'=>Product::factory()->create()->id,
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateImageProductException2(){
        $this->expectException(UpdateException::class);
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = [
            'product_id'=>null,
        ];
        $repo->update($imageProduct->id,$data);
    }

    public function testDeleteImageProduct()
    {
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $delete = $repo->delete($imageProduct->id);
        $this->assertTrue($delete);
    }
    public function testDeleteImageProductException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $repo->delete(999);
    }

    public function testGetImageProductMultiParam(){
        $imageProduct = ImageShowCase::factory()->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = $repo->findByMultiParams([
            'product_id'=>$imageProduct->product_id,
            'image_id'=>$imageProduct->image_id,
        ]);
        $this->assertEquals($data[0]->product_id,$imageProduct->product_id);
        $this->assertEquals($data[0]->image_id,$imageProduct->image_id);
    }

    public function testGetImageProductMultiParamException(){
        $this->expectException(NotFoundException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $repo->findByMultiParams([
            'product_id'=>Product::factory()->create()->id,
            'image_id'=>Image::factory()->create()->id,
        ]);
    }

    public function testGetAllImageProducts(){
        $imageProducts = ImageShowCase::factory()->times(10)->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $data = $repo->all();
        $this->assertEquals($imageProducts->count(),$data->count());
    }
    public function testGetAllImageProductsException(){
        $this->expectException(NotFoundException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $repo->all();
    }
    public function testGetAllImageProductsPaginate(){
        ImageShowCase::factory()->times(10)->create();
        $repo = new ImageProductRepository(new ImageShowCase());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllImageProductsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ImageProductRepository(new ImageShowCase());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
