<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Family;
use App\Models\RolFamily;
use App\Models\User;
use App\Repositories\RolFamilyRepository;
use Tests\TestCase;

class RolFamilyRepositoryTest extends TestCase
{
    public function testCreateRolFamily()
    {
        $data = [
            'user_id'=>User::factory()->create()->id,
            'family_id'=>Family::factory()->create()->id,
            'permission'=>$this->faker->boolean,
        ];
        $repo = new RolFamilyRepository(new RolFamily());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['user_id'], $dataRepo->user_id);
        $this->assertEquals($data['permission'], $dataRepo->permission);
    }

    public function testCreateRolFamilyException()
    {
        $this->expectException(CreateException::class);
        $repo = new RolFamilyRepository(new RolFamily());
        $repo->create([]);
    }

    public function testGetRolFamily()
    {
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = $repo->find($rolFamily->id);
        $this->assertEquals($rolFamily->permission, $data->permission);
        $this->assertEquals($rolFamily->user_id, $data->user_id);
    }
    public function testGetRolFamilyException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RolFamilyRepository(new RolFamily());
        $repo->find(999);
    }

    public function testGetRolFamilyByParam()
    {
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = $repo->findByParam('user_id',$rolFamily->user_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($rolFamily->user_id, $data[0]->user_id);
        $this->assertEquals($rolFamily->permission, $data[0]->permission);
    }

    public function testGetRolFamilyByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RolFamilyRepository(new RolFamily());
        $repo->findByParam('permission',$this->faker->word);
    }

    public function testUpdateRolFamily(){
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = [
            'permission'=> !$rolFamily->permission
        ];
        $update = $repo->update($rolFamily->id,$data);
        $this->assertEquals($update->permission, $data['permission']);
    }

    public function testUpdateRolFamilyException1(){
        $this->expectException(UpdateException::class);
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = [
            'permission'=>$this->faker->boolean
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateRolFamilyException2(){
        $this->expectException(UpdateException::class);
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = [
            'permission'=>null
        ];
        $repo->update($rolFamily->id,$data);
    }

    public function testDeleteRolFamily()
    {
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $delete = $repo->delete($rolFamily->id);
        $this->assertTrue($delete);
    }
    public function testDeleteRolFamilyException()
    {
        $this->expectException(DeleteException::class);
        $repo = new RolFamilyRepository(new RolFamily());
        $repo->delete(999);
    }

    public function testGetRolFamilyMultiParam(){
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = $repo->findByMultiParams([
            'user_id'=>$rolFamily->user_id,
            'permission'=>$rolFamily->permission
        ]);
        $this->assertEquals($data[0]->user_id,$rolFamily->user_id);
        $this->assertEquals($data[0]->permission,$rolFamily->permission);
    }

    public function testGetRolFamilyMultiParamException(){
        $this->expectException(NotFoundException::class);
        $rolFamily = RolFamily::factory()->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $repo->findByMultiParams([
            'permission'=>$this->faker->boolean,
            'user_id'=>9999
        ]);
    }

    public function testGetAllRolFamilys(){
        $rolFamilys = RolFamily::factory()->times(10)->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $data = $repo->all();
        $this->assertEquals($rolFamilys->count(),$data->count());
    }
    public function testGetAllRolFamilysException(){
        $this->expectException(NotFoundException::class);
        $repo = new RolFamilyRepository(new RolFamily());
        $repo->all();
    }
    public function testGetAllRolFamilysPaginate(){
        RolFamily::factory()->times(10)->create();
        $repo = new RolFamilyRepository(new RolFamily());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllRolFamilysPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new RolFamilyRepository(new RolFamily());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
