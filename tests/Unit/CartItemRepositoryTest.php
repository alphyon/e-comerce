<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Version;
use App\Repositories\CartItemRepository;
use Tests\TestCase;

class CartItemRepositoryTest extends TestCase
{
    public function testCreateCartItem()
    {
        $data = [
            'version_id'=>Version::factory()->create()->id,
            'cart_id'=>Cart::factory()->create()->id,
            'price'=>$this->faker->numberBetween(1000,9000),
            'quantity'=>$this->faker->numerify('##'),
        ];
        $repo = new CartItemRepository(new CartItem());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['version_id'], $dataRepo->version_id);
        $this->assertEquals($data['price'], $dataRepo->price);
    }

    public function testCreateCartItemException()
    {
        $this->expectException(CreateException::class);
        $repo = new CartItemRepository(new CartItem());
        $repo->create([]);
    }

    public function testGetCartItem()
    {
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $data = $repo->find($cartItem->id);
        $this->assertEquals($cartItem->version_id, $data->version_id);
        $this->assertEquals($cartItem->price, $data->price);
    }
    public function testGetCartItemException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CartItemRepository(new CartItem());
        $repo->find(999);
    }

    public function testGetCartItemByParam()
    {
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $data = $repo->findByParam('version_id',$cartItem->version_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($cartItem->version_id, $data[0]->version_id);
        $this->assertEquals($cartItem->price, $data[0]->price);
    }

    public function testGetCartItemByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CartItemRepository(new CartItem());
        $repo->findByParam('version_id',$this->faker->word);
    }

    public function testUpdateCartItem(){
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $data = [
            'quantity'=> $this->faker->numerify('##')
        ];
        $update = $repo->update($cartItem->id,$data);
        $this->assertEquals($update->quantity, $data['quantity']);
    }

    public function testUpdateCartItemException1(){
        $this->expectException(UpdateException::class);
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $data = [
            'quantity'=> $this->faker->numerify('##')
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateCartItemException2(){
        $this->expectException(UpdateException::class);
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $data = [
            'quantity'=> null
        ];
        $repo->update($cartItem->id,$data);
    }

    public function testDeleteCartItem()
    {
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $delete = $repo->delete($cartItem->id);
        $this->assertTrue($delete);
    }
    public function testDeleteCartItemException()
    {
        $this->expectException(DeleteException::class);
        $repo = new CartItemRepository(new CartItem());
        $repo->delete(999);
    }

    public function testGetCartItemMultiParam(){
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $data = $repo->findByMultiParams([
            'quantity'=>$cartItem->quantity,
            'version_id'=>$cartItem->version_id
        ]);
        $this->assertEquals($data[0]->cart_id,$cartItem->cart_id);
        $this->assertEquals($data[0]->price,$cartItem->price);
    }

    public function testGetCartItemMultiParamException(){
        $this->expectException(NotFoundException::class);
        $cartItem = CartItem::factory()->create();
        $repo = new CartItemRepository(new CartItem());
        $repo->findByMultiParams([
            'quantity'=>999,
            'version_id'=>999
        ]);
    }

    public function testGetAllCartItems(){
        $cartItems = CartItem::factory()->times(10)->create();
        $repo = new CartItemRepository(new CartItem());
        $data = $repo->all();
        $this->assertEquals($cartItems->count(),$data->count());
    }
    public function testGetAllCartItemsException(){
        $this->expectException(NotFoundException::class);
        $repo = new CartItemRepository(new CartItem());
        $repo->all();
    }
    public function testGetAllCartItemsPaginate(){
        CartItem::factory()->times(10)->create();
        $repo = new CartItemRepository(new CartItem());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllCartItemsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new CartItemRepository(new CartItem());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
