<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Delivery;
use App\Models\User;
use App\Repositories\DeliveryRepository;
use Tests\TestCase;

class DeliveryRepositoryTest extends TestCase
{
    public function testCreateDelivery()
    {
        $data = [
            'address'=>$this->faker->address,
            'city'=>$this->faker->city,
            'state'=>$this->faker->state,
            'country'=>$this->faker->country,
            'postcode'=>$this->faker->postcode,
            'deliverable_id'=>User::factory()->create()->id,
            'deliverable_type'=>User::class
        ];
        $repo = new DeliveryRepository(new Delivery());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['address'], $dataRepo->address);
        $this->assertEquals($data['postcode'], $dataRepo->postcode);
    }

    public function testCreateDeliveryException()
    {
        $this->expectException(CreateException::class);
        $repo = new DeliveryRepository(new Delivery());
        $repo->create([]);
    }

    public function testGetDelivery()
    {
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = $repo->find($delivery->id);
        $this->assertEquals($delivery->address, $data->address);
        $this->assertEquals($delivery->postcode, $data->postcode);
    }
    public function testGetDeliveryException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new DeliveryRepository(new Delivery());
        $repo->find(999);
    }

    public function testGetDeliveryByParam()
    {
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = $repo->findByParam('address',$delivery->address);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($delivery->address, $data[0]->address);
        $this->assertEquals($delivery->postcode, $data[0]->postcode);
    }

    public function testGetDeliveryByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new DeliveryRepository(new Delivery());
        $repo->findByParam('address',$this->faker->word);
    }

    public function testUpdateDelivery(){
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = [
            'address'=>$this->faker->word
        ];
        $update = $repo->update($delivery->id,$data);
        $this->assertEquals($update->address, $data['address']);
    }

    public function testUpdateDeliveryException1(){
        $this->expectException(UpdateException::class);
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = [
            'address'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateDeliveryException2(){
        $this->expectException(UpdateException::class);
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = [
            'address'=>null
        ];
        $repo->update($delivery->id,$data);
    }

    public function testDeleteDelivery()
    {
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $delete = $repo->delete($delivery->id);
        $this->assertTrue($delete);
    }
    public function testDeleteDeliveryException()
    {
        $this->expectException(DeleteException::class);
        $repo = new DeliveryRepository(new Delivery());
        $repo->delete(999);
    }

    public function testGetDeliveryMultiParam(){
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = $repo->findByMultiParams([
            'address'=>$delivery->address,
            'postcode'=>$delivery->postcode
        ]);
        $this->assertEquals($data[0]->address,$delivery->address);
        $this->assertEquals($data[0]->postcode,$delivery->postcode);
    }

    public function testGetDeliveryMultiParamException(){
        $this->expectException(NotFoundException::class);
        $delivery = Delivery::factory()->create();
        $repo = new DeliveryRepository(new Delivery());
        $repo->findByMultiParams([
            'address'=>$this->faker->word,
            'postcode'=>$delivery->postcode
        ]);
    }

    public function testGetAllDeliverys(){
        $deliverys = Delivery::factory()->times(10)->create();
        $repo = new DeliveryRepository(new Delivery());
        $data = $repo->all();
        $this->assertEquals($deliverys->count(),$data->count());
    }
    public function testGetAllDeliverysException(){
        $this->expectException(NotFoundException::class);
        $repo = new DeliveryRepository(new Delivery());
        $repo->all();
    }
    public function testGetAllDeliverysPaginate(){
        Delivery::factory()->times(10)->create();
        $repo = new DeliveryRepository(new Delivery());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllDeliverysPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new DeliveryRepository(new Delivery());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
