<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Contact;
use App\Models\User;use App\Repositories\ContactRepository;
use Tests\TestCase;

class ContactRepositoryTest extends TestCase
{
    public function testCreateContact()
    {
        $data = [
            'user_id'=>User::factory()->create()->id,
            'value'=>$this->faker->numerify('###### ###'),
            'reference'=>$this->faker->word
        ];
        $repo = new ContactRepository(new Contact());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['value'], $dataRepo->value);
        $this->assertEquals($data['reference'], $dataRepo->reference);
    }

    public function testCreateContactException()
    {
        $this->expectException(CreateException::class);
        $repo = new ContactRepository(new Contact());
        $repo->create([]);
    }

    public function testGetContact()
    {
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $data = $repo->find($contact->id);
        $this->assertEquals($contact->value, $data->value);
        $this->assertEquals($contact->reference, $data->reference);
    }
    public function testGetContactException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ContactRepository(new Contact());
        $repo->find(999);
    }

    public function testGetContactByParam()
    {
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $data = $repo->findByParam('value',$contact->value);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($contact->value, $data[0]->value);
        $this->assertEquals($contact->reference, $data[0]->reference);
    }

    public function testGetContactByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ContactRepository(new Contact());
        $repo->findByParam('value',$this->faker->word);
    }

    public function testUpdateContact(){
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $data = [
            'value'=>$this->faker->word
        ];
        $update = $repo->update($contact->id,$data);
        $this->assertEquals($update->value, $data['value']);
    }

    public function testUpdateContactException1(){
        $this->expectException(UpdateException::class);
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $data = [
            'value'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateContactException2(){
        $this->expectException(UpdateException::class);
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $data = [
            'value'=>null
        ];
        $repo->update($contact->id,$data);
    }

    public function testDeleteContact()
    {
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $delete = $repo->delete($contact->id);
        $this->assertTrue($delete);
    }
    public function testDeleteContactException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ContactRepository(new Contact());
        $repo->delete(999);
    }

    public function testGetContactMultiParam(){
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $data = $repo->findByMultiParams([
            'value'=>$contact->value,
            'reference'=>$contact->reference
        ]);
        $this->assertEquals($data[0]->value,$contact->value);
        $this->assertEquals($data[0]->reference,$contact->reference);
    }

    public function testGetContactMultiParamException(){
        $this->expectException(NotFoundException::class);
        $contact = Contact::factory()->create();
        $repo = new ContactRepository(new Contact());
        $repo->findByMultiParams([
            'value'=>$this->faker->word,
            'reference'=>$contact->reference
        ]);
    }

    public function testGetAllContacts(){
        $contacts = Contact::factory()->times(10)->create();
        $repo = new ContactRepository(new Contact());
        $data = $repo->all();
        $this->assertEquals($contacts->count(),$data->count());
    }
    public function testGetAllContactsException(){
        $this->expectException(NotFoundException::class);
        $repo = new ContactRepository(new Contact());
        $repo->all();
    }
    public function testGetAllContactsPaginate(){
        Contact::factory()->times(10)->create();
        $repo = new ContactRepository(new Contact());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllContactsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ContactRepository(new Contact());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
