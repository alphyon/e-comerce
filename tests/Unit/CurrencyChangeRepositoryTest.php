<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Currency;
use App\Models\CurrencyChange;
use App\Repositories\CurrencyChangeRepository;
use Tests\TestCase;

class CurrencyChangeRepositoryTest extends TestCase
{
    public function testCreateCurrencyChange()
    {
        $data = [
            'currency_id'=>Currency::factory()->create()->id,
            'currency_name_to_change'=>$this->faker->word,
            'change_value'=>$this->faker->randomDigit,
        ];
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['currency_name_to_change'], $dataRepo->currency_name_to_change);
        $this->assertEquals($data['change_value'], $dataRepo->change_value);
    }

    public function testCreateCurrencyChangeException()
    {
        $this->expectException(CreateException::class);
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $repo->create([]);
    }

    public function testGetCurrencyChange()
    {
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = $repo->find($role->id);
        $this->assertEquals($role->currency_name_to_change, $data->currency_name_to_change);
        $this->assertEquals($role->change_value, $data->change_value);
    }
    public function testGetCurrencyChangeException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $repo->find(999);
    }

    public function testGetCurrencyChangeByParam()
    {
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = $repo->findByParam('currency_name_to_change',$role->currency_name_to_change);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($role->currency_name_to_change, $data[0]->currency_name_to_change);
        $this->assertEquals($role->change_value, $data[0]->change_value);
    }

    public function testGetCurrencyChangeByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $repo->findByParam('currency_name_to_change',$this->faker->word);
    }

    public function testUpdateCurrencyChange(){
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = [
            'currency_name_to_change'=>$this->faker->word
        ];
        $update = $repo->update($role->id,$data);
        $this->assertEquals($update->currency_name_to_change, $data['currency_name_to_change']);
    }

    public function testUpdateCurrencyChangeException1(){
        $this->expectException(UpdateException::class);
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = [
            'currency_name_to_change'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateCurrencyChangeException2(){
        $this->expectException(UpdateException::class);
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = [
            'currency_name_to_change'=>null
        ];
        $repo->update($role->id,$data);
    }

    public function testDeleteCurrencyChange()
    {
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $delete = $repo->delete($role->id);
        $this->assertTrue($delete);
    }
    public function testDeleteCurrencyChangeException()
    {
        $this->expectException(DeleteException::class);
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $repo->delete(999);
    }

    public function testGetCurrencyChangeMultiParam(){
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = $repo->findByMultiParams([
            'currency_name_to_change'=>$role->currency_name_to_change,
            'change_value'=>$role->change_value
        ]);
        $this->assertEquals($data[0]->currency_name_to_change,$role->currency_name_to_change);
        $this->assertEquals($data[0]->change_value,$role->change_value);
    }

    public function testGetCurrencyChangeMultiParamException(){
        $this->expectException(NotFoundException::class);
        $role = CurrencyChange::factory()->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $repo->findByMultiParams([
            'currency_name_to_change'=>$this->faker->word,
            'change_value'=>$role->change_value
        ]);
    }

    public function testGetAllCurrencyChanges(){
        $roles = CurrencyChange::factory()->times(10)->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $data = $repo->all();
        $this->assertEquals($roles->count(),$data->count());
    }
    public function testGetAllCurrencyChangesException(){
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $repo->all();
    }
    public function testGetAllCurrencyChangesPaginate(){
        CurrencyChange::factory()->times(10)->create();
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllCurrencyChangesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyChangeRepository(new CurrencyChange());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
