<?php

namespace Tests\Unit;


use App\Http\Controllers\DumpDatabase;
use Tests\TestCase;

class DumpDataBaseTest extends TestCase
{
   public function test_dump_file_stock(){
       $new = new DumpDatabase();
       $file = $new->fillProductsAndVersions("ARTICULOS","ConnectPlain/devtest");

       $this->assertTrue($file);
   }

    public function test_dump_file_clients(){
        $new = new DumpDatabase();
        $var_fil = "CLIENTES";
        $file2 = $new->fillUsers($var_fil,"ConnectPlain/devtest");

        $this->assertTrue($file2);
    }
}
