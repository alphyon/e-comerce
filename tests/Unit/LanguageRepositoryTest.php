<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Language;
use App\Repositories\LanguageRepository;
use Tests\TestCase;

class LanguageRepositoryTest extends TestCase
{
    public function testCreateLanguage()
    {
        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->slug(1)
        ];
        $repo = new LanguageRepository(new Language());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['slug'], $dataRepo->slug);
    }

    public function testCreateLanguageException()
    {
        $this->expectException(CreateException::class);
        $repo = new LanguageRepository(new Language());
        $repo->create([]);
    }

    public function testGetLanguage()
    {
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $data = $repo->find($language->id);
        $this->assertEquals($language->name, $data->name);
        $this->assertEquals($language->slug, $data->slug);
    }
    public function testGetLanguageException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new LanguageRepository(new Language());
        $repo->find(999);
    }

    public function testGetLanguageByParam()
    {
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $data = $repo->findByParam('name',$language->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($language->name, $data[0]->name);
        $this->assertEquals($language->slug, $data[0]->slug);
    }

    public function testGetLanguageByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new LanguageRepository(new Language());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateLanguage(){
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($language->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateLanguageException1(){
        $this->expectException(UpdateException::class);
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateLanguageException2(){
        $this->expectException(UpdateException::class);
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $data = [
            'name'=>null
        ];
        $repo->update($language->id,$data);
    }

    public function testDeleteLanguage()
    {
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $delete = $repo->delete($language->id);
        $this->assertTrue($delete);
    }
    public function testDeleteLanguageException()
    {
        $this->expectException(DeleteException::class);
        $repo = new LanguageRepository(new Language());
        $repo->delete(999);
    }

    public function testGetLanguageMultiParam(){
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $data = $repo->findByMultiParams([
            'name'=>$language->name,
            'slug'=>$language->slug
        ]);
        $this->assertEquals($data[0]->name,$language->name);
        $this->assertEquals($data[0]->slug,$language->slug);
    }

    public function testGetLanguageMultiParamException(){
        $this->expectException(NotFoundException::class);
        $language = Language::factory()->create();
        $repo = new LanguageRepository(new Language());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'slug'=>$language->slug
        ]);
    }

    public function testGetAllLanguages(){
        $languages = Language::factory()->times(10)->create();
        $repo = new LanguageRepository(new Language());
        $data = $repo->all();
        $this->assertEquals($languages->count(),$data->count());
    }
    public function testGetAllLanguagesException(){
        $this->expectException(NotFoundException::class);
        $repo = new LanguageRepository(new Language());
        $repo->all();
    }
    public function testGetAllLanguagesPaginate(){
        Language::factory()->times(10)->create();
        $repo = new LanguageRepository(new Language());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllLanguagesPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new LanguageRepository(new Language());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
