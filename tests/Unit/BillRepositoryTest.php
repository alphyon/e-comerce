<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Bill;
use App\Models\Order;
use App\Models\User;
use App\Repositories\BillRepository;
use Tests\TestCase;

class BillRepositoryTest extends TestCase
{
    public function testCreateBill()
    {
        $data = [
            'user_id'=>User::factory()->create()->id,
            'order_id'=>Order::factory()->create()->id,
            'number'=>$this->faker->randomNumber([1000,3000]),
            'price'=>$this->faker->randomNumber([1000,3000]),
            'date'=>$this->faker->dateTime
        ];
        $repo = new BillRepository(new Bill());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['number'], $dataRepo->number);
        $this->assertEquals($data['price'], $dataRepo->price);
    }

    public function testCreateBillException()
    {
        $this->expectException(CreateException::class);
        $repo = new BillRepository(new Bill());
        $repo->create([]);
    }

    public function testGetBill()
    {
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $data = $repo->find($bill->id);
        $this->assertEquals($bill->number, $data->number);
        $this->assertEquals($bill->price, $data->price);
    }
    public function testGetBillException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new BillRepository(new Bill());
        $repo->find(999);
    }

    public function testGetBillByParam()
    {
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $data = $repo->findByParam('user_id',$bill->user_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($bill->number, $data[0]->number);
        $this->assertEquals($bill->price, $data[0]->price);
    }

    public function testGetBillByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new BillRepository(new Bill());
        $repo->findByParam('user_id',999);
    }

    public function testUpdateBill(){
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $data = [
            'price'=>$this->faker->randomNumber([1000,3000]),
            'date'=>$this->faker->dateTime
        ];
        $update = $repo->update($bill->id,$data);
        $this->assertEquals($update->price, $data['price']);
    }

    public function testUpdateBillException1(){
        $this->expectException(UpdateException::class);
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $data = [
            'price'=>$this->faker->randomNumber([1000,3000]),
            'date'=>$this->faker->dateTime
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateBillException2(){
        $this->expectException(UpdateException::class);
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $data = [
            'price'=>null
        ];
        $repo->update($bill->id,$data);
    }

    public function testDeleteBill()
    {
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $delete = $repo->delete($bill->id);
        $this->assertTrue($delete);
    }
    public function testDeleteBillException()
    {
        $this->expectException(DeleteException::class);
        $repo = new BillRepository(new Bill());
        $repo->delete(999);
    }

    public function testGetBillMultiParam(){
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $data = $repo->findByMultiParams([
            'number'=>$bill->number,
            'price'=>$bill->price
        ]);
        $this->assertEquals($data[0]->number,$bill->number);
        $this->assertEquals($data[0]->price,$bill->price);
    }

    public function testGetBillMultiParamException(){
        $this->expectException(NotFoundException::class);
        $bill = Bill::factory()->create();
        $repo = new BillRepository(new Bill());
        $repo->findByMultiParams([
            'number'=>$this->faker->word,
            'price'=>$this->faker->randomDigit
        ]);
    }

    public function testGetAllBills(){
        $bills = Bill::factory()->times(10)->create();
        $repo = new BillRepository(new Bill());
        $data = $repo->all();
        $this->assertEquals($bills->count(),$data->count());
    }
    public function testGetAllBillsException(){
        $this->expectException(NotFoundException::class);
        $repo = new BillRepository(new Bill());
        $repo->all();
    }
    public function testGetAllBillsPaginate(){
        Bill::factory()->times(10)->create();
        $repo = new BillRepository(new Bill());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllBillsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new BillRepository(new Bill());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
