<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Color;
use App\Repositories\ColorRepository;
use Tests\TestCase;

class ColorRepositoryTest extends TestCase
{
    public function testCreateColor()
    {
        $data = [
            'name' => $this->faker->colorName,
            'code' => $this->faker->hexColor
        ];
        $repo = new ColorRepository(new Color());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['code'], $dataRepo->code);
    }

    public function testCreateColorException()
    {
        $this->expectException(CreateException::class);
        $repo = new ColorRepository(new Color());
        $repo->create([]);
    }

    public function testGetColor()
    {
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $data = $repo->find($Color->id);
        $this->assertEquals($Color->name, $data->name);
        $this->assertEquals($Color->slug, $data->slug);
    }
    public function testGetColorException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ColorRepository(new Color());
        $repo->find(999);
    }

    public function testGetColorByParam()
    {
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $data = $repo->findByParam('name',$Color->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($Color->name, $data[0]->name);
        $this->assertEquals($Color->slug, $data[0]->slug);
    }

    public function testGetColorByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new ColorRepository(new Color());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateColor(){
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($Color->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateColorException1(){
        $this->expectException(UpdateException::class);
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateColorException2(){
        $this->expectException(UpdateException::class);
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $data = [
            'name'=>null
        ];
        $repo->update($Color->id,$data);
    }

    public function testDeleteColor()
    {
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $delete = $repo->delete($Color->id);
        $this->assertTrue($delete);
    }
    public function testDeleteColorException()
    {
        $this->expectException(DeleteException::class);
        $repo = new ColorRepository(new Color());
        $repo->delete(999);
    }

    public function testGetColorMultiParam(){
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $data = $repo->findByMultiParams([
            'name'=>$Color->name,
            'code'=>$Color->code
        ]);
        $this->assertEquals($data[0]->name,$Color->name);
        $this->assertEquals($data[0]->code,$Color->code);
    }

    public function testGetColorMultiParamException(){
        $this->expectException(NotFoundException::class);
        $Color = Color::factory()->create();
        $repo = new ColorRepository(new Color());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'code'=>$Color->code
        ]);
    }

    public function testGetAllColors(){
        $Colors = Color::factory()->times(10)->create();
        $repo = new ColorRepository(new Color());
        $data = $repo->all();
        $this->assertEquals($Colors->count(),$data->count());
    }
    public function testGetAllColorsException(){
        $this->expectException(NotFoundException::class);
        $repo = new ColorRepository(new Color());
        $repo->all();
    }
    public function testGetAllColorsPaginate(){
        Color::factory()->times(10)->create();
        $repo = new ColorRepository(new Color());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllColorsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new ColorRepository(new Color());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
