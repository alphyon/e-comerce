<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Tests\TestCase;

class CategoryRepositoryTest extends TestCase
{
    public function testCreateCategory()
    {
        $data = [
            'name' => $this->faker->name,
            'active' => $this->faker->boolean
        ];
        $repo = new CategoryRepository(new Category());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['active'], $dataRepo->active);
    }

    public function testCreateCategoryException()
    {
        $this->expectException(CreateException::class);
        $repo = new CategoryRepository(new Category());
        $repo->create([]);
    }

    public function testGetCategory()
    {
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $data = $repo->find($category->id);
        $this->assertEquals($category->name, $data->name);
        $this->assertEquals($category->active, $data->active);
    }
    public function testGetCategoryException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CategoryRepository(new Category());
        $repo->find(999);
    }

    public function testGetCategoryByParam()
    {
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $data = $repo->findByParam('name',$category->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($category->name, $data[0]->name);
        $this->assertEquals($category->active, $data[0]->active);
    }

    public function testGetCategoryByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CategoryRepository(new Category());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateCategory(){
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($category->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateCategoryException1(){
        $this->expectException(UpdateException::class);
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateCategoryException2(){
        $this->expectException(UpdateException::class);
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $data = [
            'name'=>null
        ];
        $repo->update($category->id,$data);
    }

    public function testDeleteCategory()
    {
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $delete = $repo->delete($category->id);
        $this->assertTrue($delete);
    }
    public function testDeleteCategoryException()
    {
        $this->expectException(DeleteException::class);
        $repo = new CategoryRepository(new Category());
        $repo->delete(999);
    }

    public function testGetCategoryMultiParam(){
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $data = $repo->findByMultiParams([
            'name'=>$category->name,
            'active'=>$category->active
        ]);
        $this->assertEquals($data[0]->name,$category->name);
        $this->assertEquals($data[0]->active,$category->active);
    }

    public function testGetCategoryMultiParamException(){
        $this->expectException(NotFoundException::class);
        $category = Category::factory()->create();
        $repo = new CategoryRepository(new Category());
        $repo->findByMultiParams([
            'name'=>$this->faker->word,
            'active'=>$category->active
        ]);
    }

    public function testGetAllCategorys(){
        $categorys = Category::factory()->times(10)->create();
        $repo = new CategoryRepository(new Category());
        $data = $repo->all();
        $this->assertEquals($categorys->count(),$data->count());
    }
    public function testGetAllCategorysException(){
        $this->expectException(NotFoundException::class);
        $repo = new CategoryRepository(new Category());
        $repo->all();
    }
    public function testGetAllCategorysPaginate(){
        Category::factory()->times(10)->create();
        $repo = new CategoryRepository(new Category());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllCategorysPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new CategoryRepository(new Category());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
