<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Currency;
use App\Repositories\CurrencyRepository;
use Tests\TestCase;

class CurrencyRepositoryTest extends TestCase
{
    public function testCreateCurrency()
    {
        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->slug(1)
        ];
        $repo = new CurrencyRepository(new Currency());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
        $this->assertEquals($data['slug'], $dataRepo->slug);
    }

    public function testCreateCurrencyException()
    {
        $this->expectException(CreateException::class);
        $repo = new CurrencyRepository(new Currency());
        $repo->create([]);
    }

    public function testGetCurrency()
    {
        $currency = Currency::factory()->create();
        $repo = new CurrencyRepository(new Currency());
        $data = $repo->find($currency->id);
        $this->assertEquals($currency->name, $data->name);
        $this->assertEquals($currency->slug, $data->slug);
    }
    public function testGetCurrencyException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyRepository(new Currency());
        $repo->find(999);
    }

    public function testGetCurrencyByParam()
    {
        $currency = Currency::factory()->create();
        $repo = new CurrencyRepository(new Currency());
        $data = $repo->findByParam('name',$currency->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($currency->name, $data[0]->name);
        $this->assertEquals($currency->slug, $data[0]->slug);
    }

    public function testGetCurrencyByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyRepository(new Currency());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateCurrency(){
        $currency = Currency::factory()->create();
        $repo = new CurrencyRepository(new Currency());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($currency->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateCurrencyException1(){
        $this->expectException(UpdateException::class);
        $currency = Currency::factory()->create();
        $repo = new CurrencyRepository(new Currency());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateCurrencyException2(){
        $this->expectException(UpdateException::class);
        $currency = Currency::factory()->create();
        $repo = new CurrencyRepository(new Currency());
        $data = [
            'name'=>null
        ];
        $repo->update($currency->id,$data);
    }

    public function testDeleteCurrency()
    {
        $currency = Currency::factory()->create();
        $repo = new CurrencyRepository(new Currency());
        $delete = $repo->delete($currency->id);
        $this->assertTrue($delete);
    }
    public function testDeleteCurrencyException()
    {
        $this->expectException(DeleteException::class);
        $repo = new CurrencyRepository(new Currency());
        $repo->delete(999);
    }




    public function testGetAllCurrencys(){
        $currencys = Currency::factory()->times(10)->create();
        $repo = new CurrencyRepository(new Currency());
        $data = $repo->all();
        $this->assertEquals($currencys->count(),$data->count());
    }
    public function testGetAllCurrencysException(){
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyRepository(new Currency());
        $repo->all();
    }
    public function testGetAllCurrencysPaginate(){
        Currency::factory()->times(10)->create();
        $repo = new CurrencyRepository(new Currency());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllCurrencysPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new CurrencyRepository(new Currency());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
