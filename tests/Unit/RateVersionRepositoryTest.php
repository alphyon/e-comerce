<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Rate;
use App\Models\Version;
use App\Models\RateVersion;
use App\Repositories\RateVersionRepository;
use Tests\TestCase;

class RateVersionRepositoryTest extends TestCase
{
    public function testCreateRateVersion()
    {
        $data = [
            'version_id'=>Version::factory()->create()->id,
            'rate_id'=>Rate::factory()->create()->id,
            'price'=>$this->faker->numerify('##')
        ];
        $repo = new RateVersionRepository(new RateVersion());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['version_id'], $dataRepo->version_id);
        $this->assertEquals($data['rate_id'], $dataRepo->rate_id);
    }

    public function testCreateRateVersionException()
    {
        $this->expectException(CreateException::class);
        $repo = new RateVersionRepository(new RateVersion());
        $repo->create([]);
    }

    public function testGetRateVersion()
    {
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = $repo->find($rateVersion->id);
        $this->assertEquals($rateVersion->version_id, $data->version_id);
        $this->assertEquals($rateVersion->rate_id, $data->rate_id);
    }
    public function testGetRateVersionException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RateVersionRepository(new RateVersion());
        $repo->find(999);
    }

    public function testGetRateVersionByParam()
    {
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = $repo->findByParam('version_id',$rateVersion->version_id);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($rateVersion->version_id, $data[0]->version_id);
        $this->assertEquals($rateVersion->rate_id, $data[0]->rate_id);
    }

    public function testGetRateVersionByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new RateVersionRepository(new RateVersion());
        $repo->findByParam('version_id',$this->faker->word);
    }

    public function testUpdateRateVersion(){
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = [
            'version_id'=>Version::factory()->create()->id
        ];
        $update = $repo->update($rateVersion->id,$data);
        $this->assertEquals($update->version_id, $data['version_id']);
    }

    public function testUpdateRateVersionException1(){
        $this->expectException(UpdateException::class);
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = [
            'version_id'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateRateVersionException2(){
        $this->expectException(UpdateException::class);
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = [
            'version_id'=>null
        ];
        $repo->update($rateVersion->id,$data);
    }

    public function testDeleteRateVersion()
    {
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $delete = $repo->delete($rateVersion->id);
        $this->assertTrue($delete);
    }
    public function testDeleteRateVersionException()
    {
        $this->expectException(DeleteException::class);
        $repo = new RateVersionRepository(new RateVersion());
        $repo->delete(999);
    }

    public function testGetRateVersionMultiParam(){
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = $repo->findByMultiParams([
            'version_id'=>$rateVersion->version_id,
            'rate_id'=>$rateVersion->rate_id
        ]);
        $this->assertEquals($data[0]->version_id,$rateVersion->version_id);
        $this->assertEquals($data[0]->rate_id,$rateVersion->rate_id);
    }

    public function testGetRateVersionMultiParamException(){
        $this->expectException(NotFoundException::class);
        $rateVersion = RateVersion::factory()->create();
        $repo = new RateVersionRepository(new RateVersion());
        $repo->findByMultiParams([
            'version_id'=>$this->faker->word,
            'rate_id'=>$rateVersion->rate_id
        ]);
    }

    public function testGetAllRateVersions(){
        $rateVersions = RateVersion::factory()->times(10)->create();
        $repo = new RateVersionRepository(new RateVersion());
        $data = $repo->all();
        $this->assertEquals($rateVersions->count(),$data->count());
    }
    public function testGetAllRateVersionsException(){
        $this->expectException(NotFoundException::class);
        $repo = new RateVersionRepository(new RateVersion());
        $repo->all();
    }
    public function testGetAllRateVersionsPaginate(){
        RateVersion::factory()->times(10)->create();
        $repo = new RateVersionRepository(new RateVersion());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllRateVersionsPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new RateVersionRepository(new RateVersion());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
