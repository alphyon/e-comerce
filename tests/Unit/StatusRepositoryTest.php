<?php

namespace Tests\Unit;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Models\Status;
use App\Repositories\StatusRepository;
use Tests\TestCase;

class StatusRepositoryTest extends TestCase
{
    public function testCreateStatus()
    {
        $data = [
            'name' => $this->faker->name,
        ];
        $repo = new StatusRepository(new Status());
        $dataRepo = $repo->create($data);
        $this->assertEquals($data['name'], $dataRepo->name);
    }

    public function testCreateStatusException()
    {
        $this->expectException(CreateException::class);
        $repo = new StatusRepository(new Status());
        $repo->create([]);
    }

    public function testGetStatus()
    {
        $status = Status::factory()->create();
        $repo = new StatusRepository(new Status());
        $data = $repo->find($status->id);
        $this->assertEquals($status->name, $data->name);
    }
    public function testGetStatusException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new StatusRepository(new Status());
        $repo->find(999);
    }

    public function testGetStatusByParam()
    {
        $status = Status::factory()->create();
        $repo = new StatusRepository(new Status());
        $data = $repo->findByParam('name',$status->name);
        $this->assertGreaterThan(0,$data->count());
        $this->assertEquals($status->name, $data[0]->name);
    }

    public function testGetStatusByParamException()
    {
        $this->expectException(NotFoundException::class);
        $repo = new StatusRepository(new Status());
        $repo->findByParam('name',$this->faker->word);
    }

    public function testUpdateStatus(){
        $status = Status::factory()->create();
        $repo = new StatusRepository(new Status());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update($status->id,$data);
        $this->assertEquals($update->name, $data['name']);
    }

    public function testUpdateStatusException1(){
        $this->expectException(UpdateException::class);
        $status = Status::factory()->create();
        $repo = new StatusRepository(new Status());
        $data = [
            'name'=>$this->faker->word
        ];
        $update = $repo->update(999,$data);
    }
    public function testUpdateStatusException2(){
        $this->expectException(UpdateException::class);
        $status = Status::factory()->create();
        $repo = new StatusRepository(new Status());
        $data = [
            'name'=>null
        ];
        $repo->update($status->id,$data);
    }

    public function testDeleteStatus()
    {
        $status = Status::factory()->create();
        $repo = new StatusRepository(new Status());
        $delete = $repo->delete($status->id);
        $this->assertTrue($delete);
    }
    public function testDeleteStatusException()
    {
        $this->expectException(DeleteException::class);
        $repo = new StatusRepository(new Status());
        $repo->delete(999);
    }

    public function testGetAllStatuss(){
        $statuss = Status::factory()->times(10)->create();
        $repo = new StatusRepository(new Status());
        $data = $repo->all();
        $this->assertEquals($statuss->count(),$data->count());
    }
    public function testGetAllStatussException(){
        $this->expectException(NotFoundException::class);
        $repo = new StatusRepository(new Status());
        $repo->all();
    }
    public function testGetAllStatussPaginate(){
        Status::factory()->times(10)->create();
        $repo = new StatusRepository(new Status());
        $testNumber =5;
        $data = $repo->allPaginate($testNumber);
        $this->assertEquals($testNumber,$data->count());
    }

    public function testGetAllStatussPaginateException(){
        $this->expectException(NotFoundException::class);
        $repo = new StatusRepository(new Status());
        $testNumber =5;
        $repo->allPaginate($testNumber);
    }
}
