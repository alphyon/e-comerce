<?php
return [
    'messages'=>[
        'exceptions'=>[
           'not_found_exception'=>'not found data',
           'not_update_exception'=>'an error occurred  when intent update data',
           'not_delete_exception'=>'an error occurred when intent delete data',
           'not_create_exception'=>'an error occurred when create data',
            'user_not_active'=>'user no active  '
        ]
    ],
    'rol'=>[
        'USER'=>'user',
        'ADMIN'=>'admin'
    ],
];

