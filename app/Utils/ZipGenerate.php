<?php


namespace App\Utils;


use Exception;
use Illuminate\Support\Facades\Storage;

class ZipGenerate
{

    public function download($path,$namezip='file.zip')
    {

        try {


            $zip = new \ZipArchive();
            if (true === ($zip->open($namezip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE))) {
                foreach (Storage::allFiles('public/'.$path) as $file) {
                    $name = basename($file);
                    if ($name !== '.gitignore') {

                        $zip->addFile(public_path("storage/".$path."/" . $name), $name);
                    }
                }
                $zip->close();
            }

            return  $namezip;


        } catch (Exception $exception) {
          throw new Exception("NOT CREATE FILE");
        }
    }
}
