<?php


namespace App\Utils;


use Illuminate\Support\Facades\Log;

class LogManager
{
    public function ErrorLog($message,$trace='',$ref = '') :void
    {
        $msg = sprintf('ERROR: %s%s%s', $message,$trace, $ref);
        Log::error($msg);
    }

    public function infoLog($message, $ref = '') :void
    {
        $msg = sprintf('INFO: %s%s', $message, $ref);
        Log::info($msg);
    }


}
