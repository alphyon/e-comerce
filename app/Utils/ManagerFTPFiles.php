<?php


namespace App\Utils;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ManagerFTPFiles
{

    public function getFileFromFTP($file_ftp = null, $dir_ftp = null)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        try{
            $archives= collect(Storage::disk('ftp')->files($dir_ftp));
            $finder = $archives->filter(function ($item) use($file_ftp){
                return Str::contains($item,$file_ftp);
            });
            if($finder->count() > 0){
                $file_down =Storage::disk('ftp')->get($finder->first());
                    $directory_down = 'ftp_down';
                    $filename = basename($finder->first());
                 Storage::makeDirectory($directory_down);
                 if(!Storage::put($directory_down."/".$filename,$file_down)){
                 throw new \Exception('NOT CREATE FILE');
                 }
                 return $filename;
            }
        }catch (\Exception $exception){
            Log::error("ERROR FTP: ".$exception->getMessage());
            return null;
        }
    }
}
