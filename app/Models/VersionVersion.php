<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VersionVersion extends Model
{
    use HasFactory;

    protected $fillable =[
        'parent_id',
        'child_id',
        'quantity'
    ];
}
