<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowCase extends Model
{
    use HasFactory;
    protected $table = 'show_cases';
    protected $fillable =[
        'name','address','contact','picture',
    ];
}
