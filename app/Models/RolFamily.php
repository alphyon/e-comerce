<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolFamily extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'family_id',
        'permission',
    ];

    public function user(){
        return $this->hasOne(User::class);
    }

    public function family(){
        return $this->hasOne(Family::class,'id','family_id');
    }

}
