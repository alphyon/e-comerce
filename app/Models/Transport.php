<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    use HasFactory;
    protected $fillable =['name','photo'];

    public function orders(){
        return $this->belongsTo(Order::class);
    }

    public function transportZones(){
        return $this->belongsTo(TransportZone::class);
    }
}
