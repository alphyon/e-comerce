<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransportZone extends Model
{
    use HasFactory;

    protected $fillable = [
        'transport_id',
        'zone_id',
        'min_weight',
        'max_weight',
        'price',
    ];

    public function transport(){
        return $this->hasOne(Transport::class);
    }

    public function zone(){
        return $this->hasOne(Zone::class);
    }
}
