<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'name_complement',
        'address',
        'city',
        'state',
        'country',
        'postcode',
        'contact_name',
        'connector_id',
        'iva',
        'nif',
        'business_name',
        'web',
        'currency_id',
        'bill',
        'rate_id',
        'zone_id',
    ];

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function zone(){
        return $this->hasOne(Zone::class);
    }
    public function currency(){
        return $this->hasOne(Currency::class);
    }

    public function rate(){
        return $this->hasOne(Rate::class,'id','rate_id');
    }

}
