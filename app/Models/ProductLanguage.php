<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLanguage extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'language_id',
        'description',
        'meta_title',
        'meta_keys',
        'meta_description',
    ];

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }

    public function language(){
        return $this->hasOne(Language::class,'id','language_id');
    }
}
