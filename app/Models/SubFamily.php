<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubFamily extends Model
{
    use HasFactory;

    protected $fillable = [
        'family_id',
        'name',
        'image',
    ];

    public function family(){
        return $this->hasOne(Family::class,'id','family_id');
    }

    public function downloads(){
        return $this->belongsTo(Download::class);
    }
}
