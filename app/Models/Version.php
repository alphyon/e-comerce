<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'size_id',
        'color_id',
        'stock',
        'weight',
        'photo',
        'active',
        'reference',
        'type',
        'battery',
        'material',
        'video',
        'sort',
    ];
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function size()
    {
        return $this->hasOne(Size::class, 'id', 'size_id');
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'color_id');
    }

    public function orderItems()
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function cartItems()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function galleries()
    {
        return $this->belongsTo(Gallery::class, 'id', 'version_id');
    }

    public function rateVersion()
    {
        return $this->hasOne(RateVersion::class, 'version_id', 'id');
    }

    public function notifications()
    {
        return $this->belongsTo(Notification::class);
    }
}
