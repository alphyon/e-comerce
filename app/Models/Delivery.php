<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $fillable = [
        'address',
        'city',
        'state',
        'country',
        'postcode',
        'contact_reference',
        'deliverable_id',
        'deliverable_type'
    ];
    public function deliverable(){
        return $this->morphTo();
    }

}
