<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RateVersion extends Model
{
    use HasFactory;

    protected $fillable =[
      'version_id','rate_id','price'
    ];

    public function version(){
        return $this->hasOne(Version::class,'id','version_id');
    }
    public function rate(){
        return $this->hasOne(Rate::class,'id','rate_id');
    }


}
