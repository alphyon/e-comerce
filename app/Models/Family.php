<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use HasFactory;
    protected $fillable =[
      'name','slug','picture','sort'
    ];

    public function subFamilies(){
        return $this->belongsTo(SubFamily::class);
    }

    public function rolFamily(){
        return $this->belongsTo(RolFamily::class);
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
}
