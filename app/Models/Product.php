<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'box',
        'active',
        'category_id',
        'width',
        'large',
        'height',
        'iva',
        'discount',
        'umbral'
    ];

    public function category()
    {
        return $this->hasOne(Category::class);
    }

    public function versions()
    {
        return $this->belongsTo(Version::class);
    }

    public function imageProduct()
    {
        return $this->belongsTo(ImageShowCase::class);
    }

    public function productLanguages()
    {
        return $this->belongsTo(ProductLanguage::class, 'id', 'language_id');
    }

    public function family()
    {
        return $this->belongsTo(Family::class);
    }
}
