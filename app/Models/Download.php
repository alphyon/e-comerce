<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;
    protected $fillable =[
      'subfamily_id','reference','file','name'
    ];

    public function subFamily(){
        return $this->hasOne(SubFamily::class,'id','subfamily_id');
    }
}
