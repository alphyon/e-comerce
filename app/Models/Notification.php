<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'version_id',
        'email',
        'locale'
    ];

    public function version(){
        return $this->hasOne(Version::class);
    }
}
