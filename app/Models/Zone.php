<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    use HasFactory;
    protected $fillable =[
        'name'
    ];

    public function transportZones(){
        return $this->belongsTo(TransportZone::class);
    }

    public function profiles(){
        return $this->belongsTo(Profile::class);
    }
}
