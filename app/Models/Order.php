<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'status_id',
        'transport_id',
        'date',
        'alias',
        'identify_number',
        'total',
        'idpedv',
        'iva',
        'pay_method',
        'transport_price',
        'comment',
        'bill',
        'process',
    ];

    public function user(){
        return $this->hasOne(User::class);
    }
    public function status(){
        return $this->hasOne(Status::class);
    }
    public function transport(){
        return $this->hasOne(Transport::class);
    }

    public function bills(){
        return $this->belongsTo(Bill::class);
    }
    public function orderItems(){
        return $this->belongsTo(OrderItem::class);
    }
    public function deliveries()
    {
        return $this->morphMany(Delivery::class, 'deliverable');
    }

}
