<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;
    protected $fillable =['name','slug'];

    public function productLanguages()
    {
        return $this->belongsTo(ProductLanguage::class,'language_id');

    }
}
