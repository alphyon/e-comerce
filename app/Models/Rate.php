<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','connector_id'
    ];

    public function rateVersion(){
        return $this->belongsTo(RateVersion::class);
    }

    public function profiles(){
        return $this->belongsTo(Profile::class);
    }
}
