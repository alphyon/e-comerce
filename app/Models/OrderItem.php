<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'version_id',
        'order_id',
        'price',
        'quantity',
    ];

    public function version(){
        return $this->hasOne(Version::class);
    }

    public function order(){
        return $this->hasOne(Order::class);
    }
}
