<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyChange extends Model
{
    use HasFactory;

    protected $fillable = [
        'currency_id',
        'currency_name_to_change',
        'change_value',
    ];
}
