<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageShowCase extends Model
{
    use HasFactory;
    protected $table = 'image_show_case';

    protected $fillable =[
      'show_case_id','image'
    ];

    public function showcase(){
        return $this->hasOne(ShowCase::class,'id','show_case_id');
    }


}
