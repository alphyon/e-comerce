<?php

namespace App\Http\Middleware;

use App\Models\Family;
use App\Models\RolFamily;
use Closure;
use Illuminate\Http\Request;

class RoutesFamilies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $paramRoute = $request->route()->parameters();
        if(isset($paramRoute['family'])){
           $family = RolFamily::whereHas('family',function ($query) use($paramRoute){
               $query->where('slug',$paramRoute['family']);
           })->where('user_id',auth()->user()->id)->first();
          if(isset($family->permission) &&!$family->permission){
              return redirect('/');
          }
        }


            return $next($request);


    }
}
