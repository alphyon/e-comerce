<?php

namespace App\Http\Livewire\Admin\Showcases;

use App\Models\Download;
use App\Models\ImageShowCase as ModelsImageShowCase;
use App\Models\SubFamily;
use App\Models\SubFamily as ModelsSubFamily;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Manage extends Component
{
    use WithPagination;
    use WithFileUploads;
    use WithSorting;

    public $deleteModal = false;
    public $deleteModalItem = false;
    public $modal = false;
    public $listOpen = false;
    public $selectedItem;
    public $modalItem = false;
    public $itemIdDownload;
    public $showcase;
    public $picture;
    public $picture2;

    public $downloadItems ;
    public ModelsImageShowCase $imageShowCase;


    public function openItem($itemId = null,$action=null)
    {
        if ($action==='delete') {
            $this->selectedItem = $itemId;
            $this->imageShowCase = ModelsImageShowCase::find($itemId);
            $this->deleteModalItem=true;
        }else {
            $this->itemIdDownload = $itemId;
            $this->imageShowCase = new ModelsImageShowCase;
            $this->modalItem = true;
        }

    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->downloadItems = null;
        $this->modalItem = false;
        $this->deleteModalItem=false;
    }

    /**
     * Eliminar registro
     *
     */
    public function destroyItem()
    {
        $this->imageShowCase->delete();
        $this->cancel();
    }


    /**
     * Guardar/Actualizar registro
     *
     */


    public function saveItem()
    {
        $this->validate();
        $this->imageShowCase->show_case_id= $this->showcase;
        $this->imageShowCase->image = $this->picture2->store('/public/showcase/'.$this->showcase);
        $this->imageShowCase->save();
        $this->cancel();
    }

    protected function rules()
    {

        return [
            'picture2' => 'image',

        ];
    }

    public function mount($showcase)
    {
        $this->showcase = $showcase;

    }

    public function render()
    {
        return view('livewire.admin.showcases.manage', [
            'items' => ModelsImageShowCase::where('show_case_id',$this->showcase)->get()
        ])
            ->layout('layouts.admin');
    }
}
