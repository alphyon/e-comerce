<?php

namespace App\Http\Livewire\Admin;

use App\Models\Color;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Color as ModelsColors;
use App\Traits\WithSorting;

class Colors extends Component
{
    use WithPagination;
    use WithSorting;

    public $deleteModal = false;
    public $modal =false;

    public $selectedItem;

    public ModelsColors $color;

    protected $rules =[
        'color.name'=>'required|string',
        'color.code'=>'required|string',
    ];

    public function open($action =null, $itemId=null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->color = ModelsColors::find($itemId);
        }
        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->color->code = "#".$this->color->code;
            $this->modal = true;
        } else {
            $this->color = new ModelsColors;
            $this->modal = true;
        }
    }

    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->color = new ModelsColors;
    }
    public function destroy()
    {
        $this->color->delete();
        $this->cancel();
    }

    public function save()
    {
        $this->validate();
        $this->color->code=str_replace('#', '', $this->color->code);
        $this->color->save();
        $this->cancel();
    }

    public function render()
    {
        return view('livewire.admin.colors', [
            'colors'=>ModelsColors::where('name', 'LIKE', "%{$this->search}%")
            ->orderBy($this->sortField, $this->sortDirection)
            ->paginate($this->perPage)
        ])->layout('layouts.admin');
    }
}
