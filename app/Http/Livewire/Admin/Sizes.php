<?php

namespace App\Http\Livewire\Admin;

use App\Models\Size as ModelsSizes;
use App\Traits\WithSorting;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class Sizes extends Component
{
    use WithPagination;
    use WithSorting;

    public $deleteModal = false;
    public $modal =false;

    public $selectedItem;

    public ModelsSizes $size;

    protected $rules =[
        'size.size'=>'required|string',
    ];

    public function open($action =null, $itemId=null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->size = ModelsSizes::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->modal = true;
        } else {
            $this->size = new ModelsSizes;
            $this->modal = true;
        }
    }

    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->size = new ModelsSizes;
    }
    public function destroy()
    {
        $this->size->delete();
        $this->cancel();
    }

    public function save()
    {
        $this->validate();
        $this->size->save();
        $this->cancel();
    }
    
    public function render()
    {
        return view('livewire.admin.sizes', [
            'sizes' => ModelsSizes::where('size', 'LIKE', "%{$this->search}%")
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate($this->perPage)
        ])
            ->layout('layouts.admin');
    }
}
