<?php

namespace App\Http\Livewire\Admin;

use App\Models\Product as ModelsProducts;
use App\Models\ProductLanguage;
use App\Models\ProductLanguage as ModelsProductLanguage;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithPagination;

class Products extends Component
{
    use WithPagination;
    use WithSorting;

    public $deleteModal = false;
    public $modal = false;
    public $editAction = false;
    public $flag=false;

    public $selectedItem;

    public ModelsProducts $product;
    public ModelsProductLanguage $productLanguage;

    protected $rules = [
        'product.name' => 'required|string',
        'product.box' => 'required|numeric',
        'product.width' => 'required|numeric',
        'product.large' => 'required|numeric',
        'product.height' => 'required|numeric',
        'product.iva' => 'required|numeric',
        'product.discount' => 'required|numeric',
        'product.umbral' => 'required|numeric',
        'product.family_id' => 'required',
        'product.category_id' => 'nullable',

    ];

    public function open($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->product = ModelsProducts::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->editAction = true;
            $this->product->height = ($this->product->height / 100);
            $this->product->width = ($this->product->width / 100);
            $this->product->large = ($this->product->large / 100);
            $this->modal = true;
            $this->flag=false;
        } else {
            $this->product = new ModelsProducts;
            $this->product->umbral = 2;
            $this->product->discount = 0;
            $this->modal = true;
            $this->flag=true;
        }
    }

    public function destroy()
    {
        $this->product->active =! $this->product->active;
        $this->product->save();
        $this->cancel();
    }

    public function cancel()
    {
        $this->flag=false;
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->product = new ModelsProducts;
    }

    public function save($flag = false)
    {
        $this->validate();
        $this->product->height = ($this->product->height * 100);
        $this->product->width = ($this->product->width * 100);
        $this->product->large = ($this->product->large * 100);
        $this->product->umbral = 2;
        $this->product->save();
        $prod = $this->product->id;

        $language = \App\Models\Language::all();
        if ($this->flag) {
            $language->each(function ($item) use ($prod) {
                $this->productLanguage = new ModelsProductLanguage;
                $this->productLanguage->product_id = $prod;
                $this->productLanguage->language_id = $item->id;
                $this->productLanguage->save();
            });
        }

        $this->cancel();
    }

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortField = $field;
    }

    public function render()
    {
        return view('livewire.admin.products', [
            'products' => ModelsProducts::with('family')->where('name', 'LIKE', "%{$this->search}%")
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate($this->perPage),
            'families' => \App\Models\Family::all()
        ])
            ->layout('layouts.admin');
    }
}
