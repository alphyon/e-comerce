<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Requests extends Component
{
    public function render()
    {
        return view('livewire.admin.requests')
            ->layout('layouts.admin');
    }
}
