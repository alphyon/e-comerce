<?php

namespace App\Http\Livewire\Admin\Downloads;

use App\Models\Download;
use App\Models\Download as ModelsDownload;
use App\Models\SubFamily;
use App\Models\SubFamily as ModelsSubFamily;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Manage extends Component
{
    use WithPagination;
    use WithFileUploads;
    use WithSorting;

    public $deleteModal = false;
    public $deleteModalItem = false;
    public $modal = false;
    public $listOpen = false;
    public $selectedItem;
    public $modalItem = false;
    public $itemIdDownload;
    public $subFamily;
    public $picture;
    public $picture2;

    public $downloadItems ;
    public ModelsDownload $itemDownload;


    public function openItem($itemId = null,$action=null)
    {
        if ($action==='delete') {
            $this->selectedItem = $itemId;
            $this->itemDownload = ModelsDownload::find($itemId);
            $this->deleteModalItem=true;
        }else {
            $this->itemIdDownload = $itemId;
            $this->itemDownload = new ModelsDownload;
            $this->modalItem = true;
        }

    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->downloadItems = null;
        $this->modalItem = false;
        $this->deleteModalItem=false;
    }

    /**
     * Eliminar registro
     *
     */
    public function destroyItem()
    {
        $this->itemDownload->delete();
        $this->cancel();
    }


    /**
     * Guardar/Actualizar registro
     *
     */


    public function saveItem()
    {
        $this->validate();
        $this->itemDownload->subfamily_id= $this->subFamily;
        $this->itemDownload->file = $this->picture2->store('/public/subfamily/'.$this->subFamily);
        $this->itemDownload->save();
        $this->cancel();
    }

    protected function rules()
    {

            return [
                'picture2' => 'image',
                'itemDownload.name'=> 'required|string',
                'itemDownload.reference'=> 'required|string',
            ];
    }

    public function mount($subFamily)
    {
        $this->subFamily = $subFamily;

    }

    public function render()
    {
        return view('livewire.admin.downloads.manage', [
            'items' => Download::where('subfamily_id',$this->subFamily)->get()
        ])
            ->layout('layouts.admin');
    }
}
