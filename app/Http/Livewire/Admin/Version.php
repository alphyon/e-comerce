<?php

namespace App\Http\Livewire\Admin;

use App\Models\Gallery;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\RateVersion as ModelsRateVersions;
use App\Models\Gallery as ModelsGallery;
use App\Models\Version as DVersion;

class Version extends Component
{
    use WithPagination;
    use WithSorting;
    use WithFileUploads;

    public $deleteModal = false;
    public $modalRateVersion = false;
    public $modalGallery = false;

    public $selectedItem;
    public $picture;
    public $version;

    public ModelsRateVersions $rateVersion;
    public ModelsGallery $gallery;


    public function mount(DVersion $version)
    {
        $this->version = $version;

    }

    public function openRateVersion($itemId = null)
    {

            $this->selectedItem = $itemId;
            $this->rateVersion = ModelsRateVersions::find($itemId);
            $this->picture = $this->rateVersion->url;

        $this->rateVersion->price = ($this->rateVersion->price / 100);
        $this->modalRateVersion = true;
    }

    public function openGallery($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->gallery = ModelsGallery::find($itemId);
        }
        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->modalGallery = true;
        } else {
            $this->gallery = new ModelsGallery;
            $this->modalGallery = true;
        }
    }

    public function destroy()
    {
        $this->gallery->delete();
        $this->cancel();
    }

    public function cancel()
    {
        $this->modalRateVersion = false;
        $this->modalGallery = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->rateVersion = new ModelsRateVersions;
    }

    public function saveRateVersion()
    {
        $this->validate();
        $this->rateVersion->price = $this->rateVersion->price * 100;
        $this->rateVersion->save();
        $this->cancel();
    }

    public function saveGallery()
    {
        $this->validate();
        $this->gallery->version_id = $this->version->id;
        $this->gallery->url = $this->picture->store('/public/version/' . $this->version->id . '/');
        $this->gallery->save();
        $this->cancel();
    }

    public function render()
    {
        $this->perPage = 5;
        return view('livewire.admin.version', [
            'rateVersions' => ModelsRateVersions::with('rate')->where('version_id',$this->version->id)
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate($this->perPage),
            'galleries' => Gallery::where('version_id', $this->version->id)->get()
        ])->layout('layouts.admin');
    }

    protected function rules()
    {
        if ($this->modalGallery === true)
            return [
                'picture' => 'image'
            ];

        if ($this->modalRateVersion === true)
            return [
                'rateVersion.price' => 'required|numeric',
            ];
    }
}
