<?php

namespace App\Http\Livewire\Admin;

use App\Models\Family;
use App\Models\Product;
use App\Models\Version;
use App\Traits\WithSorting;
use Livewire\Component;
use App\Models\Version as ModelsVersion;
use Livewire\WithPagination;

class Orders extends Component
{
    use WithPagination;
    use WithSorting;

    public $family;
    public $products;

    public $statusChange = false;
    public ModelsVersion $version;
    public $keyActive=0;
    protected $rules =[
        'version.sort'=>'numeric',

    ];
    public function changeStatus($id,$key){
        $this->version = ModelsVersion::find($id);
        $this->keyActive = $key;
        $this->statusChange = !$this->statusChange;
    }
    public function initialState(){
        $this->statusChange =false;
    }

    public function update(){
      $this->version->save();
      $this->keyActive=0;
      $this->statusChange =false;
    }

    public function render()
    {
        $versions=[];
        if(!empty($this->family)) {
            $this->products = Product::where('family_id', $this->family)->pluck('id');
            $versions = ModelsVersion::whereIn('product_id',$this->products->toArray())
                ->with(['product','color','size'])
                ->orderBy('sort', 'desc')
                ->paginate($this->perPage);

        }
        return view('livewire.admin.orders',[
            'families'=>\App\Models\Family::all(),
            'versions'=> $versions
            ])->layout('layouts.admin');


    }
}
