<?php

namespace App\Http\Livewire\Admin;

use App\Models\ShowCase;
use App\Models\SubFamily;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\ShowCase as ModelsShowCase;
use App\Models\ImageShowCase as ModelsImageShowCase;

class Showcases extends Component
{
    use WithPagination;
    use WithFileUploads;
    use WithSorting;

    public $deleteModal = false;
    public $deleteModalItem = false;
    public $modal = false;
    public $listOpen = false;
    public $selectedItem;
    public $modalItem = false;
    public $itemIdImage;

    public $picture;
    public $picture2;

    public $downloadItems ;
    public ModelsShowCase $showCase ;
    public ModelsImageShowCase $imageShowCase;



    public function open($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->showCase = ModelsShowCase::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {

            $this->modal = true;
        }elseif($action ==='list'){
            $this->listOpen = true;
            $this->downloadItems = ModelsImageShowCase::where('show_case_id',$itemId)->get();
        } else {
            $this->showCase = new ModelsShowCase;
            $this->modal = true;
        }
    }

    public function openItem($itemId,$action=null)
    {
        if ($action==='delete') {
            $this->selectedItem = $itemId;
            $this->imageShowCase = ModelsImageShowCase::find($itemId);
            $this->deleteModalItem=true;
        }else {
            $this->itemIdImage = $itemId;
            $this->imageShowCase = new ModelsImageShowCase;
            $this->modalItem = true;
        }

    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->downloadItems = null;
        $this->modalItem = false;
        $this->showCase = new ModelsShowCase();
    }

    /**
     * Eliminar registro
     *
     */
    public function destroy()
    {
        $data = ModelsImageShowCase::where('show_case_id',$this->itemIdImage);
        $data->each(function ($item){
            $item->delete();
        });
        $this->showCase->delete();
        $this->cancel();
    }


    /**
     * Guardar/Actualizar registro
     *
     */
    public function save()
    {

        if(!is_string($this->picture) ) {

            $this->showCase->picture = $this->picture->store('/public/showcase');
        }
        $this->validate();
        $this->showCase->save();

        $this->cancel();
    }

    public function saveItem()
    {
        $this->validate();
        $this->imageShowCase->show_case_id= $this->itemIdImage;
        $this->imageShowCase->image = $this->picture2->store('/public/showcase/'.$this->itemIdImage);
        $this->imageShowCase->save();
        $this->cancel();
    }

    protected function rules()
    {
        if ($this->modal === true)
            return [
                'showCase.name' => 'required|string',
                'showCase.address' => 'nullable|string',
                'showCase.contact' => 'nullable|string',
                'picture' => 'image',
            ];

        if ($this->modalItem === true)
            return [
                'picture2' => 'image',
            ];
    }

    public function render()
    {
        return view('livewire.admin.showcases',
            [
                'showcases' => ModelsShowCase::where('name', 'LIKE', "%{$this->search}%")
                    ->orderBy($this->sortField, $this->sortDirection)
                    ->paginate($this->perPage),
            ])
            ->layout('layouts.admin');
    }
}
