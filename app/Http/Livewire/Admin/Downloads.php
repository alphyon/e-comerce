<?php

namespace App\Http\Livewire\Admin;

use App\Models\Download;
use App\Models\SubFamily;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\Download as ModelsDownload;
use App\Models\SubFamily as ModelsSubFamily;

class Downloads extends Component
{
    use WithPagination;
    use WithFileUploads;
    use WithSorting;

    public $deleteModal = false;
    public $deleteModalItem = false;
    public $modal = false;
    public $listOpen = false;
    public $selectedItem;
   public $modalItem = false;
   public $itemIdDownload;

    public $picture;
    public $picture2;

    public $downloadItems ;
    public ModelsSubFamily $subFamily ;
    public ModelsDownload $itemDownload;



    public function open($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->subFamily = ModelsSubFamily::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->picture = $this->download->picture;
            $this->modal = true;
        }elseif($action ==='list'){
            $this->listOpen = true;
            $this->downloadItems = ModelsDownload::where('subfamily_id',$itemId)->get();
        } else {
            $this->subFamily = new ModelsSubFamily;
            $this->modal = true;
        }
    }

    public function openItem($itemId,$action=null)
    {
        if ($action==='delete') {
            $this->selectedItem = $itemId;
            $this->itemDownload = ModelsDownload::find($itemId);
            $this->deleteModalItem=true;
        }else {
            $this->itemIdDownload = $itemId;
            $this->itemDownload = new ModelsDownload;
            $this->modalItem = true;
        }

    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->downloadItems = null;
        $this->modalItem = false;
        $this->subFamily = new ModelsSubFamily();
    }

    /**
     * Eliminar registro
     *
     */
    public function destroy()
    {
        $data = Download::where('subfamily_id',$this->itemIdDownload);
        $data->each(function ($item){
            $item->delete();
        });
        $this->subFamily->delete();
        $this->cancel();
    }


    /**
     * Guardar/Actualizar registro
     *
     */
    public function save()
    {
        $this->validate();


        $this->subFamily->image = $this->picture->store('/public/subfamily');
        $this->subFamily->save();

        $this->cancel();
    }

    public function saveItem()
    {
       $this->validate();
        $this->itemDownload->subfamily_id= $this->itemIdDownload;
        $this->itemDownload->file = $this->picture2->store('/public/subfamily/'.$this->itemIdDownload);
        $this->itemDownload->save();
        $this->cancel();
    }

    protected function rules()
    {
        if ($this->modal === true)
            return [
                'subFamily.name' => 'required|string',
                'subFamily.family_id' => 'required|numeric',
                'picture' => 'image',
            ];

        if ($this->modalItem === true)
            return [
                'picture2' => 'image',
                'itemDownload.name'=> 'required|string',
                'itemDownload.reference'=> 'required|string',
            ];
    }

    public function render()
    {
        return view('livewire.admin.downloads',
            [
                'downloads' => SubFamily::with(['family'])->where('name', 'LIKE', "%{$this->search}%")
                    ->orderBy($this->sortField, $this->sortDirection)
                    ->paginate($this->perPage),
                'families'=>\App\Models\Family::all(),

            ])
            ->layout('layouts.admin');
    }
}
