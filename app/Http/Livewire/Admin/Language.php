<?php

namespace App\Http\Livewire\Admin;

use App\Models\Language as ModelsLanguages;
use App\Traits\WithSorting;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Language extends Component
{
    use WithPagination;
    use WithSorting;
    use WithFileUploads;

    public $deleteModal = false;
    public $modal =false;

    public $selectedItem;
    public $picture;

    public ModelsLanguages $language;

    protected $rules =[
        'language.name'=>'required|string',
        'language.locale'=>'required|string',
    ];

    public function open($action =null, $itemId=null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->language = ModelsLanguages::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->modal = true;
        } else {
            $this->language = new ModelsLanguages;
            $this->modal = true;
        }
    }

    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->language = new ModelsLanguages;
    }
    public function destroy()
    {
        $this->language->delete();
        $this->cancel();
    }

    public function save()
    {
        $this->validate();
        $this->language->locale = Str::slug($this->language->locale);
        $this->language->save();
        $this->cancel();
    }

    public function render()
    {
        return view(
            'livewire.admin.language',
            [
                'languages' => ModelsLanguages::where('name', 'LIKE', "%{$this->search}%")
                    ->orderBy($this->sortField, $this->sortDirection)
                    ->paginate($this->perPage)
            ]
        )->layout('layouts.admin');
    }
}
