<?php

namespace App\Http\Livewire\Admin;

use App\Models\Rate;
use App\Models\RolFamily;
use App\Models\User;
use App\Models\Profile as ModelsProfile;
use App\Models\RolFamily as ModelsRolFamily;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Users extends Component
{
    use WithPagination;
    use WithSorting;

    public $deleteModal = false;
    public $modalChangeRate = false;
    public $modalFamilyManage = false;
    public $modal = false;
    public $selectedItem;

    public $picture;
    public $password;
    public $password_confirmation;
    public $email;
    public ModelsProfile $profile;
    public  $rolFamilies=[];



    public function open($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->profile = ModelsProfile::with(['user'])->find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->email = $this->profile->user->email;
            $this->modal = true;
        } else {
            $this->profile = new ModelsProfile;
            $this->modal = true;
        }
    }

    public function openRate($itemId = null)
    {
        $this->selectedItem = $itemId;
        $this->profile = ModelsProfile::find($itemId);
        $this->modalChangeRate = true;
    }
    public function openManageFamily($itemId = null)
    {
        $families = \App\Models\Family::all();
        $families->each(function ($item) use ($itemId){
            $rolFamily = RolFamily::firstOrCreate(
                ['user_id' =>  $itemId,'family_id'=>$item->id],
                ['permission' => true]
            );


        });
        $this->selectedItem = $itemId;
        $this->rolFamilies = ModelsRolFamily::with('family')->where('user_id',$itemId)->get();

        $this->modalFamilyManage = true;
    }

    public function changeStatus($field = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->profile = ModelsProfile::find($itemId);
        }

        if ($field === 'IVA') {
            $this->profile->iva = !$this->profile->iva;
        } else {
            $this->profile->bill = !$this->profile->bill;
        }
        $this->profile->save();
    }

    public function changePermission($id){
        $data = ModelsRolFamily::find($id);
        $data->permission = !$data->permission;
        $data->save();
    }

    /**
     * Eliminar registro
     *
     */
    public function changeUserStatus()
    {
        $this->profile->user->status = !$this->profile->user->status;
        $this->profile->user->save();


        $this->cancel();
    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->password = null;
        $this->password_confirmation = null;
        $this->modalChangeRate=false;
        $this->profile = new ModelsProfile;
        $this->modalFamilyManage=false;
    }

    /**
     * Guardar/Actualizar registro
     *
     */
    public function save()
    {
        $this->validate();
        if ($this->password != null && $this->password_confirmation != null) {
            $encode = bcrypt($this->password);
            $this->profile->user->password = $encode;
            $this->profile->user->save();

        }
        $this->profile->save();
        $this->cancel();
    }

    public function saveRate()
    {
        $this->validate();
        $this->profile->save();
        $this->cancel();
    }
    protected function rules()
    {
        if ($this->modal === true)
            return [
                'profile.name' => 'string',
                'profile.name_complement' => 'string',
                'email' => 'email',
                'password' => 'nullable|confirmed',
                'password_confirmation' => 'nullable'
            ];

        if ($this->modalChangeRate === true)
            return [
                'profile.rate_id'=>'numeric|required',
            ];
    }

    public function render()
    {
        return view('livewire.admin.users',
            [
                'profiles' => ModelsProfile::with(['user', 'rate'])->where('name', 'LIKE', "%{$this->search}%")
                    ->orderBy($this->sortField, $this->sortDirection)
                    ->paginate($this->perPage),
                'rates' => Rate::all(),
                'families'=>$this->rolFamilies,

            ])->layout('layouts.admin');
    }
}
