<?php

namespace App\Http\Livewire\Admin;

use App\Models\Config as ModelsConfig;
use App\Traits\WithSorting;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Configuration extends Component
{
    use WithPagination;
    use WithFileUploads;
    use WithSorting;

    public $deleteModal = false;
    public $modal = false;
    public $imgValue = false;
    public $selectedItem;

    public $picture;

    public ModelsConfig $config ;


    protected $rules = [
        'config.name' => 'required|string',
        'config.value'=>'required|string',
        'config.complement'=>'nullable',
        'picture' => 'nullable|image'
    ];

    public function checkData()
    {
        $this->imgValue = !$this->imgValue;
        if($this->imgValue){

            $this->config->value ="IMAGE";
        }

    }
    public function open($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->config = ModelsConfig::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->modal = true;
        } else {
            $this->config = new ModelsConfig;
            $this->modal = true;
        }
    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->config = new ModelsConfig;
    }

    /**
     * Eliminar registro
     *
     */
    public function destroy()
    {
        $this->config->delete();
        // $this->repository->delete($this->selectedItem);

        $this->cancel();
    }

    /**
     * Guardar/Actualizar registro
     *
     */
    public function save()
    {
        $this->validate();

      if($this->picture != null){

          $this->config->value = $this->picture->store('/public/config');
      }
      $this->config->name = Str::upper($this->config->name);
        $this->config->save();

        $this->cancel();
    }

    public function render()
    {
        return view('livewire.admin.configuration', [
            'configurations' => ModelsConfig::where('name', 'LIKE', "%{$this->search}%")
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate($this->perPage)
        ])->layout('layouts.admin');
    }
}
