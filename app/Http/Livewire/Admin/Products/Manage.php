<?php

namespace App\Http\Livewire\Admin\Products;

use App\Models\Color;
use App\Models\Language;
use App\Models\Product;
use App\Models\ProductLanguage;
use App\Models\ProductLanguage as ModelsProductLanguage;
use App\Models\Size;
use App\Models\Version as ModelsVersion;
use App\Models\RateVersion as ModelsRateVersion;
use App\Models\Version;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Manage extends Component
{
    use WithPagination;
    use WithSorting;
    use WithFileUploads;

    public $deleteModal = false;
    public $modalLanguage =false;
    public $modalVersion =false;

    public $selectedItem;
    public ModelsVersion $version;
    public ModelsProductLanguage $pLanguage;
    public $product;
    public $picture;

    public function mount(Product $product)
    {
        $this->product = $product;
    }


    protected $rules =[
        'pLanguage.description'=>'string',
        'pLanguage.meta_title'=>'string',
        'pLanguage.meta_keys'=>'string',
        'pLanguage.meta_description'=>'string',
        'version.stock'=>'required|numeric',
        'version.weight'=>'required|numeric',
        'version.battery'=>'string',
        'version.material'=>'string',
        'version.reference'=>'required|string',
        'version.type'=>'required',
        'version.video'=>'string',
        'version.size_id'=>'required|numeric',
        'version.color_id'=>'required|numeric',
        'version.product_id'=>'numeric',
        'picture'=>'required',

    ];

    public function openVersion($action =null,$itemId=null)
    {

        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->version = Version::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->picture= $this->version->photo;
            $this->modalVersion = true;
        } else {
            $this->version= new ModelsVersion;
            $this->version->product_id=$this->product->id;
            $this->modalVersion = true;
        }

    }

    public function openProductLanguage($itemId=null)
    {

        $this->selectedItem = $itemId;
        $this->pLanguage = ProductLanguage::find($itemId);
        $this->modalLanguage = true;

    }


    public function cancel()
    {
        $this->modalLanguage = false;
        $this->deleteModal = false;
        $this->modalVersion =false;
        $this->selectedItem = null;
        $this->pLanguage = new ModelsProductLanguage;
    }
    public function destroy()
    {
        $this->version->active = !$this->version->active;
        $this->version->save();
        $this->cancel();
    }

    public function saveLanguage()
    {
        $this->validate();
        $this->pLanguage->save();
        $this->cancel();
    }

    public function saveVersion()
    {
        if(!is_string($this->picture) ) {
            $this->rules['picture'] = "image";
            $this->version->photo = $this->picture->store('/public/version');
        }
        $this->validate();
        $this->version->save();
        $version = $this->version->id;

        $rates = \App\Models\Rate::all();

        $rates->each(function ($item) use($version){
            $this->rateVersion = new ModelsRateVersion;
            $this->rateVersion->version_id = $version;
            $this->rateVersion->rate_id = $item->id;
            $this->rateVersion->save();
        });

        $this->cancel();
    }

    public function render()
    {
        return view('livewire.admin.products.manage', [
            'versions' => Version::with(['color','size'])->where('product_id', $this->product->id)
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate($this->perPage),
            'languages' => ProductLanguage::with('language')
                ->where('product_id',$this->product->id)
                ->get(),
            'sizes'=>Size::all(),
            'colors'=>Color::all()
        ])->layout('layouts.admin');

    }
}
