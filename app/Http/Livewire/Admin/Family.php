<?php

namespace App\Http\Livewire\Admin;

use App\Models\Family as ModelsFamily;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithPagination;

use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Family extends Component
{
    use WithPagination;
    use WithFileUploads;
    use WithSorting;

    public $deleteModal = false;
    public $modal = false;
    public $selectedItem;

    public $picture;

    public ModelsFamily $family ;


    protected $rules = [
        'family.name' => 'required|string',
        'picture' => 'required'
    ];


    public function open($action = null, $itemId = null)
    {
        if ($itemId) {
            $this->selectedItem = $itemId;
            $this->family = ModelsFamily::find($itemId);
        }

        if ($action === 'delete') {
            $this->deleteModal = true;
        } elseif ($action === 'edit') {
            $this->picture = $this->family->picture;
            $this->modal = true;
        } else {
            $this->family = new ModelsFamily;
            $this->modal = true;
        }
    }

    /**
     * Cerrar modal
     *
     */
    public function cancel()
    {
        $this->modal = false;
        $this->deleteModal = false;
        $this->selectedItem = null;
        $this->family = new ModelsFamily;
    }

    /**
     * Eliminar registro
     *
     */
    public function destroy()
    {
        $this->family->delete();
        // $this->repository->delete($this->selectedItem);

        $this->cancel();
    }

    /**
     * Guardar/Actualizar registro
     *
     */
    public function save()
    {

        $this->family->slug = Str::slug($this->family->name);
        if(!is_string($this->picture) ) {
            $this->rules['picture']="image";
            $this->family->picture = $this->picture->store('/public/family');
        }
        $this->validate();
        $this->family->save();

        $this->cancel();
    }

    public function render()
    {
        return view('livewire.admin.family', [
            'families' => ModelsFamily::where('name', 'LIKE', "%{$this->search}%")
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate($this->perPage)
        ])->layout('layouts.admin');
    }
}
