<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Logout extends Component
{
    public function logout()
    {
        Auth::logout();
        return $this->redirect(App::getLocale()."/");
    }

    public function render()
    {
        return view('livewire.auth.logout');
    }
}
