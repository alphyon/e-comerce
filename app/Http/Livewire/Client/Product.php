<?php

namespace App\Http\Livewire\Client;

use App\Models\CartItem;
use App\Models\Gallery;
use App\Models\Language;
use App\Models\ProductLanguage;
use App\Models\Version;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Product extends Component
{
    public $version;

    private $productLang;

    // public $items;

    public $gallery;

    public $prodQuantity;

    public $email;

    public function addToCart()
    {
        if ($this->prodQuantity>0) {
            CartItem::updateOrCreate(
                ['cart_id' => auth()->user()->cart->id, 'version_id'=>$this->version->id],
                [
                    'quantity' => DB::raw("quantity + {$this->prodQuantity}"),
                    'price' => $this->version->rateVersion->price
                ]
            );
        }
    }

    public function avisame()
    {
        //guardar email $this->email;
    }

    public function mount($id)
    {
        $languageId = Language::where('locale', app()->getLocale())->first();

        $this->version = Version::with(['product'])->find($id)->first();
        $this->productLang = ProductLanguage::where('language_id', $languageId->id)->where('product_id', $this->version->product->id);

        $this->version->language = $this->productLang->first()->toArray();
        $this->gallery = \App\Models\Gallery::where('version_id', $id)->get();
    }

    public function render()
    {
        return view('livewire.client.product', [
            'items' => CartItem::with(['version'])->where('cart_id', auth()->user()->cart->id)->orderBy('id', 'desc')->get()
        ])->layout('layouts.app');
    }
}
