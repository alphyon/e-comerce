<?php

namespace App\Http\Livewire\Client\Showcases;

use App\Models\ImageShowCase;
use Livewire\Component;

class Items extends Component
{
    public $showcase;

    public function mount($id)
    {

        $this->showcase = $id;

    }

    public function render()
    {
        return view('livewire.client.showcases.items', [
            'images' => ImageShowCase::where('show_case_id', $this->showcase)->get()
        ])->layout('layouts.app');
    }
}
