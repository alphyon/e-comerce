<?php

namespace App\Http\Livewire\Client\Dashboard;

use App\Models\Delivery;
use App\Models\User;
use Livewire\Component;

class AddressChange extends Component
{
    public $address;
    public $city;
    public $state;
    public $country;
    public $postcode;
    public $contact_reference;
    public $setData = false;

    protected $rules = [
        'address' => 'nullable|string',
        'city' => 'nullable|string',
        'state' => 'nullable|string',
        'country' => 'nullable|string',
        'postcode' => 'nullable|numeric',
        'contact_reference' => 'nullable'
    ];

    public function editDelivery()
    {
        $this->setData=false;
        try{
        Delivery::updateOrCreate(
            ['deliverable_type' => User::class, 'deliverable_id' => auth()->user()->id],
            [
                'address' => $this->address,
                'city' => $this->city,
                'state' => $this->state,
                'country' => $this->country,
                'postcode' => $this->postcode,
                'contact_reference' => $this->contact_reference,
            ]
        );
            request()->session()->flash(
                'success',
                'se actualizo la direccion de entrega'
            );
        }catch (\Exception $exception){
            request()->session()->flash(
                'error',
                'Erro al actualizar los datos'
            );
        }
        return redirect()->route('client.dashboard',app()->getLocale());
    }

    public function checkData()
    {
        $this->setData = !$this->setData;
        $this->setData ? $this->useSameRegister() : $this->clean();
    }

    public function useSameRegister()
    {
        $user = User::with(['profile'])->find(auth()->user()->id);
        $this->address = $user->profile->address;
        $this->city = $user->profile->city;
        $this->state = $user->profile->state;
        $this->country = $user->profile->country;
        $this->postcode = $user->profile->postcode;
        $this->contact_reference = $user->profile->contact_reference;

    }

    public function clean()
    {
        $this->address = null;
        $this->city = null;
        $this->state = null;
        $this->country = null;
        $this->postcode = null;
        $this->contact_reference = null;
    }

    public function close()
    {
        return redirect()->route('client.dashboard', app()->getLocale());
    }

    public function mount(){
        $delivery = Delivery::where('deliverable_type',User::class)->where('deliverable_id',auth()->user()->id)->first();
        $this->address = $delivery->address??null;
        $this->city = $delivery->city??null;
        $this->state = $delivery->state??null;
        $this->country = $delivery->country??null;
        $this->postcode = $delivery->postcode??null;
        $this->contact_reference = $delivery->contact_reference??null;
    }

    public function render()
    {
        return view('livewire.client.dashboard.address-change')->layout('layouts.app');
    }
}
