<?php

namespace App\Http\Livewire\Client\Dashboard;

use App\Http\Livewire\Auth\Logout;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ChangePassword extends Component
{
    public User $user;
    public $password;
    public $password_confirmation;
    public $showModal = false;

    protected  $rules =[
        'password'=>'required|confirmed',
        'password_confirmation'=>'required'
    ];

    public function changePassword(){
        $this->validate();

        try{

        $this->user = User::find(\auth()->user()->id);
        $this->user->password = bcrypt($this->password);
        $this->user->save();
            $this->showModal = true;
        }catch (\Exception $exception){
            request()->session()->flash(
                'error',
                __("Ocurrio un error al procesar la petición")
            );

        }
    }
    public function close(){
        return redirect()->route('client.dashboard',app()->getLocale());

    }

    public function cancel(){
        $this->showModal=false;
        Auth::logout();
        return redirect(route('login'));

}

    public function render()
    {
        return view('livewire.client.dashboard.change-password')->layout('layouts.app');
    }
}
