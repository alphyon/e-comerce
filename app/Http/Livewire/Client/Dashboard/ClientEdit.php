<?php

namespace App\Http\Livewire\Client\Dashboard;

use App\Models\Contact;
use Livewire\Component;
use App\Models\User;

class ClientEdit extends Component
{


    public User $user;
    public $email;
    public $name;
    public $name_complement;
    public $address;
    public $city;
    public $state;
    public $country;
    public $postcode;
    public $nif;
    public $business_name;
    public $currency_id;
    public $phone1;
    public $phone2;
    public $fax;
    public $web;
    protected $rules = [
        'email' => 'required|email',
        'name' => 'required|string',
        'name_complement' => 'required|string',
        'address' => 'nullable|string',
        'city' => 'nullable|string',
        'state' => 'nullable|string',
        'country' => 'nullable|string',
        'postcode' => 'nullable|numeric',
        'nif' => 'required|numeric',
        'business_name' => 'nullable|string',
        'currency_id' => 'nullable|numeric',
        'phone1' => 'nullable',
        'phone2' => 'nullable',
        'fax' => 'nullable',
        'web' => 'nullable|url',

    ];

    public function editUser()
    {
        $this->validate();

        $this->user->profile->name = $this->name;
        $this->user->profile->city = $this->city;
        $this->user->profile->state = $this->state;
        $this->user->profile->country = $this->country;
        $this->user->profile->postcode = $this->postcode;
        $this->user->profile->address = $this->address;
        $this->user->profile->business_name = $this->business_name;
        $this->user->profile->web = $this->web;
        $this->user->profile->currency_id = $this->currency_id;
        $this->user->profile->name_complement = $this->name_complement;
        $this->user->email = $this->email;
        $this->user->profile->nif = $this->nif;
        try {
            if (isset($this->phone1)) {
                Contact::updateOrCreate(
                    ['user_id' => $this->user->id, 'reference' => 'phone1'],
                    ['value' => $this->phone1]
                );
            }
            if (isset($this->phone2)) {
                Contact::updateOrCreate(
                    ['user_id' => $this->user->id, 'reference' => 'phone2'],
                    ['value' => $this->phone2]
                );
            }
            if (isset($this->fax)) {
                Contact::updateOrCreate(
                    ['user_id' => $this->user->id, 'reference' => 'fax'],
                    ['value' => $this->fax]
                );
            }
            $this->user->save();
            $this->user->profile->save();
            request()->session()->flash(
                'success',
                'Datos actualizados'
            );
            return redirect()->route('client.dashboard',app()->getLocale());

        }catch (\Exception $exception){
            request()->session()->flash(
                'error',
                'Erro al actualizar los datos'
            );
            return redirect()->route('client.dashboard',app()->getLocale());
        }

    }

    public function close(){
        return redirect()->route('client.dashboard',app()->getLocale());

}

    public function mount()
    {

        $this->user = User::with('profile', 'contacts')->find(auth()->user()->id);
        $this->name = $this->user->profile->name;
        $this->city = $this->user->profile->city;
        $this->state = $this->user->profile->state;
        $this->country = $this->user->profile->country;
        $this->postcode = $this->user->profile->postcode;
        $this->phone1 = $this->user->contacts->where('reference', 'phone1')->first()->value ?? "";
        $this->phone2 = $this->user->contacts->where('reference', 'phone2')->first()->value ?? "";
        $this->fax = $this->user->contacts->where('reference', 'fax')->first()->value ?? "";
        $this->address = $this->user->profile->address;
        $this->business_name = $this->user->profile->business_name;
        $this->web = $this->user->profile->web;
        $this->currency_id = $this->user->profile->currency_id;
        $this->name_complement = $this->user->profile->name_complement;
        $this->email = $this->user->email;
        $this->nif = $this->user->profile->nif;
    }

    public function render()
    {


        return view('livewire.client.dashboard.client-edit', [

        ])->layout('layouts.app');
    }
}
