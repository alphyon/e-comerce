<?php

namespace App\Http\Livewire\Client;

use App\Models\Download;
use App\Models\Family as ModelsFamily;
use App\Models\SubFamily;
use App\Utils\ZipGenerate;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;


class Downloads extends Component
{
    public $family;


    public function mount(ModelsFamily $family)
    {
        $this->family = $family;

    }

    public function download($id)
    {
        try {
            $zipGen = new ZipGenerate();
            $subfamily = SubFamily::find($id);
            $path = 'subfamily1/' . $id;
            $zip = $zipGen->download($path, $subfamily->name.".zip");
            return response()->download(public_path($zip));

        }catch (Exception $exception){
            request()->session()->flash(
                'error',
                'No se puede realizar la descarga'
            );
        }
    }

    public function render()
    {
        $families = SubFamily::where('family_id', $this->family->id)->get();

        return view('livewire.client.downloads',
            [
                'packages' => $families
            ]
        )->layout('layouts.app');
    }
}
