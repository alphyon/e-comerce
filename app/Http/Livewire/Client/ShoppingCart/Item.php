<?php

namespace App\Http\Livewire\Client\ShoppingCart;

use App\Models\CartItem;
use Livewire\Component;

class Item extends Component
{
    public $item;
    public $quantity;

    protected $listeners = ['refreshItem'];

    public function refreshItem($id)
    {
        if ($this->item->version_id === $id) {
            $this->item->refresh();
            $this->quantity = $this->item->quantity;
        }
    }

    public function mount(CartItem $item)
    {
        $this->item = $item;
        $this->quantity = $this->item->quantity;
    }
    
    protected $rules = [
        'item.quantity' => 'digit|required'
    ];
    
    public function quantityChange($value)
    {
        $this->item->quantity = $value;
        $this->item->save();
        $this->quantity = $this->item->quantity;
    }

    public function delete()
    {
        $this->item->delete();
        $this->emitUp('refreshParentComponent');
    }
    
    public function render()
    {
        return view('livewire.client.shopping-cart.item', ['item'=>$this->item]);
    }
}
