<?php

namespace App\Http\Livewire\Client;

use App\Models\CartItem;
use App\Models\Category;
use App\Models\Color;
use App\Models\Family as ModelsFamily;
use App\Models\Product;
use App\Models\RateVersion;
use App\Models\Version;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Family extends Component
{
    use WithPagination;

    public $family;

    public $search;
    public $category;
    public $order;

    public $items;

    protected $listeners = ['refreshParentComponent' => '$refresh'];

    public function mount(ModelsFamily $family)
    {
        $this->family = $family;
        $this->items = CartItem::with(['version'])->where('cart_id', auth()->user()->cart->id)->orderBy('id', 'desc')->get();
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function addBoxToCart($version_id)
    {
        $version = Version::find($version_id);
        $mb = Version::where('reference', "MB{$version->reference}")->first();
        if ($mb) {
            $totalp = $mb->rateVersion->price * $mb->product->box;
            $iva = 3 / 100;
            $total = $totalp * $iva;
            $price = $totalp - $total;
            
            CartItem::updateOrCreate(
                [
                    'cart_id' => auth()->user()->cart->id,
                    'version_id'=> $mb->id,
                ],
                [
                    'quantity' => DB::raw('quantity + 1'),
                    'price' => $price
                ]
            );
            $this->emit('refreshItem', $mb->id);
            $this->items = null;
            $this->items = CartItem::with(['version'])->where('cart_id', auth()->user()->cart->id)->orderBy('id', 'desc')->get();
        }
    }

    public function addToCart($version_id)
    {
        $version = Version::find($version_id);

        CartItem::updateOrCreate(
            [
                'cart_id' => auth()->user()->cart->id,
                'version_id'=>$version->id,
            ],
            [
                'price' => $version->rateVersion->price,
                'quantity' => DB::raw('quantity + 1'),
            ]
        );
        
        $this->emit('refreshItem', $version->id);
        $this->items = null;
        $this->items = CartItem::with(['version'])->where('cart_id', auth()->user()->cart->id)->orderBy('id', 'desc')->get();
    }

    public function render()
    {
        $var = RateVersion::where('rate_id', session('profile')['rate_id'])->where('price', '>', 0)->with('version')->get();
        $products = Product::where('family_id', $this->family->id)->get();


        $dat = collect();

        $products->each(function ($product) use ($var, $dat) {
            $var->each(function ($item) use ($product, $dat) {
                if ($item->version->product_id == $product->id) {
                    $item->color = Color::find($item->version->color_id)->code;
                    $item->product = $product->toArray();
                    $dat->push($item);
                }
            });
        });
        return view('livewire.client.family', [
            'versions' => $this->paginate($dat),
            'categories' => Category::all(),
            'items' => $this->items,
        ])->layout('layouts.app');
    }
}
