<?php

namespace App\Http\Livewire\Client;

use App\Models\Family;
use App\Models\Product;
use App\Models\Version;
use Livewire\Component;

class Adrenalistic extends Component
{
    public  $slug;
    public function mount($slug){
        $this->slug = $slug;
    }


    public function render()
    {
        $family = Family::where('slug',$this->slug)->first()->toArray();

        $products = Product::where('family_id',$family['id'])->pluck('id');

        return view('livewire.client.adrenalistic', [
            'versions' => Version::with(['product','color','size','rate_version'])->whereIn('product_id',$products)->paginate()

        ])->layout('layouts.app');
    }
}
