<?php

namespace App\Http\Livewire\Client;

use App\Http\Livewire\Admin\Configuration;
use App\Models\Config;
use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.client.index',[
            'config'=>Config::where('name','BANNER_HOME')->first()
        ])->layout('layouts.app');
    }
}
