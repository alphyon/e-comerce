<?php

namespace App\Http\Livewire\Client;

use App\Models\CartItem;
use Livewire\Component;

class ShoppingCart extends Component
{
    public function quantityChange($id, $value)
    {
        $cartItem = CartItem::find($id);
        $cartItem->quantity = $value;
        $cartItem->save();
    }

    public function render()
    {
        return view('livewire.client.shopping-cart', [
            'items' => CartItem::with(['version' => function ($query) {
                $query->with(['rateVersion'=> function ($q) {
                    $q->where('rate_id', auth()->user()->profile->rate_id);
                }]);
            }])->where('cart_id', auth()->user()->cart->id)->get()
        ]);
    }
}
