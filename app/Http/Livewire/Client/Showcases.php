<?php

namespace App\Http\Livewire\Client;

use App\Models\ShowCase;
use App\Models\SubFamily;
use Livewire\Component;

class Showcases extends Component
{
    public function render()
    {
        $showcases = ShowCase::all();

        return view('livewire.client.showcases',
        ['stores'=>$showcases])->layout('layouts.app');;
    }
}
