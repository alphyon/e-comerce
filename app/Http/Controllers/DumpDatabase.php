<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Delivery;
use App\Models\Family;
use App\Models\Language;
use App\Models\Product;
use App\Models\ProductLanguage;
use App\Models\Profile;
use App\Models\Rate;
use App\Models\Role;
use App\Models\RolFamily;
use App\Models\User;
use App\Models\Version;
use App\Repositories\UserRepository;
use App\Utils\ManagerFTPFiles;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DumpDatabase extends Controller
{
public $countC =0;
public $countU =0;
    public function fillProductsAndVersions($file, $dir)
    {

        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 0);
            $file_ftp = new ManagerFTPFiles();
            $file_local_name = $file_ftp->getFileFromFTP($file, $dir);
            $file = Storage::get('ftp_down/' . $file_local_name);
            $tem_collect = Str::of($file)->split("[\r\n]")
                ->filter(function ($element) {
                    return !empty($element);
                });
        $tem_collect->each(function ($item) {
                $convert = utf8_encode($item);
                $row = Str::of($convert)->explode(";");
                if($row->count() === 14) {
                    $versionGet = Version::where('reference', $row[0])->get();

                    if ($versionGet->count() === 0) {

                        $cat = Category::firstOrCreate(
                            ['name' => $row[2]],
                            ['active' => true]
                        );
                        $boxValue=1;
                        $product = new Product();
                        $product->name = $row[1];
                        if(Str::contains($row[0],"MB")){
                            if(preg_match('~([(| ][0-9]+ [U|u])~', $row[1], $match)){

                                $boxValue = preg_replace('/\D/', '', $match[0]);
                            };

                        }else{

                            $boxValue = $row[3];
                        }
                        $product->box= $boxValue;
                        $product->iva = $row[7];
                        $product->width = $row[4];
                        $product->height = $row[5];
                        $product->large = $row[6];
                        $product->category_id = $cat->id;
                        $product->name = $row[1];
                        $product->save();

                        Language::all()->each(function ($item) use ($product) {
                            $productLanguage = new ProductLanguage();
                            $productLanguage->product_id = $product->id;
                            $productLanguage->language_id = $item->id;
                            $productLanguage->save();
                        });

                        $version = new Version();
                        $version->reference = $row[0];
                        $version->stock = (float)$row[9] <= 0 ? 0 : $row[9];
                        $version->weight = (float)$row[10] * 100 ?? 0;
                        $version->material = $row[11] ?? null;
                        $version->battery = $row[12] ?? null;
                        $version->type = "version";
                        $version->product_id = $product->id;
                        $version->save();
                        $this->countC++;
                    } else {
                        if (isset($row[9])) {
                            $toUpdate = Version::find($versionGet[0]->first()->id);
                            $toUpdate->stock = (float)$row[9] <= 0 ? 0 : $row[9];
                            $toUpdate->weight = (float)$row[10] * 100;
                            $toUpdate->material = $row[11];
                            $toUpdate->battery = $row[12];
                            $toUpdate->save();
                            $this->countU++;
                        }
                    }
                }

            });
        return true;
        }catch (\Exception $exception){
            Log::error("CREADOS ".$this->countC);
            Log::error("ACTUALIZADOS ".$this->countU);
            Log::error($exception->getMessage());
            Log::error($exception->getFile());
            Log::error($exception->getLine());
        return false;
        }

    }
    public function fillUsers($fileP, $dir)
    {


        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 0);
            $file_ftp = new ManagerFTPFiles();
            $file_local_name = $file_ftp->getFileFromFTP($fileP, $dir);
            $file = Storage::get('ftp_down/' . $file_local_name);
            $tem_collect = Str::of($file)->split("[\r\n]")
                ->filter(function ($element) {
                    return !empty($element);
                });
            $tem_collect->each(function ($item) {
                $convert = utf8_encode($item);
                $row = Str::of($convert)->explode(";");
                if($row->count() === 21) {
                    $userGet = User::where('connector_id', $row[0])->get();

                    if ($userGet->count() === 0) {

                        $code= md5(time());
                        $repo = new UserRepository(new User());
                        $data =[
                            'connector_id' => $row[0],
                            'email' => $row[2],
                            'salt' =>$code,
                            'status' => false,
                            'password' => bcrypt($code),
                            'role_id' => Role::where('slug','user')->first()->id
                        ];
                        $user = $repo->create($data);



                        $profile = new Profile();
                        $profile->user_id = $user->id;
                        $profile->name = $row[1];
                        $profile->address = $row[6];
                        $profile->city = $row[7];
                        $profile->state = $row[9];
                        $profile->country = $row[10];
                        $profile->postcode = $row[8];
                        $profile->business_name = $row[4];
                        $profile->nif = $row[3];
                        $profile->rate_id = Rate::where('connector_id',$row[19])->first()->id ?? 1;

                        $profile->save();

                        if(isset($row[12])){
                            $contact = new Contact();
                            $contact->user_id = $user->id;
                            $contact->value = $row[12];
                            $contact->reference = "phone1";
                            $contact->save();
                        }

                        if(isset($row[13])){
                            $contact = new Contact();
                            $contact->user_id = $user->id;
                            $contact->value = $row[13];
                            $contact->reference = "phone2";
                            $contact->save();
                        }
                        if(isset($row[14])){
                            $contact = new Contact();
                            $contact->user_id = $user->id;
                            $contact->value = $row[14];
                            $contact->reference = "fax";
                            $contact->save();
                        }

                        $delivery = new Delivery();
                        $delivery->deliverable_type = $user;
                        $delivery->deliverable_id = $user->id;
                        $delivery->city = $row[18];
                        $delivery->postcode = $row[17];
                        $delivery->state = $row[16];
                        $delivery->save();

                        $cartUser = new Cart();
                        $cartUser->user_id=$user->id;
                        $cartUser->save();

                        $families = Family::all();
                        $families->each(function ($item) use ($user){
                           $family = new RolFamily();
                           $family->user_id= $user->id;
                           $family->family_id = $item->id;
                           $family->save();
                        });
                    }
                }
            });
            return true;
        }catch (\Exception $exception){
            Log::error("CREADOS ".$this->countC);
            Log::error("ACTUALIZADOS ".$this->countU);
            Log::error($exception->getMessage());
            Log::error($exception->getFile());
            Log::error($exception->getLine());
            return false;
        }

    }
}
