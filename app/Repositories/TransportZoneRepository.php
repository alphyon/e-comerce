<?php


namespace App\Repositories;


use App\Interfaces\ITransportZoneRepository;
use app\models\transportzone;

class TransportZoneRepository extends BaseRepository implements ITransportZoneRepository
{

    public function __construct(TransportZone $model)
    {
        parent::__construct($model);
    }

}
