<?php


namespace App\Repositories;


use App\Interfaces\IOrderItemRepository;
use App\Models\OrderItem;

class OrderItemRepository extends BaseRepository implements IOrderItemRepository
{

    public function __construct(OrderItem $model)
    {
        parent::__construct($model);
    }

}
