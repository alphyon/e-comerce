<?php


namespace App\Repositories;


use App\Interfaces\IContactRepository;
use App\Models\Contact;

class ContactRepository extends BaseRepository implements IContactRepository
{

    public function __construct(Contact $model)
    {
        parent::__construct($model);
    }

}
