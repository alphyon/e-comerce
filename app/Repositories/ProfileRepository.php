<?php


namespace App\Repositories;


use App\Interfaces\IProfileRepository;
use App\Models\Profile;

class ProfileRepository extends BaseRepository implements IProfileRepository
{

    public function __construct(Profile $model)
    {
        parent::__construct($model);
    }

}
