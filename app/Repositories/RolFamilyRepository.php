<?php


namespace App\Repositories;


use App\Interfaces\IRolFamilyRepository;
use App\Models\RolFamily;

class RolFamilyRepository extends BaseRepository implements IRolFamilyRepository
{

    public function __construct(RolFamily $model)
    {
        parent::__construct($model);
    }

}
