<?php


namespace App\Repositories;


use App\Interfaces\IConfigRepository;
use App\Models\Config;

class ConfigRepository extends BaseRepository implements IConfigRepository
{

    public function __construct(Config $model)
    {
        parent::__construct($model);
    }

}
