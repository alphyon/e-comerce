<?php


namespace App\Repositories;


use App\Interfaces\IBillRepository;
use App\Models\Bill;

class BillRepository extends BaseRepository implements IBillRepository
{

    public function __construct(Bill $model)
    {
        parent::__construct($model);
    }

}
