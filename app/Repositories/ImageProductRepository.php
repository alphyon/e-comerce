<?php


namespace App\Repositories;


use App\Interfaces\IImageProductRepository;
use App\Models\ImageShowCase;

class ImageProductRepository extends BaseRepository implements IImageProductRepository
{

    public function __construct(ImageShowCase $model)
    {
        parent::__construct($model);
    }

}
