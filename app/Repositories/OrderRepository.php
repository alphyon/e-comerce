<?php


namespace App\Repositories;


use App\Interfaces\IOrderRepository;
use App\Models\Order;

class OrderRepository extends BaseRepository implements IOrderRepository
{

    public function __construct(Order $model)
    {
        parent::__construct($model);
    }

}
