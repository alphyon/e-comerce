<?php


namespace App\Repositories;


use App\Interfaces\IVersionRepository;
use App\Models\Version;

class VersionRepository extends BaseRepository implements IVersionRepository
{

    public function __construct(Version $model)
    {
        parent::__construct($model);
    }

}
