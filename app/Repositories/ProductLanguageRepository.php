<?php


namespace App\Repositories;


use App\Interfaces\IProductLanguageRepository;
use App\Models\ProductLanguage;

class ProductLanguageRepository extends BaseRepository implements IProductLanguageRepository
{

    public function __construct(ProductLanguage $model)
    {
        parent::__construct($model);
    }

}
