<?php


namespace App\Repositories;


use App\Interfaces\IVersionVersionRepository;
use App\Models\VersionVersion;

class VersionVersionRepository extends BaseRepository implements IVersionVersionRepository
{

    public function __construct(VersionVersion $model)
    {
        parent::__construct($model);
    }

}
