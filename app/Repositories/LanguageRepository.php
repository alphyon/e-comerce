<?php


namespace App\Repositories;


use App\Interfaces\ILanguageRepository;
use App\Models\Language;

class LanguageRepository extends BaseRepository implements ILanguageRepository
{

    public function __construct(Language $model)
    {
        parent::__construct($model);
    }

}
