<?php


namespace App\Repositories;


use App\Interfaces\IStatusRepository;
use App\Models\Status;

class StatusRepository extends BaseRepository implements IStatusRepository
{

    public function __construct(Status $model)
    {
        parent::__construct($model);
    }

}
