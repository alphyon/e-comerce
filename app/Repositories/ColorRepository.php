<?php


namespace App\Repositories;


use App\Interfaces\IColorRepository;
use App\Models\Color;

class ColorRepository extends BaseRepository implements IColorRepository
{

    public function __construct(Color $model)
    {
        parent::__construct($model);
    }

}
