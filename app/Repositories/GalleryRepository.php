<?php


namespace App\Repositories;


use App\Interfaces\IGalleryRepository;
use App\Models\Gallery;

class GalleryRepository extends BaseRepository implements IGalleryRepository
{

    public function __construct(Gallery $model)
    {
        parent::__construct($model);
    }

}
