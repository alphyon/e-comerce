<?php


namespace App\Repositories;


use App\Interfaces\IDownloadRepository;
use App\Models\Download;

class DownloadRepository extends BaseRepository implements IDownloadRepository
{

    public function __construct(Download $model)
    {
        parent::__construct($model);
    }

}
