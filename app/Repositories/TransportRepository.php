<?php


namespace App\Repositories;


use App\Interfaces\ITransportRepository;
use App\Models\Transport;

class TransportRepository extends BaseRepository implements ITransportRepository
{

    public function __construct(Transport $model)
    {
        parent::__construct($model);
    }

}
