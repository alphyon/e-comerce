<?php


namespace App\Repositories;

use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Interfaces\IRepository;
use App\Utils\LogManager;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class BaseRepository implements IRepository
{
    protected $model;
    protected $log;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->log = new LogManager();
    }

    public function create(array $data)
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $exception) {
            $this->log->ErrorLog(
                config('constants.messages.exceptions.not_create_exception'),
                $exception->getMessage()
            );
            throw new CreateException($exception);
        }
    }

    public function find($id)
    {
        try {
            return $this->model->findOrFail($id);
        } catch (Exception $exception) {
            $this->log->ErrorLog(
                config('constants.messages.exceptions.not_found_exception'),
                $exception->getMessage()
            );
            throw new NotFoundException('constants.messages.exceptions.not_found_exception');
        }
    }

    public function update($id, array $data)
    {
        try {
            return tap($this->model->findOrFail($id))->update($data);
        } catch (Exception $exception) {
            $this->log->ErrorLog(
                config('constants.messages.exceptions.not_update_exception'),
                $exception->getMessage()
            );
            throw new UpdateException(config('constants.messages.exceptions.not_update_exception'));
        }
    }

    public function delete($id)
    {
        try {
            return $this->model->findOrFail($id)->delete();
        } catch (Exception $exception) {
            $this->log->ErrorLog(
                config('constants.messages.exceptions.not_delete_exception'),
                $exception->getMessage()
            );
            throw new DeleteException(config('constants.messages.exceptions.not_delete_exception'));
        }
    }

    public function findByParam($column, $value)
    {
        $data = $this->model->where($column, $value)->paginate();
        if ($data->count() === 0) {
            throw new NotFoundException(config('constants.messages.exceptions.not_found_exception'));
        }
        return $data;
    }

    public function findByMultiParams(array $conditions)
    {
        $data = $this->model->where($conditions)->get();
        if ($data->count() === 0) {
            throw new NotFoundException(config('constants.messages.exceptions.not_found_exception'));
        }
        return $data;
    }

    public function all()
    {
        $data = $this->model->all();
        if ($data->count() === 0) {
            throw new NotFoundException(config('constants.messages.exceptions.not_found_exception'));
        }
        return $data;
    }

    public function allPaginate($number=15)
    {
        $data = $this->model->paginate($number);
        if ($data->count() === 0) {
            throw new NotFoundException(config('constants.messages.exceptions.not_found_exception'));
        }
        return $data;
    }
}
