<?php


namespace App\Repositories;


use App\Interfaces\ICurrencyChangeRepository;
use App\Models\CurrencyChange;

class CurrencyChangeRepository extends BaseRepository implements ICurrencyChangeRepository
{

    public function __construct(CurrencyChange $model)
    {
        parent::__construct($model);
    }

}
