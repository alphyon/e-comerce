<?php


namespace App\Repositories;


use App\Interfaces\ICartRepository;
use App\Models\Cart;

class CartRepository extends BaseRepository implements ICartRepository
{

    public function __construct(Cart $model)
    {
        parent::__construct($model);
    }

}
