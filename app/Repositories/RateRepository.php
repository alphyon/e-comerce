<?php


namespace App\Repositories;


use App\Interfaces\IRateRepository;
use App\Models\Rate;

class RateRepository extends BaseRepository implements IRateRepository
{

    public function __construct(Rate $model)
    {
        parent::__construct($model);
    }

}
