<?php


namespace App\Repositories;


use App\Interfaces\ISizeRepository;
use App\Models\Size;

class SizeRepository extends BaseRepository implements ISizeRepository
{

    public function __construct(Size $model)
    {
        parent::__construct($model);
    }

}
