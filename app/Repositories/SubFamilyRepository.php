<?php


namespace App\Repositories;


use App\Interfaces\ISubFamilyRepository;
use App\Models\SubFamily;

class SubFamilyRepository extends BaseRepository implements ISubFamilyRepository
{

    public function __construct(SubFamily $model)
    {
        parent::__construct($model);
    }

}
