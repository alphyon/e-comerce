<?php


namespace App\Repositories;


use App\Interfaces\IRateVersionRepository;
use App\Models\RateVersion;

class RateVersionRepository extends BaseRepository implements IRateVersionRepository
{

    public function __construct(RateVersion $model)
    {
        parent::__construct($model);
    }

}
