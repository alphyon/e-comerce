<?php


namespace App\Repositories;


use App\Interfaces\ICartItemRepository;
use App\Models\CartItem;

class CartItemRepository extends BaseRepository implements ICartItemRepository
{

    public function __construct(CartItem $model)
    {
        parent::__construct($model);
    }

}
