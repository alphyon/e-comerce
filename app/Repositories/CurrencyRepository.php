<?php


namespace App\Repositories;


use App\Interfaces\ICurrencyRepository;
use App\Models\Currency;

class CurrencyRepository extends BaseRepository implements ICurrencyRepository
{

    public function __construct(Currency $model)
    {
        parent::__construct($model);
    }

}
