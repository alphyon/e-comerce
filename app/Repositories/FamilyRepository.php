<?php


namespace App\Repositories;


use App\Exceptions\CreateException;
use App\Exceptions\DeleteException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UpdateException;
use App\Interfaces\IRepository;
use App\Interfaces\IFamilyRepository;
use App\Models\Family;
use App\Utils\LogManager;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class FamilyRepository extends BaseRepository implements IFamilyRepository
{

    public function __construct(Family $model)
    {
        parent::__construct($model);
    }

}
