<?php


namespace App\Repositories;


use App\Interfaces\IDeliveryRepository;
use App\Models\Delivery;

class DeliveryRepository extends BaseRepository implements IDeliveryRepository
{

    public function __construct(Delivery $model)
    {
        parent::__construct($model);
    }

}
