<?php


namespace App\Repositories;


use App\Interfaces\IZoneRepository;
use App\Models\Zone;

class ZoneRepository extends BaseRepository implements IZoneRepository
{

    public function __construct(Zone $model)
    {
        parent::__construct($model);
    }

}
