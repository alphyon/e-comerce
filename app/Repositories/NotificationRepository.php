<?php


namespace App\Repositories;


use App\Interfaces\INotificationRepository;
use App\Models\Notification;

class NotificationRepository extends BaseRepository implements INotificationRepository
{

    public function __construct(Notification $model)
    {
        parent::__construct($model);
    }

}
