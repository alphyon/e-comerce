<?php


namespace App\Repositories;


use App\Interfaces\IRoleRepository;
use App\Models\Role;

class RoleRepository extends BaseRepository implements IRoleRepository
{

    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

}
