<?php


namespace App\Interfaces;


interface ICartRepository
{
    public function create(array $data);
    public function find($id);
    public function delete($id);
    public function findByParam($column,$value);
    public function all();
    public function allPaginate();
}
