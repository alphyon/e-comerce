<?php


namespace App\Interfaces;


interface IImageRepository
{
    public function create(array $data);
    public function find($id);
    public function update($id,array $data);
    public function delete($id);
    public function findByParam($column,$value);
    public function all();
    public function allPaginate();
}
