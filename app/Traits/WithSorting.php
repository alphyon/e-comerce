<?php

namespace App\Traits;

trait WithSorting
{
    public $perPage = 10;
    public $search = '';
    public $sortField = 'id';
    public $sortDirection = 'desc';
    public $showPaginate = true;
    /**
     * Ordenar por
     *
     */
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc': 'asc';
        } else {
            $this->sortDirection = 'asc';
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->page = 1;
    }
}
