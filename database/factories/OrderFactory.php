<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Status;
use App\Models\Transport;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'=>User::factory()->create()->id,
            'status_id'=>Status::factory()->create()->id,
            'transport_id'=>Transport::factory()->create()->id,
            'date'=>$this->faker->date(),
            'alias'=>$this->faker->word,
            'identify_number'=>$this->faker->randomNumber([1000,3000]),
            'total'=>$this->faker->randomNumber([1000,3000]),
            'idpedv'=>$this->faker->randomNumber([100,300]),
            'iva'=>$this->faker->randomNumber([1000,3000]),
            'pay_method'=>$this->faker->word,
            'transport_price'=>$this->faker->randomDigit,
            'comment'=>$this->faker->text,
            'bill'=>$this->faker->slug(3).$this->faker->fileExtension,
            'process'=>$this->faker->boolean,
        ];
    }
}
