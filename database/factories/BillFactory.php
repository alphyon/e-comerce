<?php

namespace Database\Factories;

use App\Models\Bill;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BillFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bill::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'=>User::factory()->create()->id,
            'order_id'=>Order::factory()->create()->id,
            'number'=>$this->faker->randomNumber([1000,3000]),
            'price'=>$this->faker->randomNumber([1000,3000]),
            'date'=>$this->faker->dateTime
        ];
    }
}
