<?php

namespace Database\Factories;

use App\Models\Rate;
use App\Models\RateVersion;
use App\Models\Version;
use Illuminate\Database\Eloquent\Factories\Factory;

class RateVersionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RateVersion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'version_id'=>Version::factory()->create()->id,
            'rate_id'=>Rate::factory()->create()->id,
            'price'=>$this->faker->numberBetween(1000,2000)
        ];
    }
}
