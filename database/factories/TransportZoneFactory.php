<?php

namespace Database\Factories;

use App\Models\Transport;
use App\Models\TransportZone;
use App\Models\Zone;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransportZoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TransportZone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'transport_id'=>Transport::factory()->create()->id,
            'zone_id'=>Zone::factory()->create()->id,
            'min_weight'=>$this->faker->numberBetween(1000,3000),
            'max_weight'=>$this->faker->numberBetween(3000,5000),
            'price'=>$this->faker->numberBetween(1000,3000),
        ];
    }
}
