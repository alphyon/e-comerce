<?php

namespace Database\Factories;

use App\Models\Notification;
use App\Models\Version;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'version_id'=>Version::factory()->create()->id,
            'email'=>$this->faker->safeEmail,
            'locale'=>$this->faker->locale
        ];
    }
}
