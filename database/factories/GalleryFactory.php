<?php

namespace Database\Factories;

use App\Models\Gallery;
use App\Models\Version;
use Illuminate\Database\Eloquent\Factories\Factory;

class GalleryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gallery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'version_id'=>Version::factory()->create()->id,
            'url'=>$this->faker->imageUrl(),
        ];
    }
}
