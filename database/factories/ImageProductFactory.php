<?php

namespace Database\Factories;

use App\Models\Image;
use App\Models\ImageShowCase;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ImageShowCase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id'=>Product::factory()->create()->id,
            'image_id'=>Image::factory()->create()->id,
        ];
    }
}
