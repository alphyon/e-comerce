<?php

namespace Database\Factories;

use App\Models\Family;
use App\Models\SubFamily;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubFamilyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubFamily::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'family_id'=>Family::factory()->create()->id,
            'name'=>$this->faker->word,
            'image'=>$this->faker->imageUrl(),
        ];
    }
}
