<?php

namespace Database\Factories;

use App\Models\Version;
use App\Models\VersionVersion;
use Illuminate\Database\Eloquent\Factories\Factory;

class VersionVersionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VersionVersion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'parent_id'=>Version::factory()->create()->id,
            'child_id'=>Version::factory()->create()->id,
            'quantity'=>$this->faker->numerify('##')
        ];
    }
}
