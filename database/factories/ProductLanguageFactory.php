<?php

namespace Database\Factories;

use App\Models\Language;
use App\Models\Product;
use App\Models\ProductLanguage;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductLanguageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductLanguage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id'=>Product::factory()->create()->id,
            'language_id'=>Language::factory()->create()->id,
            'description'=>$this->faker->text,
            'meta_title'=>$this->faker->locale,
            'meta_keys'=>$this->faker->word,
            'meta_description'=>$this->faker->word,
        ];
    }
}
