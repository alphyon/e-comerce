<?php

namespace Database\Factories;

use App\Models\RateModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class RateModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RateModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
