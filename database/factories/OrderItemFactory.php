<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Version;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'version_id'=>Version::factory()->create()->id,
            'order_id'=>Order::factory()->create()->id,
            'price'=>$this->faker->numberBetween(1000,9000),
            'quantity'=>$this->faker->numerify('##'),
        ];
    }
}
