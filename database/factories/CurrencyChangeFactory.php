<?php

namespace Database\Factories;

use App\Models\Currency;
use App\Models\CurrencyChange;
use Illuminate\Database\Eloquent\Factories\Factory;

class CurrencyChangeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CurrencyChange::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'currency_id'=>Currency::factory()->create()->id,
            'currency_name_to_change'=>$this->faker->word,
            'change_value'=>$this->faker->randomDigit,
        ];
    }
}
