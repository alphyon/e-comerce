<?php

namespace Database\Factories;

use App\Models\Delivery;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Delivery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $delivery = $this->faker->randomElement([Order::class,User::class]);
        return [
            'address'=>$this->faker->address,
            'city'=>$this->faker->city,
            'state'=>$this->faker->state,
            'country'=>$this->faker->country,
            'postcode'=>$this->faker->postcode,
            'deliverable_type'=>$delivery,
            'deliverable_id'=>self::factoryForModel($delivery)->create()->id
        ];
    }
}
