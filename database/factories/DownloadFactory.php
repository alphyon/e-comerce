<?php

namespace Database\Factories;

use App\Models\Download;
use App\Models\SubFamily;
use Illuminate\Database\Eloquent\Factories\Factory;

class DownloadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Download::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subfamily_id'=>SubFamily::factory()->create()->id,
            'reference'=>$this->faker->slug(3),
            'file'=>$this->faker->slug(3).".".$this->faker->fileExtension
        ];
    }
}
