<?php

namespace Database\Factories;

use App\Models\Currency;
use App\Models\Profile;
use App\Models\Rate;
use App\Models\User;
use App\Models\Zone;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'=>User::factory()->create()->id,
            'name'=>$this->faker->name,
            'name_complement'=>$this->faker->sentence(5),
            'address'=>$this->faker->address,
            'city'=>$this->faker->city,
            'state'=>$this->faker->state,
            'country'=>$this->faker->country,
            'postcode'=>$this->faker->postcode,
            'contact_reference'=>$this->faker->name,
            'connector_id'=>$this->faker->numerify('####'),
            'iva'=>$this->faker->boolean,
            'nif'=>$this->faker->numerify('#######'),
            'business_name'=>$this->faker->company,
            'web'=>$this->faker->url,
            'currency_id'=>Currency::factory()->create()->id,
            'bill'=>$this->faker->boolean,
            'rate_id'=>Rate::factory()->create()->id,
            'zone_id'=>Zone::factory()->create()->id,
        ];
    }
}
