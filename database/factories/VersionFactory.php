<?php

namespace Database\Factories;

use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\Models\Version;
use Illuminate\Database\Eloquent\Factories\Factory;

class VersionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Version::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id'=>Product::factory()->create()->id,
            'size_id'=>Size::factory()->create()->id,
            'color_id'=>Color::factory()->create()->id,
            'stock'=>$this->faker->numberBetween(10,100),
            'weight'=>$this->faker->numberBetween(1000,3000),
            'photo'=>$this->faker->imageUrl(),
            'active'=>$this->faker->boolean,
            'reference'=>$this->faker->numerify('#####'),
            'type'=>$this->faker->randomElement(['pack','version']),
            'battery'=>$this->faker->slug,
            'material'=>$this->faker->slug,
            'video'=>$this->faker->imageUrl(),
            'sort'=>$this->faker->randomDigit,
        ];
    }
}
