<?php

namespace Database\Factories;

use App\Models\Family;
use App\Models\RolFamily;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RolFamilyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RolFamily::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'=>User::factory()->create()->id,
            'family_id'=>Family::factory()->create()->id,
            'permission'=>$this->faker->boolean,
        ];
    }
}
