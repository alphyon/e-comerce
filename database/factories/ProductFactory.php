<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->word,
            'box'=>$this->faker->randomDigit,
            'active'=>$this->faker->boolean,
            'category_id'=>Category::factory()->create()->id,
            'width'=>$this->faker->randomNumber(),
            'large'=>$this->faker->randomNumber(),
            'height'=>$this->faker->randomNumber(),
            'iva'=>$this->faker->randomNumber(),
            'discount'=>$this->faker->randomNumber(),
            'umbral'=>$this->faker->randomDigit
        ];
    }
}
