insert into products (id, name, box, active, category_id, family_id, width, large, height, iva, discount, umbral, created_at, updated_at)
values  (1, 'product1', 10, 1, null, 1, 100, 100, 100, 1000, 0, 2, '2021-04-01 22:51:15', '2021-04-01 22:51:15'),
        (2, 'product2', 1, 1, null, 1, 100, 100, 100, 100, 0, 2, '2021-04-01 22:51:38', '2021-04-01 22:51:38'),
        (3, 'product3', 1, 1, null, 2, 100, 100, 100, 100, 0, 2, '2021-04-01 22:51:55', '2021-04-01 22:51:55'),
        (4, 'prodcuto test ', 10, 1, null, 1, 100, 100, 100, 100, 0, 2, '2021-04-02 02:10:14', '2021-04-02 02:10:14');
