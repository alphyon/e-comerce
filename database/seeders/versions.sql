insert into versions (id, product_id, size_id, color_id, stock, weight, photo, active, reference, type, battery, material, video, sort, created_at, updated_at)
values  (1, 1, 1, 1, 10, 1, 'public/version/f76qRbDpU8Uv5UrqUse8hoEzSFcA4buyVVsU3OPn.png', 1, '123', 'version', null, 'abs', null, 0, '2021-04-01 22:52:21', '2021-04-01 22:52:21'),
        (2, 1, 5, 3, 100, 1, 'public/version/DNudgzLaikzGfI5Awt6o0nYF33EHozKtxP0IPNvc.png', 1, '1', 'version', '1', '1', '1', 0, '2021-04-01 22:53:11', '2021-04-01 22:53:11'),
        (3, 2, 1, 7, 10, 1, 'public/version/HDwq7vzyN5lK8tlYpl20wXbAST0qTx80S2MuOkV3.png', 1, '12312', 'pack', '1', '1', '1', 0, '2021-04-01 22:53:56', '2021-04-01 22:53:56'),
        (4, 3, 4, 4, 1, 1, 'public/version/rHJvo21R8UpQhg5SlqRniRv2OoAHvIR7a1UYzutM.png', 1, 'da33', 'version', '1', '1', '1', 0, '2021-04-01 22:54:28', '2021-04-01 22:54:28'),
        (5, 4, 1, 5, 10, 1, 'public/version/KhOx2dLbz1Yl4b7OKbWBMcAvXAUdmYctdDD3u45i.png', 1, 'mm3883', 'version', '1', '1', '1', 0, '2021-04-02 02:10:44', '2021-04-02 02:10:44'),
        (6, 4, 2, 12, 12, 1, 'public/version/GlJ37ovTHmDPnwgN79mfmsvnZunVBibaEpSyBpen.png', 1, '12312x', 'version', null, '1', null, 0, '2021-04-02 03:01:11', '2021-04-02 03:01:11'),
        (7, 4, 2, 3, 112, 1, 'public/version/xHimP9VcpuFbjdJhjmLMYdRAnWxCEMj9QdmLJE4F.png', 1, '12312dsdsa', 'version', null, '1', null, 0, '2021-04-02 03:02:02', '2021-04-02 03:02:02');
