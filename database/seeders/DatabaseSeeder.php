<?php

namespace Database\Seeders;

use App\Models\Family;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            FamilySeed::class,
            LanguageSeeder::class,
            ColorSeeder::class,
            SizeSeeder::class,
            RateSeeder::class,
            CurrencySeeder::class,
            CategorySeeder::class,
            RolSeeder::class,
            UserSeeder::class,
            ProfileSeeder::class,
            CartSeeder::class,
            ProductSeeder::class,
            ProductLanguageSeeder::class,
            VersionSeeder::class,
            VersionRatesSeeder::class
        ]);
    }
}
