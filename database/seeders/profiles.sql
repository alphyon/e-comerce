insert into profiles (id, user_id, name, name_complement, address, city, state, country, postcode, contact_reference,
                       iva, nif, business_name, web, currency_id, bill, rate_id, zone_id, created_at,
                      updated_at)
values (1, 1, 'Marcia Kris', 'Et maiores laudantium et quam.', '788 Osinski Branch
New Zetta, IN 45582', 'Kassulkechester', 'North Dakota', 'Saint Barthelemy', '74233-3037', 'Eliezer Hamill',  1,
        '9821658', 'Rice-Mraz', 'https://www.mclaughlin.com/et-eaque-molestiae-magnam-dolores-accusamus-hic-illo', 1, 0,
        1, null, '2021-03-31 04:06:39', '2021-03-31 04:06:39'),
       (2, 2, 'Mrs. Augustine Krajcik', 'Iste quo aut nisi quos quia.', '889 Jacobs Mountains Suite 579
New Providenci, GA 27483', 'Ambershire', 'Wyoming', 'El Salvador', '73507-7955', 'Alden Hahn DDS',  0, '4731149',
        'Murray, Bednar and Dickens',
        'http://www.gutkowski.com/ut-deserunt-architecto-facilis-minus-est-sint-officia-sit', 1, 0, 118, null,
        '2021-03-31 04:06:39', '2021-03-31 04:06:39'),
       (3, 3, 'Ms. Hulda Leuschke', 'Libero harum et nisi non dolorum.', '976 Dorcas Ridges Suite 687
East Easton, OH 63670-7334', 'Deeburgh', 'Kansas', 'Yemen', '48806', 'Missouri Roberts',  1, '8062022',
        'Eichmann and Sons', 'https://reichel.biz/laboriosam-sit-vel-quam.html', 1, 0, 118, null, '2021-03-31 04:06:39',
        '2021-03-31 04:06:39');
