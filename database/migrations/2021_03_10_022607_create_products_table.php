<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->integer('box');
            $table->boolean('active')->default(true);
            $table->foreignId('category_id')->nullable()->constrained();
            $table->foreignId('family_id')->nullable()->constrained();
            $table->integer('width');
            $table->integer('large');
            $table->integer('height');
            $table->integer('iva');
            $table->integer('discount')->default(0);
            $table->integer('umbral')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
