<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained();
            $table->foreignId('size_id')->nullable()->constrained();
            $table->foreignId('color_id')->nullable()->constrained();
            $table->bigInteger('stock')->unsigned();
            $table->integer('weight')->unsigned();
            $table->string('photo')->nullable();
            $table->boolean('active')->default(true);
            $table->string('reference');
            $table->string('type')->nullable();
            $table->text('battery')->nullable();
            $table->text('material')->nullable();
            $table->text('video')->nullable();
            $table->integer('sort')->default(0)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('versions');
    }
}
