<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_zones', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transport_id')->constrained();
            $table->foreignId('zone_id')->constrained();
            $table->integer('min_weight');
            $table->integer('max_weight');
            $table->integer('price')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_zones');
    }
}
