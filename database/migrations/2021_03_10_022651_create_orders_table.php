<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('status_id')->constrained('status');
            $table->foreignId('transport_id')->constrained();
            $table->dateTime('date');
            $table->string('alias')->nullable();
            $table->integer('identify_number');
            $table->integer('total')->unsigned();
            $table->integer('idpedv')->unsigned();
            $table->integer('iva')->unsigned();
            $table->string('pay_method');
            $table->integer('transport_price')->default(0);
            $table->text('comment')->nullable();
            $table->string('bill')->nullable();
            $table->boolean('process')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
