<?php

use App\Http\Livewire\Admin\Colors;
use App\Http\Livewire\Admin\Configuration;
use App\Http\Livewire\Admin\Downloads;
use App\Http\Livewire\Admin\Family;
use App\Http\Livewire\Admin\Index;
use App\Http\Livewire\Admin\Language;
use App\Http\Livewire\Admin\Orders;
use App\Http\Livewire\Admin\Products;
use App\Http\Livewire\Admin\Products\Manage;
use App\Http\Livewire\Admin\Requests;
use App\Http\Livewire\Admin\Showcases;
use App\Http\Livewire\Admin\Sizes;
use App\Http\Livewire\Admin\Users;
use App\Http\Livewire\Admin\Version;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->name('admin.')->middleware(['auth', 'admin'])->group(function () {
    Route::get('/', Index::class)->name('index');
    Route::get('/language', Language::class)->name('language');
    Route::get('/family', Family::class)->name('family');
    Route::get('/colors', Colors::class)->name('colors');
    Route::get('/sizes', Sizes::class)->name('sizes');

    Route::get('/products', Products::class)->name('products.index');
    Route::get('/products/{product}/manage', Manage::class)->name('products.manage');
    Route::get('/version/{version}/manage', Version::class)->name('versions.manage');

    Route::get('/orders', Orders::class)->name('orders');
    Route::get('/users', Users::class)->name('users');
    Route::get('/request', Requests::class)->name('requests');
    Route::get('/downloads', Downloads::class)->name('downloads');
    Route::get('/settings', Configuration::class)->name('settings');
    Route::get('/showcases', Showcases::class)->name('showcases');
    Route::get('/downloads/{subFamily}/items', Downloads\Manage::class)->name('downloads.items');
    Route::get('/showcases/{showcase}/items', \App\Http\Livewire\Admin\Showcases\Manage::class)->name('showcase.items');
});
