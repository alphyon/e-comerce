<?php

use App\Http\Livewire\Client\Adrenalistic;
use App\Http\Livewire\Client\Dashboard;
use App\Http\Livewire\Client\Family;
use App\Http\Livewire\Client\Index;
use App\Http\Livewire\Client\Product;
use App\Http\Livewire\Client\ShoppingCart;
use Illuminate\Support\Facades\Route;

Route::prefix('/{lang}')->middleware(['localization', 'auth'])->group(function () {
    Route::get('/', Index::class)->name('home');
    Route::middleware(['families'])->get('/family/{family:slug}', Family::class)->name('client.family');
    Route::middleware(['families'])->get('/downloads/{family:slug}', \App\Http\Livewire\Client\Downloads::class)->name('client.download');
    Route::get('/showcases', \App\Http\Livewire\Client\Showcases::class)->name('client.showcases');
    Route::get('/showcases/{id}/items', \App\Http\Livewire\Client\Showcases\Items::class)->name('client.showcases.items');
    Route::get('/product/{id}', Product::class)->name('client.product');
    Route::get('/dashboard/', Dashboard::class)->name('client.dashboard');
    Route::get('/dashboard/edit', Dashboard\ClientEdit::class)->name('client.dashboard.edit');
    Route::get('/dashboard/change-pass', Dashboard\ChangePassword::class)->name('client.dashboard.pass_change');
    Route::get('/dashboard/change-delivery', Dashboard\AddressChange::class)->name('client.dashboard.delivery_change');
    Route::get('/shopping-cart', ShoppingCart::class)->name('client.cart');
});
